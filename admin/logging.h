/***************************************************************************
               logging.h - Status logging data
                             -------------------
    begin                : Wed Apr 01, 2009
    copyright            : (C) 2009 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LOGGING_H
#define LOGGING_H



enum LogType
{
  TYPE_NORMAL                 // Normal log messages
, TYPE_STATUS                 // Service status or activity messages
, TYPE_DEBUG                  // Debug messages
, TYPE_ERROR                  // Server error messages
, TYPE_INCOMING_TRAFFIC       // Incoming data
, TYPE_OUTGOING_TRAFFIC       // Outgoing data
, TYPE_OUTGOING_ERROR         // Outgoing error response data
};


enum ConnectionType
{
  TYPE_DISPATCH
, TYPE_NS
, TYPE_SB
, TYPE_SOAP
};


#endif
