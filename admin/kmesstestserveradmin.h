/***************************************************************************
   kmesstestserveradmin.h - KMess Testing MSN Server GUI administration program
                             -------------------
    begin                : Sat March 28, 2009
    copyright            : (C) 2009 by Valerio Pilo, Sjors Gielen
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef KMESSTESTSERVERADMIN_H
#define KMESSTESTSERVERADMIN_H

#include <QMainWindow>

#include "logging.h"
#include "ui_kmesstestserveradmin.h"

class AdminConnection;
class MsnServerGroup;
class TokenManager;
struct Token;
class Account;


/**
 * @brief The KMess Testing MSN Server administration program.
 *
 * This is the user interface part of the fake testing server.
 * It starts a MsnServerGroup object internally to start listening at the various sockets.
 */
class KMessTestServerAdmin : public QMainWindow, private Ui::KMessTestServerAdmin
{
  Q_OBJECT


  public:

    // Constructor
                          KMessTestServerAdmin( AdminConnection *ac, QWidget *parent = 0 );
    // Destructor
    virtual              ~KMessTestServerAdmin();


  public slots:

    // Add a status message for one of the servers
    void                  statusLog( const QString &sender,
                                     const QDateTime &date,
                                     LogType logType, const QString &message );
    // Add a log message for a child connection to the server status GUI
    void                  statusLog( const QString &message, int connectionId,
                                     ConnectionType connectionType,
                                     LogType type, const QString &origSource,
                                     const QDateTime &date);
    void                  initialize();


  private slots:

    // Switch to another page
    void                  changePage( QListWidgetItem *newPage );
    // Switch to another connection in a server page
    void                  selectConnection( QListWidgetItem *newConnection );
    // Edit a user (when the button is clicked)
    void                  editUser();

    void          tokensReceived( const QHash<int, QString>& );
    void          tokenInfoReceived( int id, const QList<QByteArray>& );

  private: // Private properties

    // List of connection dumps for the Dispatch server
    QMap<int,QString>     dispatchLogs_;
    // List of connection dumps for the Notification server
    QMap<int,QString>     notificationLogs_;
    // List of connection dumps for the SOAP server
    QMap<int,QString>     soapLogs_;
    // List of accounts
    QList<Account*>       accounts_;
    // The server
    AdminConnection      *adminConnection_;

    // Page icons
    QListWidgetItem      *mainPageIcon_;
    QListWidgetItem      *dispatchPageIcon_;
    QListWidgetItem      *notificationPageIcon_;
    QListWidgetItem      *soapPageIcon_;
    QListWidgetItem      *tokenPageIcon_;
    QListWidgetItem      *contactsPageIcon_;

};



#endif
