/***************************************************************************
   connectionloglist.cpp - Connection logs in a list
                             -------------------
    begin                : Thu Aug 5, 2010
    copyright            : (C) 2010 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <QDebug>

#include "connectionloglist.h"
#include "ui_connectionloglist.h"

ConnectionLogList::ConnectionLogList(const QString &serverLogName,
                                     QWidget *parent)
: QWidget(parent)
, ui(new Ui::ConnectionLogList)
{
  ui->setupUi(this);
  setLogName( serverLogName );

  // Connect the connections lists' signals (to allow choosing a connection)
  connect( ui->list_, SIGNAL(    itemActivated(QListWidgetItem*) ),
           this,      SLOT  ( selectConnection(QListWidgetItem*) ) );
}

ConnectionLogList::~ConnectionLogList()
{
  delete ui;
}

void ConnectionLogList::addConnectionLog( const QDateTime &date,
                                          int connectionId )
{
  Q_ASSERT( !hasConnectionId( connectionId ) );

  int index = ui->list_->count();
  connectionIdToIndex_.insert( connectionId, index );

  QString label = QString::number( connectionId ) + " - "
                + date.time().toString();
  ui->list_->insertItem( connectionId, label );

  connectionIdToLogs_.insert( connectionId, "" );

  qDebug() << "Adding connectionId" << connectionId << "as index" << index;

  Q_ASSERT( ui->list_->item( index ) != 0 );
  Q_ASSERT( ui->list_->item( index )->text() == label );
}

void ConnectionLogList::addToLog( int connectionId, const QString &msg )
{
  Q_ASSERT( hasConnectionId( connectionId ) );

  connectionIdToLogs_[connectionId].append( msg );
  // TODO: update the text browser if connectionId == currently selected,
  // and scroll down etc
}

void ConnectionLogList::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
  case QEvent::LanguageChange:
      ui->retranslateUi(this);
      break;
  default:
      break;
  }
}

bool ConnectionLogList::hasConnectionId( int connectionId ) const
{
  return connectionIdToIndex_.contains( connectionId );
}

// Switch to another connection
void ConnectionLogList::selectConnection( QListWidgetItem *newConnection )
{
  int index = ui->list_->row( newConnection );
  int connectionId = connectionIdToIndex_.key( index, -1 );
  qDebug() << "Index:" << index << "connectionId:" << connectionId;
  Q_ASSERT( hasConnectionId( connectionId ) );

  ui->text_->setHtml( connectionIdToLogs_[ connectionId ] );
}

void ConnectionLogList::setLogName( const QString &serverLogName )
{
  ui->serverLogName_->setText( serverLogName + " server log:" );
}
