#ifndef LOGINWINDOW_H
#define LOGINWINDOW_H

#include <QtGui/QDialog>
#include <QtNetwork/QAbstractSocket>

namespace Ui {
    class LoginWindow;
}

class AdminConnection;
class KMessTestServerAdmin;

class LoginWindow : public QDialog {
    Q_OBJECT
public:
    LoginWindow(QWidget *parent = 0);
    ~LoginWindow();

protected:
    void changeEvent(QEvent *e);

private slots:
    void accept();
    void connected();
    void error(QAbstractSocket::SocketError);
    void hostFound();
    void sendAuthentication();
    void slotAuthenticationSucceeded();
    void slotAuthenticationFailed();
    void protocolError(const QString&);

private:
    Ui::LoginWindow *m_ui;
    AdminConnection *ac_;
    KMessTestServerAdmin *adm_;
    QString handle_;
    QString password_;
    bool ignoreClose_;
};

#endif // LOGINWINDOW_H
