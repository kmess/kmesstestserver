/***************************************************************************
               adminconnection.h - The connection to the server.
                             -------------------
    begin                : Sat Nov 28, 2009
    copyright            : (C) 2009 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ADMINCONNECTION_H
#define ADMINCONNECTION_H

#include <QObject>
#include <QTcpSocket>
#include <QStringList>

#include "logging.h"



/**
 * @brief The connection to the KMess Live server.
 * @author Sjors Gielen
 */
class AdminConnection : public QTcpSocket
{
  Q_OBJECT

  public:

                  AdminConnection( QObject *parent = 0 );
    virtual      ~AdminConnection();
    LogType       readLogType( const QString& ) const;
    bool          isPayloadCommand( const QString &cmd ) const;

  public slots:
    void          authenticate( const QString &u, const QString &p );
    void          retrieveLogMessages( int howmany = 100 );
    void          retrieveConnectionLogs( int howmany = 25 );
    void          retrieveCurrentConnections();
    void          retrieveTokens();
    void          retrieveContacts();

  private slots:
    void          slotConnected();
    void          slotReadyRead();

  signals:
    void          protocolError(const QString&);
    void          authenticationNeeded();
    void          authenticationSucceeded();
    void          authenticationFailed();

    void          tokensReceived( const QHash<int, QString>& );
    void          tokenInfoReceived( int id, const QList<QByteArray>& );

    void          logMessage( const QString &source, const QDateTime &date,
                              LogType type, const QString &payload );
    void          connectionLog( const QString &message, const QDateTime &date );
    void          addConnection( const QString &handle );

  private:
    void          writeCommand( const QString &cmd,
                                const QStringList &args = QStringList(),
                                const QByteArray &payload = QByteArray() );
    int           lastTrId_;
};



#endif
