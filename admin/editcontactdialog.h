/***************************************************************************
               editcontactdialog.h - Dialog to change a contact's settings
                             -------------------
    begin                : Fri Aug 7, 2009
    copyright            : (C) 2009 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef EDITCONTACTDIALOG_H
#define EDITCONTACTDIALOG_H

#include "ui_editcontactdialog.h"
#include <QObject>

class QAbstractButton;
class QPushButton;


/**
 * @brief A dialog to edit a contacts' settings.
 *
 * This dialog allows the user to edit a contacts' settings, as well as
 * creating a new contact.
 *
 * @author Sjors Gielen
 */
class EditContactDialog : public QDialog, private Ui::EditContactDialog
{
  Q_OBJECT

  public:

    // Constructor
                  EditContactDialog( const QString &handle, QWidget *parent = 0 );
    // Destructor
    virtual      ~EditContactDialog();

  private slots:
    void          clicked( QAbstractButton *button );

  private:
    QString       handle_;
    QPushButton  *saveAsNew_;
};



#endif
