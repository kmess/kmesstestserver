/***************************************************************************
                  adminconnection.cpp - Connection to the KMess Live server
                             -------------------
    begin                : Sat Nov 28, 2009
    copyright            : (C) 2009 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <QtCore/QDebug>
#include <QtCore/QStringList>
#include <QtCore/QDateTime>

#include "adminconnection.h"

#define PROTOCOL_VERSION "KMADM2"

// Constructor
AdminConnection::AdminConnection( QObject *parent )
: QTcpSocket( parent )
, lastTrId_( -1 )
{
  connect( this, SIGNAL(     connected() ),
           this, SLOT(   slotConnected() ) );
  connect( this, SIGNAL(     readyRead() ),
           this, SLOT(   slotReadyRead() ) );
}

// Destructor
AdminConnection::~AdminConnection()
{}

bool AdminConnection::isPayloadCommand(const QString &cmd) const
{
  QStringList payloadCommands;
  payloadCommands << "LOG" << "TKN";

  return payloadCommands.contains( cmd, Qt::CaseInsensitive );
}

void AdminConnection::slotConnected()
{
  writeCommand( "VER", QStringList() << PROTOCOL_VERSION );
}

void AdminConnection::slotReadyRead()
{
  if(!canReadLine()) return;

  QByteArray line = readLine().trimmed();
  if( line.isEmpty() )
  {
    emit protocolError(QString("No data could be read: %1").arg(errorString()));
    close();
    return;
  }

  QStringList command = QString(line).split( " ", QString::SkipEmptyParts );
  if( command.size() == 0 )
  {
    emit protocolError( QString( "No valid command could be read: " )
                               .append( line ) );
    close();
    return;
  }

  QByteArray payload;
  if( isPayloadCommand( command[0] ) )
  {
    bool valid = true;
    QString last = command.takeLast();
    int payloadSize = last.toInt( &valid );
    if( !valid )
    {
      // better abort right away, because this will f*** up all data from now on
      emit protocolError("Payload command didn't have valid payload at "
                                 "end of line. (last=" + last + ")");
      close();
      return;
    }
    while( payload.size() < payloadSize )
    {
      if( !bytesAvailable() )
      {
        waitForReadyRead( -1 );
      }
      QByteArray r = read( payloadSize - payload.size() );
      payload.append( r );
    }
  }

  qDebug() << "Read command: " << command;
  qDebug() << "Payload: " << payload;

  if( command[0].toUpper() == "VER" )
  {
    if( command.size() < 3 || command[2] != PROTOCOL_VERSION )
    {
      emit protocolError( "Remote server does not speak " PROTOCOL_VERSION );
      close();
      return;
    }

    emit authenticationNeeded();
  }
  else if( command[0].toUpper() == "USR" )
  {
    if( command[2] != "OK" )
    {
      emit authenticationFailed();
    }
    else
    {
      emit authenticationSucceeded();
    }
  }
  else if( command[0].toUpper() == "LOG" )
  {
    QString source = command[1];
    QDateTime date = QDateTime::fromString( command[2], Qt::ISODate );
    LogType type   = readLogType( command[3] );

    emit logMessage( source, date, type, payload );
  }
  else if( command[0].toUpper() == "TKN" )
  {
    QString retType = command[1];
    Q_ASSERT( retType == "INFO" || retType == "LIST" );
    if( retType == "INFO" )
    {
      bool valid = false;
      int tokenId = command[2].toInt(&valid);
      Q_ASSERT( valid );
      QList<QByteArray> info = payload.split( '\n' );
      emit tokenInfoReceived( tokenId, info );
    }
    else
    {
      QHash<int, QString> hash;

      QList<QByteArray> tokens = payload.split( '\n' );
      hash.reserve( tokens.size() );
      foreach( const QByteArray &b, tokens )
      {
        if( b.trimmed().isEmpty() )
          continue;
        int space = b.indexOf( ' ' );
        Q_ASSERT( space > 0 );
        bool valid = false;
        int tokenId = b.left(space).toInt(&valid);
        Q_ASSERT( valid );
        QString label = b.mid(space+1);
        hash.insert( tokenId, label );
      }
      
      emit tokensReceived( hash );
    }
  }
  else
  {
    qWarning() << "Unknown command received. Payloadsize=" << payload.size()
               << "; Command: " << command;
  }

  if( bytesAvailable() )
  {
    slotReadyRead();
  }
}

void AdminConnection::authenticate( const QString &handle, const QString &password )
{
  // password stuff is TODO.
  writeCommand( "USR", QStringList() << handle );
}



/**
 * @brief Turn a log type from the server in a LogType enum value.
 */
LogType AdminConnection::readLogType( const QString &type ) const
{
  if( type == "Outgoing" )
    return TYPE_OUTGOING_TRAFFIC;
  else if( type == "Incoming" )
    return TYPE_INCOMING_TRAFFIC;
  else if( type == "Debug" )
    return TYPE_DEBUG;
  else if( type == "Info" )
    return TYPE_NORMAL;
  else if( type == "Warning" )
    return TYPE_ERROR;
  else if( type == "Error" )
    return TYPE_ERROR;
  else if( type == "Fatal" )
    return TYPE_ERROR;
  else
    return (LogType)-1;
}



/**
 * @brief Request a list of connection logs from the server.
 *
 * A signal connectionLog() will be emitted for every connection log retrieved.
 */
void AdminConnection::retrieveConnectionLogs( int howmany )
{
  writeCommand("CNL", QStringList() << "*");
}



/**
 * @brief Request the contacts that exist on the server.
 *
 * The signal contactsRetrieved() will be emitted when the answer is received.
 */
void AdminConnection::retrieveContacts()
{
  writeCommand( "CNT", QStringList() << "*" );
}



/**
 * @brief Request a list of current connections.
 *
 * A signal addConnection() will be emitted for every connection retrieved.
 */
void AdminConnection::retrieveCurrentConnections()
{
  writeCommand("CCN", QStringList() << "*");
}



/**
 * @brief Request a list of previous log messages from the server.
 *
 * A signal logMessage() will be emitted for every log message retrieved.
 */
void AdminConnection::retrieveLogMessages( int howmany )
{
  writeCommand("LMG", QStringList() << "*");
}



/**
 * @brief Request a list of current tokens from the server.
 * The signal #tokensReceived() will be emitted when the answer is received.
 */
void AdminConnection::retrieveTokens()
{
  writeCommand( "TKN", QStringList() << "*" );
}



/**
 * @brief Write a command to the server.
 *
 * @param cmd The command to send.
 * @param args The arguments to send. MUST NOT contain payload size or
 * transmission ID.
 * @param payload The payload to send. Is only sent if isPayloadCommand
 * returns true for the given cmd.
 */
void AdminConnection::writeCommand( const QString &cmd,
                                    const QStringList &args,
                                    const QByteArray &payload )
{
  QByteArray arguments;
  foreach( QString arg, args )
  {
    Q_ASSERT( !arg.contains( ' ' ) );
    Q_ASSERT( !arg.contains( '\n' ) );
    Q_ASSERT( !arg.contains( '\r' ) );
    arguments.append( arg.toUtf8() + ' ' );
  }

  // remove the last space
  arguments.remove( arguments.length() - 1, 1 );

  if( isPayloadCommand( cmd ) )
  {
    if( arguments.size() > 0 )
    {
      arguments.append( ' ' );
    }
    arguments.append( QByteArray::number( payload.size() ) );
  }

  // Write the command itself
  QByteArray toSend = cmd.toAscii() + ' '
         + QByteArray::number( ++lastTrId_ ) + ' '
         + arguments + "\r\n";
  qDebug() << "Sending: " << toSend;
  write( toSend );

  // Write its payload
  if( isPayloadCommand( cmd ) )
  {
    write( payload );
  }
}
