/***************************************************************************
    kmesstestserveradmin.cpp - KMess Testing MSN Server administration program
                             -------------------
    begin                : Sat March 28, 2009
    copyright            : (C) 2009 by Valerio Pilo, Sjors Gielen
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <stdlib.h>

#include <QDebug>
#include <QDateTime>
#include <QMessageBox>
#include "../util/shared.h"

#include "editcontactdialog.h"
#include "kmesstestserveradmin.h"
#include "adminconnection.h"

// Constructor
KMessTestServerAdmin::KMessTestServerAdmin( AdminConnection *ac, QWidget *parent )
: QMainWindow(parent)
, Ui::KMessTestServerAdmin()
, adminConnection_( ac )
{
  Q_ASSERT( adminConnection_->isOpen() );
  qDebug() << "Initializing UI...";

  // Set up the dialog
  QWidget *mainWidget = new QWidget( this );
  setupUi( mainWidget );
  setCentralWidget( mainWidget );
  setWindowTitle( tr( "KMess Testing MSN Server Administration Interface" ) );
  setWindowIcon( QIcon(":/icons/kmesstestserver.png") );
  show();

  // Give the connection log displayers their names (cosmetic)
  dispatchConnectionLogs_->setLogName( "Dispatch" );
  notificationConnectionLogs_->setLogName( "Notification" );
  soapConnectionLogs_->setLogName( "SOAP" );

  // Quit the administration panel when this dialog is closed
  setAttribute( Qt::WA_DeleteOnClose );
  connect( closeButton_, SIGNAL( clicked() ),
           this,         SLOT  (   close() ) );

  // Populate the window's pages
  mainPageIcon_         = new QListWidgetItem( QIcon(":/icons/status.png"),             tr("Main"),         pages_ );
  dispatchPageIcon_     = new QListWidgetItem( QIcon(":/icons/dispatchserver.png"),     tr("Dispatch"),     pages_ );
  notificationPageIcon_ = new QListWidgetItem( QIcon(":/icons/notificationserver.png"), tr("Notification"), pages_ );
  soapPageIcon_         = new QListWidgetItem( QIcon(":/icons/soapserver.png"),         tr("SOAP"),         pages_ );
  tokenPageIcon_        = new QListWidgetItem( QIcon(":/icons/token.png"),              tr("Authentication tokens"), pages_ );
  contactsPageIcon_     = new QListWidgetItem( QIcon(":/icons/contacts.png"),
        tr("Contacts"),     pages_ );

  connect( pages_, SIGNAL( itemActivated(QListWidgetItem*) ),
           this,   SLOT  (    changePage(QListWidgetItem*) ) );
  pages_->setCurrentRow( 0 );

  // Connect the connections lists' signals (to allow choosing a connection)
  connect( tokens_,                  SIGNAL(    itemActivated(QListWidgetItem*) ),
           this,                     SLOT  ( selectConnection(QListWidgetItem*) ) );
  connect( contacts_,                SIGNAL(    itemActivated(QListWidgetItem*) ),
           this,                     SLOT  ( selectConnection(QListWidgetItem*) ) );

  // Connect the contact add/edit/delete buttons
  connect( addContactButton_,    SIGNAL(  clicked() ),
           this,                 SLOT(   editUser() ) );
  connect( editContactButton_,   SIGNAL(  clicked() ),
           this,                 SLOT(   editUser() ) );
  connect( deleteContactButton_, SIGNAL(  clicked() ),
           this,                 SLOT(   editUser() ) );
  connect( tokenRefreshButton_,  SIGNAL(         clicked() ),
           adminConnection_,     SLOT(    retrieveTokens() ) );
  connect( contactsRefreshButton_, SIGNAL(       clicked() ),
           adminConnection_,     SLOT(  retrieveContacts() ) );

  connect( adminConnection_, SIGNAL( logMessage(QString,QDateTime,
                                                LogType,QString) ),
           this,             SLOT(    statusLog(QString,QDateTime,
                                                LogType,QString) ) );
  connect( adminConnection_, SIGNAL( tokensReceived( const QHash<int,
                                                       QString>& ) ),
           this,             SLOT(   tokensReceived( const QHash<int,
                                                       QString>& ) ) );
  connect( adminConnection_, SIGNAL( tokenInfoReceived( int,
                                            const QList<QByteArray>& ) ),
           this,             SLOT(   tokenInfoReceived( int,
                                            const QList<QByteArray>& ) ) );
}



// Destructor
KMessTestServerAdmin::~KMessTestServerAdmin()
{
}



// Show the "edit contact" or "confirm delete account" dialog
void KMessTestServerAdmin::editUser()
{
  int accountid = contacts_->currentRow();
  QString handle;

  if( accountid >= 0 && accountid < accounts_.size()
  &&  sender() != addContactButton_ )
  {
    handle = QString::number(accountid).append("@unknown");
  }

  if( handle.size() == 0 && sender() != addContactButton_ )
  {
    return;
  }

  if( sender() == deleteContactButton_ )
  {
    //QString handle( account->getProperty( "handle" ).toString() );
    if( QMessageBox::question( this, tr("Are you sure?"),
          tr("Are you sure you want to delete the account %1?").arg( handle ),
          QMessageBox::Yes | QMessageBox::Cancel,
          QMessageBox::Cancel ) == QMessageBox::Yes )
    {
      qDebug() << "Deleting account " << handle;
      #warning LiveService::instance()->deleteAccount( handle );
    }
    else
    {
      qDebug() << "Not deleting account " << handle;
    }
    return;
  }

  EditContactDialog *dialog = new EditContactDialog( handle );
  dialog->exec();

  return;
}



// Switch to another connection in a server page
void KMessTestServerAdmin::selectConnection( QListWidgetItem *newConnection )
{
  QTextBrowser *browser;
  QString       logString;
  QListWidget  *parent = newConnection->listWidget();
  int           connectionId = parent->row( newConnection );

  if( parent == tokens_ )
  {
    browser = tokenInfo_;
    #warning logString = tokenManager_->getHtmlTokenInfo( connectionId );
    logString = "[[unknown]]";
  }
  else if( parent == contacts_ )
  {
    browser = contactInfo_;
    if( accounts_[ connectionId ] == 0 )
    {
      logString = "This account has been deleted.";
    }
    else
    {
      #warning logString = LiveService::instance()->getHtmlAccountInfo( accounts_[ connectionId ] );
      logString = "[[unknown]]";
    }
  }
  else
  {
    qDebug() << "selectConnection() invoked from an unknown caller!";
    return;
  }

  browser->setHtml( logString );
}

void KMessTestServerAdmin::tokensReceived( const QHash<int, QString> &t )
{
  qDebug() << t.size() << "tokens received!";
}

void KMessTestServerAdmin::tokenInfoReceived( int id, const QList<QByteArray> &t )
{
}



// Switch to another page
void KMessTestServerAdmin::changePage( QListWidgetItem *newPage )
{
  if( newPage == mainPageIcon_ )
  {
    container_->setCurrentIndex( 0 );
  }
  else if( newPage == dispatchPageIcon_ )
  {
    container_->setCurrentIndex( 1 );
  }
  else if( newPage == notificationPageIcon_ )
  {
    container_->setCurrentIndex( 2 );
  }
  else if( newPage == soapPageIcon_ )
  {
    container_->setCurrentIndex( 3 );
  }
  else if( newPage == tokenPageIcon_ )
  {
    container_->setCurrentIndex( 4 );
  }
  else if( newPage == contactsPageIcon_ )
  {
    container_->setCurrentIndex( 5 );
  }
  else
  {
    qDebug() << "Invalid page selected!";
  }
}



/**
 * @brief Initialize the KMessTestServerAdmin.
 *
 * This method will request a list of log messages and current connections from
 * the server, to fill its UI.
 */
void KMessTestServerAdmin::initialize()
{
  // Request 100 last log messages
  adminConnection_->retrieveLogMessages( 100 );
  adminConnection_->retrieveConnectionLogs( 25 );

  // Request current connections
  adminConnection_->retrieveCurrentConnections();
}



// Slot for servers to add messages
void KMessTestServerAdmin::statusLog( const QString &sender,
  const QDateTime &date, LogType logType, const QString &message )
{
  QString htmlMessage;

  // See if it's a known type that should go somewhere else:
  int connectionId = -1;
  int connectionIdIndex = sender.indexOf( "#" );
  if( connectionIdIndex != -1 )
  {
    connectionId = sender.mid( connectionIdIndex + 1 ).toInt();
  }
  if( sender.startsWith( "DispatchConnection" ) )
  {
    statusLog( message, connectionId, TYPE_DISPATCH, logType, sender, date );
    return;
  }
  else if( sender.startsWith( "NotificationConnection" ) )
  {
    statusLog( message, connectionId, TYPE_NS, logType, sender, date );
    return;
  }
  else if( sender.startsWith( "SwitchboardConnection" ) )
  {
    statusLog( message, connectionId, TYPE_SB, logType, sender, date );
    return;
  }
  else if( sender.startsWith( "SoapConnection" ) )
  {
    statusLog( message, connectionId, TYPE_SOAP, logType, sender, date );
    return;
  }

  // It's an unknown kind of message, so put it in the "generic" log.

  QString divColor;
  switch( logType )
  {
    case TYPE_NORMAL:
      divColor = "black";
      break;

    case TYPE_STATUS:
    case TYPE_DEBUG:
      divColor = "gray";
      break;

    case TYPE_ERROR:
    default:
      divColor = "#900";
      break;

    case TYPE_INCOMING_TRAFFIC:
      divColor = "#090";
      break;

    case TYPE_OUTGOING_TRAFFIC:
      divColor = "#009";
      break;

    case TYPE_OUTGOING_ERROR:
      divColor = "#990";
      break;
  }

  htmlMessage = "<hr/>\n"
                "<span style=\"color:silver;font-size:small;\"> "
              + Shared::htmlEscape( date.toString() )
              + " - " + Shared::htmlEscape( sender )
              + "</span><br/>\n"
                "<div style=\"color:" + divColor + ";\">\n"
              + Shared::htmlEscape( message )
              + "</div>\n";

  mainStatus_->insertHtml( htmlMessage );
}



// Add a log message to the server status GUI (added to a child item)
void KMessTestServerAdmin::statusLog( const QString &message, int connectionId,
                          ConnectionType connectionType, LogType logType,
                          const QString &origSource, const QDateTime &date )
{
  ConnectionLogList *connectionsList;

  switch( connectionType )
  {
    case TYPE_DISPATCH:
      connectionsList = dispatchConnectionLogs_;
      break;

    case TYPE_NS:
      connectionsList = notificationConnectionLogs_;
      break;

    case TYPE_SB:
      // connectionsList = switchboardConnectionLogs_;
      return;

    case TYPE_SOAP:
      connectionsList = soapConnectionLogs_;
      break;

    default:
      qDebug() << "statusLog() invoked using unknown connectionType! Message:";
      qDebug() << origSource << " (" << connectionType << connectionId << "): "
               << message;
      return;
  }

  // The list of connection doesn't have this one, create it
  if( !connectionsList->hasConnectionId( connectionId ) )
  {
    connectionsList->addConnectionLog( QDateTime::currentDateTime(),
                                       connectionId );
  }

  QString divColor;
  switch( logType )
  {
    case TYPE_NORMAL:
      divColor = "black";
      break;

    case TYPE_STATUS:
    case TYPE_DEBUG:
      divColor = "gray";
      break;

    case TYPE_ERROR:
    default:
      divColor = "#900";
      break;

    case TYPE_INCOMING_TRAFFIC:
      divColor = "#090";
      break;

    case TYPE_OUTGOING_TRAFFIC:
      divColor = "#009";
      break;

    case TYPE_OUTGOING_ERROR:
      divColor = "#990";
      break;
  }

  QString htmlMessage = "<hr/>\n"
                "<span style=\"color:silver;font-size:small;\"> "
              + Shared::htmlEscape( date.toString() )
              + " - " + Shared::htmlEscape( origSource )
              + ": </span><br/>\n"
                "<div style=\"color:" + divColor + ";\">\n"
              + Shared::htmlEscape( message )
              + "</div>\n";

  connectionsList->addToLog( connectionId, htmlMessage );

  // also send it to the debug output
  qDebug() << origSource << ": " << message;
}

