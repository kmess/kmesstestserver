/***************************************************************************
   connectionloglist.h - Connection logs in a list
                             -------------------
    begin                : Thu Aug 5, 2010
    copyright            : (C) 2010 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CONNECTIONLOGLIST_H
#define CONNECTIONLOGLIST_H

#include <QWidget>
#include <QDateTime>
#include <QHash>

namespace Ui {
    class ConnectionLogList;
}

class QListWidgetItem;

class ConnectionLogList : public QWidget {
    Q_OBJECT

public:
    ConnectionLogList(const QString &serverLogName = QString(), QWidget *parent = 0);
    ~ConnectionLogList();
    void addConnectionLog( const QDateTime &date, int connectionId );
    void addToLog( int connectionId, const QString &htmlMessage );
    void setLogName( const QString &serverLogName );
    bool hasConnectionId( int connectionId ) const;

protected:
    void changeEvent(QEvent *e);

private slots:
    void selectConnection( QListWidgetItem *newConnection );

private:
    Ui::ConnectionLogList *ui;
    QHash<int,int> connectionIdToIndex_;
    QHash<int,QString> connectionIdToLogs_;
};

#endif // CONNECTIONLOGLIST_H
