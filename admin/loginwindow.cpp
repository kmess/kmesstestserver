#include <QtCore/QDebug>

#include "loginwindow.h"
#include "ui_loginwindow.h"
#include "adminconnection.h"
#include "kmesstestserveradmin.h"

#define NORMAL_BORDER_SS "QLineEdit { border: 1px solid black; }"
#define ERROR_BORDER_SS  "QLineEdit { border: 1px solid red;   }"

LoginWindow::LoginWindow(QWidget *parent)
: QDialog(parent)
, m_ui(new Ui::LoginWindow)
, ac_( 0 )
, ignoreClose_( false )
{
  m_ui->setupUi(this);

  m_ui->hostName->setStyleSheet(NORMAL_BORDER_SS);
  m_ui->handle  ->setStyleSheet(NORMAL_BORDER_SS);
  m_ui->password->setStyleSheet(NORMAL_BORDER_SS);
}

LoginWindow::~LoginWindow()
{
  delete m_ui;
  delete ac_;
  delete adm_;
}

void LoginWindow::accept()
{
  QString hostName = m_ui->hostName->text();
  QString handle   = m_ui->handle  ->text();
  QString password = m_ui->password->text();
  int     port     = 1865;

  m_ui->hostName->setStyleSheet(NORMAL_BORDER_SS);
  m_ui->handle  ->setStyleSheet(NORMAL_BORDER_SS);
  m_ui->password->setStyleSheet(NORMAL_BORDER_SS);

  bool incorrect = false;
  if( hostName.isEmpty() )
  {
    m_ui->hostName->setStyleSheet(ERROR_BORDER_SS);
    incorrect = true;
  }
  if( handle.isEmpty() || handle.contains( " " ) )
  {
    m_ui->handle->setStyleSheet(ERROR_BORDER_SS );
    incorrect = true;
  }

  if( hostName.count( ":" ) > 1 )
  {
    m_ui->hostName->setStyleSheet(ERROR_BORDER_SS);
    incorrect = true;
  }
  else if( hostName.count( ":" ) == 1 )
  {
    int index       = hostName.indexOf( ":" );
    QString rawPort = hostName.mid( index + 1 );
    bool ok         = true;
    port            = rawPort.toInt( &ok );
    hostName        = hostName.left( index );

    qDebug() << "rawPort=" << rawPort << ";hostName="<<hostName;

    if( !ok || port < 0 || port > 65535 )
    {
      m_ui->hostName->setStyleSheet(ERROR_BORDER_SS);
      incorrect = true;
    }
  }

  if( incorrect )
  {
    m_ui->status->setText( "<html><font color=\"red\">Please check your input.</font></html>" );
    return;
  }

  if( ac_ != 0 )
  {
    delete ac_;
    ac_ = 0;
  }

  m_ui->status->setText( "Resolving..." );
  ac_ = new AdminConnection();

  connect( ac_,  SIGNAL( connected()                             ),
           this, SLOT(   connected()                             ) );
  connect( ac_,  SIGNAL(     error(QAbstractSocket::SocketError) ),
           this, SLOT(       error(QAbstractSocket::SocketError) ) );
  connect( ac_,  SIGNAL( hostFound()                             ),
           this, SLOT(   hostFound()                             ) );
  connect( ac_,  SIGNAL(        authenticationNeeded()           ),
           this, SLOT(            sendAuthentication()           ) );
  connect( ac_,  SIGNAL(     authenticationSucceeded()           ),
           this, SLOT(   slotAuthenticationSucceeded()           ) );
  connect( ac_,  SIGNAL(        authenticationFailed()           ),
           this, SLOT(      slotAuthenticationFailed()           ) );
  connect( ac_,  SIGNAL(               protocolError(QString)    ),
           this, SLOT(                 protocolError(QString)    ) );

  handle_ = handle;
  password_ = password;

  ac_->connectToHost( hostName, port );
}

void LoginWindow::slotAuthenticationSucceeded()
{
  m_ui->status->setText( "" );
  adm_ = new KMessTestServerAdmin( ac_, this );
  hide();
  adm_->initialize();
}

void LoginWindow::slotAuthenticationFailed()
{
  m_ui->status  ->setText( "Authentication failed." );
  m_ui->handle  ->setStyleSheet(ERROR_BORDER_SS);
  m_ui->password->setStyleSheet(ERROR_BORDER_SS);
  ignoreClose_ = true;
}

void LoginWindow::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void LoginWindow::connected()
{
  m_ui->status->setText( "Authenticating..." );
}

void LoginWindow::hostFound()
{
  m_ui->status->setText( "Connecting..." );
}

void LoginWindow::error(QAbstractSocket::SocketError)
{
  if( ignoreClose_ )
  {
    ignoreClose_ = false;
    return;
  }
  m_ui->status->setText( ac_->errorString() );
  delete ac_;
  ac_ = 0;
}

void LoginWindow::sendAuthentication()
{
  ac_->authenticate( handle_, password_ );
}

void LoginWindow::protocolError(const QString &msg)
{
  m_ui->status->setText( msg );
  qDebug() << "Protocol error:" << msg;
  ignoreClose_ = true;
}
