/***************************************************************************
             editcontactdialog.cpp - Dialog to change a contact's settings
                             -------------------
    begin                : Fri Aug 7, 2009
    copyright            : (C) 2009 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <QDebug>
#include <QPushButton>
#include <QAbstractButton>
#include <QInputDialog>
#include <QMessageBox>

#include "editcontactdialog.h"

// Constructor
EditContactDialog::EditContactDialog( const QString &handle, QWidget *parent )
: QDialog( parent )
, handle_( handle )
{
  setupUi( this );

  // Set some window settings
  setAttribute( Qt::WA_DeleteOnClose );
  setSizeGripEnabled( true );

  // some GUI changes
  saveAsNew_ = buttonBox_->addButton( tr("Save as new"), QDialogButtonBox::YesRole );

  // seed the right values
  if( handle.isEmpty() )
  {
    handleEdit_->setEnabled( true );
  }
  else
  {
    #warning handleEdit_->setText( account->getProperty( "handle" ).toString() );
    #warning passwordEdit_->setText( account->getProperty( "password" ).toString() );
    #warning friendlyNameEdit_->setText( account->getProperty( "friendly" ).toString() );
    #warning cidEdit_->setText( account->getProperty( "cid" ).toString() );
    #warning if( account->getProperty( "verified" ).toString() == "0" )
    handleEdit_->setText( handle );
    if( true )
    {
      verified_->setChecked( true );
    }
    else
    {
      verified_->setChecked( false );
    }
  }

  connect( buttonBox_, SIGNAL( clicked( QAbstractButton* ) ),
           this,       SLOT(   clicked( QAbstractButton* ) ) );
}



// Destructor
EditContactDialog::~EditContactDialog()
{}



void EditContactDialog::clicked( QAbstractButton *button )
{
  QString handle;

  if( button == buttonBox_->button( QDialogButtonBox::Save ) )
  {
    qDebug() << "Saving on top of account " << handle_;
    handle = handle_;
  }
  else if( button == buttonBox_->button( QDialogButtonBox::Cancel ) )
  {
    qDebug() << "Cancelling";
  }
  else if( button == saveAsNew_ )
  {
    bool ok = false;
    if( handle_.isEmpty() )
    {
      handle = handleEdit_->text();
      ok = true;
    }
    else
    {
      handle = QInputDialog::getText( this, tr("New handle"), tr("Handle for the new user:"), QLineEdit::Normal, "", &ok );

      #warning if( ok && ( handle.isEmpty() || LiveService::instance()->hasAccount( handle ) ) )
      if( ok && ( handle.isEmpty() || true ) )
      {
        QMessageBox::critical( this, tr("Cannot add account"), tr("Can not add that account: The handle is incorrect or already exists.") );
        ok = false;
      }
    }

    if( !ok )
    {
      handle = "";
    }
    else
    {
      qDebug() << "Saving as new account.";
    }
  }

#warning
#if 0
  if( account )
  {
    account->setProperty( "password", passwordEdit_->text() );
    account->setProperty( "friendly", friendlyNameEdit_->text() );

    if( verified_->isChecked() )
    {
      account->setProperty( "verified", 0 );
    }
    else
    {
      account->setProperty( "verified", 1 );
    }
  }
#endif

  hide();
}
