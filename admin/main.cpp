/***************************************************************************
                    main.cpp - KMess Testing MSN Server
                             -------------------
    begin                : Sat March 28, 2009
    copyright            : (C) 2009 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "loginwindow.h"

#include <QApplication>


int main( int argc, char **argv )
{
  QApplication app( argc, argv );

  app.setApplicationName("kmesstestserveradmin");
  app.setApplicationVersion("1.0");

  // Open the login dialog, exit when it closes
  LoginWindow *lw = new LoginWindow();
  lw->show();
  lw->raise();

  return app.exec();

  delete lw;
}


