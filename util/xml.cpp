/***************************************************************************
                          xml.cpp -  XML handling functions
                             -------------------
    begin                : Sun Jul 24 2005
    copyright            : (C) 2005 by Diederik van der Boor
    email                : vdboor --at-- codingdomain.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "xml.h"

#include <QStringList>
#include <QTextStream>




// Helper function, create an element with some text within
QDomElement Xml::createTextElement( QDomDocument &document, const QString &tagName, const QString &value )
{
  QDomElement newElement( document.createElement( tagName ) );
  QDomText elementText( document.createTextNode( value ) );

  newElement.appendChild( elementText );
  return newElement;
}



// Helper function, get a specific node
QDomNode Xml::getNode( const QDomNode &rootNode, const QString &path )
{
/*
#ifdef KMESSTEST
  KMESS_ASSERT( ! rootNode.isNull() );
  KMESS_ASSERT( ! path.isEmpty()    );
#endif
*/

  const QStringList& pathItems( path.split( "/", QString::SkipEmptyParts ) );
  QDomNode           childNode( rootNode.namedItem( pathItems[0] ) );  // can be a null node

  int i = 1;
  while( i < pathItems.count() )
  {
    if( childNode.isNull() )
    {
      break;
    }

    childNode = childNode.namedItem( pathItems[ i ] );
    i++;  // not using for loop so i is always correct for kDebug() below.
  }
/*
#ifdef KMESSDEBUG_XMLFUNCTIONS
  QString message( "reading node \"" + rootNode.nodeName() + "/" + pathItems.join("/") + "\": "
                   + ( ! childNode.isNull() ? "found" : "not found: " + pathItems[ i - 1 ] ) );
  kDebug() << message.toLatin1().data();
#endif
*/

  return childNode;
}



// Helper function, get the attribute text of a node
QString Xml::getNodeAttribute( const QDomNode &node, const QString &attribute )
{
/*
#ifdef KMESSTEST
  KMESS_ASSERT( ! node.isNull()       );
  KMESS_ASSERT( ! attribute.isEmpty() );
#endif
*/
  // Writing this is not funny
  return node.attributes().namedItem( attribute ).toAttr().value();
}



// Helper function, get a specific child node
QDomNode Xml::getNodeChildByKey( const QDomNodeList &childNodes, const QString &keyTagName, const QString &keyValue )
{
/*
#ifdef KMESSTEST
  KMESS_ASSERT( ! keyTagName .isEmpty() );
  KMESS_ASSERT( ! keyValue   .isEmpty() );
#endif
*/
  for( int i = 0; i < childNodes.count(); i++ )
  {
//    kDebug() << "node " << childNodes.item(i).nodeName() << "/" << keyTagName
//              << "="     << childNodes.item(i).namedItem(keyTagName).toElement().text() << " == " << keyValue << "?";

      // If the node has an childname with a certain value... e.g. <childNodes> <item><name>value</name></item> .. </childNodes>
    if( childNodes.item( i ).namedItem( keyTagName ).toElement().text() == keyValue)
    {
        // Return the node
      return childNodes.item( i );
    }
  }

  // Return a null node (is there a better way?)
  return childNodes.item( childNodes.count() );
}



// Helper function, get the text value of a node
QString Xml::getNodeValue( const QDomNode &rootNode, const QString &path )
{
/*
#ifdef KMESSTEST
  KMESS_ASSERT( ! rootNode.isNull() );
  KMESS_ASSERT( ! path.isEmpty()    );

  // Added code to avoid more assertion errors, and trace the cause.
  if( rootNode.isNull() )
  {
    kWarning() << "Attempted to request '" << path << "' on null root node.";
    return QString();
  }
#endif
*/

  // Because writing node.namedItem("childItem").namedItem("child2").toElement().text() is not funny.
  //return getNode( rootNode, path ).firstChild().toText().data();
  return getNode( rootNode, path ).toElement().text();
}



// Helper function, get the source XML of a node.
QString Xml::getSource( const QDomNode &node, int indent )
{
  QString source;

  QTextStream textStream( &source, QIODevice::WriteOnly );
  node.save( textStream, indent );

  return source;
}


