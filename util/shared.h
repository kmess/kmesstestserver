/***************************************************************************
               shared.h  -  Shared use methods
                             -------------------
    begin                : Thu May 8 2008
    copyright            : (C) 2008 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SHARED_H
#define SHARED_H

#include <QObject>
#include <QString>



/**
 * @brief Shared globally useful methods
 *
 * This class is just a container for many methods which are useful throughout the entire
 * application.
 *
 * @author Valerio Pilo <valerio@kmess.org>
 * @ingroup Utils
 */
class Shared
{

  public: // Public static methods
    // Create a HMACSha1 hash
//     static QByteArray      createHMACSha1( const QByteArray &keyForHash, const QByteArray &secret );
    // Return a derived key with HMACSha1 algorithm
//     static QByteArray      deriveKey( const QByteArray &keyToDerive, const QByteArray &magic );
    // Generate a random GUID.
    static QString         generateGUID();
    // Generate an random number to use as ID
    static quint32         generateID();
    // Converts a string with HTML to one with escaped entities
    static QString        &htmlEscape( QString &string );
    // Converts a string constant with HTML to one with escaped entities
    static QString         htmlEscape( const QString &string );
    // Converts a string with escaped entities to one with HTML
    static QString        &htmlUnescape( QString &string );
    // Converts a string constant with escaped entities to one with HTML
    static QString         htmlUnescape( const QString &string );

};

#endif
