/***************************************************************************
                          xml.h -  XML handling functions
                             -------------------
    begin                : Sun Jul 24 2005
    copyright            : (C) 2005 by Diederik van der Boor
    email                : vdboor --at-- codingdomain.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef XML_H
#define XML_H

#include <QDomNode>


// Forward declarations
class QDomNodeList;



/**
 * @brief Helper functions to make the use of QDom easier.
 *
 * @author Diederik van der Boor
 * @ingroup Utils
 */
class Xml
{
  public:
    // Helper function, create an element with some text within
    static QDomElement createTextElement( QDomDocument &document, const QString &tagName, const QString &value );
    // Helper function, get a specific node
    static QDomNode  getNode(const QDomNode &rootNode, const QString &path);
    // Helper function, get the attribute text of a node
    static QString   getNodeAttribute(const QDomNode &node, const QString &attribute);
    // Helper function, get a specific child node
    static QDomNode  getNodeChildByKey(const QDomNodeList &childNodes,
                                                  const QString &keyTagName, const QString &keyValue);
    // Helper function, get the text value of a node
    static QString   getNodeValue(const QDomNode &rootNode, const QString &path);
    // Helper function, get the source XML of a node.
    static QString   getSource( const QDomNode &node, int indent = 0 );
};

#endif
