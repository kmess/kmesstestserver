/***************************************************************************
             shared.cpp  -  Shared use methods
                             -------------------
    begin                : Thu May 8 2008
    copyright            : (C) 2008 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "shared.h"

#include <stdlib.h>



/*
// Create a HMACSha1 hash
QByteArray Shared::createHMACSha1 ( const QByteArray &keyForHash , const QByteArray &secret )
{
  // The algorithm is definited into RFC 2104
  int blocksize = 64;
  QByteArray key( keyForHash );
  QByteArray opad( blocksize, 0x5c );
  QByteArray ipad( blocksize, 0x36 );

  // If key size is too larg, compute the hash
  if( key.size() > blocksize )
  {
    key = QCryptographicHash::hash( key, QCryptographicHash::Sha1 );
  }

  // If too small, pad with 0x00
  if( key.size() < blocksize )
  {
    key += QByteArray( blocksize - key.size() , 0x00 );
  }

  // Compute the XOR operations
  for(int i=0; i < key.size() - 1; i++ )
  {
    ipad[i] = (char) ( ipad[i] ^ key[i] );
    opad[i] = (char) ( opad[i] ^ key[i] );
  }

  // Append the data to ipad
  ipad += secret;

  // Compute result: hash sha1 of ipad and append the data to opad
  opad += QCryptographicHash::hash( ipad, QCryptographicHash::Sha1 );;

  // Return array contains the result of HMACSha1
  return QCryptographicHash::hash( opad, QCryptographicHash::Sha1 );
}



// Return a derived key with HMACSha1 algorithm
QByteArray Shared::deriveKey ( const QByteArray &keyToDerive, const QByteArray &magic )
{
  // create the four hashes needed for the implementation.
  QByteArray hash1, hash2, hash3, hash4, temp;
  QByteArray key(keyToDerive);

  // append the magic string.
  temp.append ( magic );

  // create the HMAC Sha1 hash and assign it.
  hash1 = createHMACSha1 ( key, temp );
  temp.clear();

  // append the first hash with the magic string.
  temp.append ( hash1 + magic );

  // create the HMAC Sha1 hash and assign it.
  hash2 = createHMACSha1 ( key, temp );
  temp.clear();

  // append the first hash.
  temp.append ( hash1 );

  // create the HMAC Sha1 hash and assign it.
  hash3 = createHMACSha1 ( key, temp );
  temp.clear();

  // assign the third hash with the magic string.
  temp.append ( hash3 + magic );
  // create the HMAC Sha1 hash and assign it.
  hash4 = createHMACSha1 ( key, temp );
  temp.clear();

  // return the second hash with the first four bytes of the fourth hash.
  return hash2 + hash4.left ( 4 );
}
*/



/**
 * @brief Generate a random GUID.
 *
 * This is used in MSNP2P for the Call ID and Branch ID.
 *
 * @return A randomly generated GUID value.
 */
QString Shared::generateGUID()
{
  // This code is based on Kopete, but much shorter.
  QString guid( "{"
                + QString::number((rand() & 0xAAFF) + 0x1111, 16)
                + QString::number((rand() & 0xAAFF) + 0x1111, 16)
                + "-"
                + QString::number((rand() & 0xAAFF) + 0x1111, 16)
                + "-"
                + QString::number((rand() & 0xAAFF) + 0x1111, 16)
                + "-"
                + QString::number((rand() & 0xAAFF) + 0x1111, 16)
                + "-"
                + QString::number((rand() & 0xAAFF) + 0x1111, 16)
                + QString::number((rand() & 0xAAFF) + 0x1111, 16)
                + QString::number((rand() & 0xAAFF) + 0x1111, 16)
                + "}" );
  return guid.toUpper();
}



/**
 * @brief Generate an random number to use as ID.
 *
 * For use in MSNP2P, the value will not be below 4
 *
 * @return A random value between 4 and 4294967295.
 */
quint32 Shared::generateID()
{
  return ( rand() & 0x00FFFFFC );
}



/**
 * Converts a string with HTML to one with escaped entities
 *
 * Only the main HTML control characters are escaped; the string is made suitable for
 * insertion within an HTML tag attribute.
 * Neither KDE nor Qt have escape/unescape methods this thorough, they only replace the <>&. Annoying.
 *
 * @param string  The string to modify
 * @return        A reference to the modified string
 */
QString &Shared::htmlEscape( QString &string )
{
  string.replace( ";", "&#59;" )
        .replace( "&", "&amp;" )
        .replace( "&amp;#59;", "&#59;" ) // oh god :(
        .replace( "<", "&lt;"  )
        .replace( ">", "&gt;"  )
        .replace( "'", "&#39;" )
        .replace( '"', "&#34;" );  // NOTE: by not using &quot; this result is also usable for XML.

  return string;
}



/**
 * Converts a string constant with HTML to one with escaped entities
 *
 * This version is suitable for use with string constants, as it creates a copy of the original string.
 *
 * @param string  The string to modify
 * @return        Another string with the escaped contents
 */
QString Shared::htmlEscape( const QString &string )
{
  QString copy( string );
  return htmlEscape( copy );
}



/**
 * Converts a string with escaped entities to one with HTML
 *
 * Only the main HTML control characters are unescaped; the string is reverted back to its
 * original state.
 * Neither KDE nor Qt have HTML to text decoding methods. Annoying.
 *
 * @param string  The string to modify
 * @return        A reference to the modified string
 */
QString &Shared::htmlUnescape( QString &string )
{
  string.replace( "&#34;", "\"" )
        .replace( "&#39;", "'"  )
        .replace( "&gt;",  ">"  )
        .replace( "&lt;",  "<"  )
        .replace( "&#59;", "&amp;#59;"  )
        .replace( "&amp;", "&"  )
        .replace( "&#59;", ";"  );

  return string;
}

/**
 * Converts a string with escaped entities to one with HTML
 *
 * This version is suitable for use with string constants, as it creates a copy of the original string.
 *
 * @param string  The string to modify
 * @return        Another string with the escaped contents
 */
QString Shared::htmlUnescape( const QString &string )
{
  QString copy( string );
  return htmlUnescape( copy );
}


