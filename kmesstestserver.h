/***************************************************************************
           kmesstestserver.h - KMess Testing MSN Server
                             -------------------
    begin                : Sat March 28, 2009
    copyright            : (C) 2009 by Valerio Pilo, Sjors Gielen
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef KMESSTESTSERVER_H
#define KMESSTESTSERVER_H

#include "logging/loggingobject.h"
#include <QCoreApplication>
#include <QMap>

class MsnServerGroup;
class TokenManager;
struct Token;
class Account;
class QFile;


/**
 * @brief The KMess Testing MSN Server.
 *
 * This is the starting point for the KMess Testing MSN server.
 * It starts a MsnServerGroup object internally to start listening at the various sockets.
 */
class KMessTestServer : public LoggingObject
{
  Q_OBJECT


  public:

    // Constructor
                          KMessTestServer( QString logFile = "" );
    // Destructor
    virtual              ~KMessTestServer();
    // Retrieve a path to the log file
    const QString        &logFile();
    // Set a new log file path (will be automatically reopened)
    void                  setLogFile( const QString &logFile );


  public slots:

    // Add a status message for one of the servers
    void                  statusLog( const LoggingBase *a, const QString &m,
                                     LoggingBase::MessageType type );
    void                  reopenLogFile();


  private slots:

    // Add a new token to the list
    void                  addToken( Token *newToken );
    // Remove a token from the list
    void                  removeToken( Token *oldToken );
    // Add a new user to the list
    void                  addUser( Account *account );
    // Remove a user from the list
    void                  removeUser( Account *account );
    // Edit a user (when the button is clicked)
    //void                  editUser();
    // Shutdown the server
    void                  shutDown();

  private: // Private properties

    // The log filename for this server
    QString               logFileName_;
    // The (open) log QFile for this server
    QFile                *logFile_;
    // List of connection dumps for the Dispatch server
    QMap<int,QString>     dispatchLogs_;
    // List of connection dumps for the Notification server
    QMap<int,QString>     notificationLogs_;
    // The msn server object
    MsnServerGroup       *msnServerGroup_;
    // The token manager
    TokenManager         *tokenManager_;
    // List of connection dumps for the SOAP server
    QMap<int,QString>     soapLogs_;
    // List of accounts
    QList<Account*>       accounts_;

};



#endif
