/***************************************************************************
                    main.cpp - KMess Testing MSN Server
                             -------------------
    begin                : Sat March 28, 2009
    copyright            : (C) 2009 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "kmesstestserver.h"

#include <QCoreApplication>
#include <QStringList>


int main( int argc, char **argv )
{
  QCoreApplication app( argc, argv );

  app.setApplicationName("kmesstestserver");
  app.setApplicationVersion("1.0");

  const QStringList &args = app.arguments();
  QString logFile = "kmesstestserver.log";
  if( args.size() > 1 )
  {
    logFile = args.at(1);
  }

  // Start the kmesstestserver
  KMessTestServer *s = new KMessTestServer( logFile );

  return app.exec();

  delete s;
}


