/***************************************************************************
         liveservice.cpp - Contacts and Settings host
                             -------------------
    begin                : Wed April 1, 2009
    copyright            : (C) 2009 by Valerio Pilo, Sjors Gielen
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "group.h"
#include "membership.h"
#include "liveservice.h"
#include "../util/xml.h"


#include <QDir>
#include <QDomDocument>
#include <QFile>
#include <QTextStream>
#include <QCoreApplication>
#include <QTimer>

// Set last supported config version
#define LAST_CONFIG_VERSION 1

// Initialization of static members
LiveService *LiveService::instance_( 0 );



// Constructor
LiveService::LiveService( QObject *parent )
: LoggingObject( parent )
{
  setObjectName( "LiveService" );
}

bool LiveService::loadConfig()
{
  if( !accounts_.empty() )
  {
    warning( "Tried to initialize LiveService twice!" );
    return false;
  }

  const QString defaultConfig( "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n"
                               "<LiveService>\n"
                               "  <Accounts>\n"
                               "    <account>\n"
                               "      <Properties>\n"
                               "        <handle>test@test.com</handle>\n"
                               "        <password>testing</password>\n"
                               "        <friendly>I am a test account</friendly>\n"
                               "        <verified>0</verified>\n"
                               "        <admin>true</admin>\n"
                               "      </Properties>\n"
                               "    </account>\n"
                               "  </Accounts>\n"
                               "  <Settings>\n"
                               "    <lastcid>0</lastcid>\n"
                               "    <sbstartport>1900</sbstartport>\n"
                               "    <sbendport>2000</sbendport>\n"
                               "    <configversion>1</configversion>\n"
                               "    <hostname>127.0.0.1</hostname>\n"
                               "  </Settings>\n"
                               "</LiveService>\n" );

  QDomDocument configXml;

  QDir home( QDir::home() );
  home.mkpath( ".kmesstestserver" );
  home.cd( ".kmesstestserver" );
  QFile configFile( home.absolutePath() + "/liveservice.xml" );

  if( ! configFile.open( QIODevice::ReadOnly ) )
  {
    warning( "No Live Service configuration file found." );
    configXml.setContent( defaultConfig );
  }
  else
  {
    QString errorMessage;
    int errorLine, errorColumn;
    bool loaded = configXml.setContent( configFile.readAll(), false, &errorMessage, &errorLine, &errorColumn );
    configFile.close();

    if( ! loaded )
    {
      warning( "Live Service configuration loading failed!\n"
               "File name: " + configFile.fileName() + "\n"
               "File operation error: " + configFile.error() + "\n"
               "Error: " + errorMessage + " at line " + errorLine + " column "
               + errorColumn + "\n\n"
               "If you didn't break the file yourself, report a bug. Otherwise,"
               "fix the file. Or, if you want to start from scratch with a new "
               "configuration, delete the file." );

      return false;
    }
  }

  const QDomElement &body( configXml.documentElement() );

  // first load settings
  int numsettings = 0;
  const QDomNodeList& settingsList( body.firstChildElement( "Settings" ).childNodes() );
  for( int i = 0; i < settingsList.count(); i++ )
  {
    QDomElement setting( settingsList.at( i ).toElement() );

    setSetting( setting.tagName(), setting.toElement().text() );
    debug( "Loaded setting " + setting.tagName() + " = " + setting.toElement().text() );

    numsettings++;
  }

  // load a valid lastCid
  int cid = getSetting( "lastCid" ).toInt();
  if( cid < 0 )
  {
    cid = 0;
  }

  // get configuration version
  int configVersion = getSetting( "configVersion" ).toInt();
  if( configVersion < 0 )
  {
    configVersion = 0;
  }
  if( configVersion > LAST_CONFIG_VERSION )
  {
    fatalError( "Error: Configuration version is too high. Shutting down." );
    return false;
  }

  // and then load accounts
  int numaccounts = 0;
  const QDomNodeList& accountsList( body.firstChildElement( "Accounts" ).childNodes() );

  // Memberships will be saved and entered when all accounts are loaded
  QHash<Account*,QDomNodeList> accountMemberships;
  QHash<Account*,QDomNodeList> accountGroups;

  for( int i = 0; i < accountsList.count(); i++ )
  {
    const QDomElement& accountElement( accountsList.item( i ).toElement() );

    Account *account = new Account( "__unset__" ); // handle will be set later
    relayChildLogMessages( account );
    QDomNodeList properties;
    if( configVersion == 0 )
    {
      // load configuration the old way: right under Account
      properties = accountElement.childNodes();
    }
    else if( configVersion == 1 )
    {
      // load configuration the new way: below Account/Properties
      properties = accountElement.firstChildElement( "Properties" ).childNodes();
    }

    for( int index = 0; index < properties.size(); ++index )
    {
      QDomElement property( properties.at( index ).toElement() );
      account->setProperty( property.tagName(), property.toElement().text() );
    }

    if( ! account->getProperty( "cid" ).isValid() )
    {
      // get a new CID
      account->setProperty( "cid", ++cid );
    }
    else if( account->getPropertyInt( "cid" ) > cid )
    {
      // make sure lastCid is at least the highest found CID
      cid = account->getPropertyInt( "cid" );
    }

    if( configVersion == 1 )
    {
      // load memberships and groups
      // they will be processed after all accounts are done loading.
      QDomNodeList memberships      = accountElement.firstChildElement( "Memberships" ).childNodes();
      accountMemberships[ account ] = memberships;
      QDomNodeList groups           = accountElement.firstChildElement( "Groups" ).childNodes();
      accountGroups[ account ]      = groups;
    }

    const QString &handle( account->getProperty( "handle" ).toString() );
    accounts_[ handle.toLower() ] = account;

    emit userAdded( account );
    numaccounts++;
  }

  // and seed the memberships
  QHashIterator<Account*,QDomNodeList> it( accountMemberships );
  while( it.hasNext() )
  {
    it.next();
    Account *account = it.key();

    QDomNodeList memberships = it.value();

    for( int index = 0; index < memberships.size(); ++index )
    {
      QDomElement membership( memberships.at( index ).toElement() );
      QString groupString = membership.attribute( "group" );
      MembershipGroup group;
      if( groupString == "Allow" )
      {
        group = MEMBERSHIP_ALLOW;
      }
      else if( groupString == "Block" )
      {
        group = MEMBERSHIP_BLOCK;
      }
      else if( groupString == "Reverse" )
      {
        group = MEMBERSHIP_REVERSE;
      }
      else if( groupString == "Pending" )
      {
        group = MEMBERSHIP_PENDING;
      }
      else
      {
        warning( "Unknown group string " + groupString + ", skipping this membership." );
        continue;
      }

      debug( "Seeding membership group " + QString::number(group) + " for account " + account->getPropertyString( "handle" ) );
      Membership *mship = account->memberships_[ group ];
      if( mship->members().size() != 0 )
      {
        warning( "Seeding membership but membership group already has " +
                    QString::number(mship->members().size()) + " members!" );
      }

      QDomNodeList members = membership.childNodes();
      debug( "Number of members: " + members.size() );
      for( int mi = 0; mi < members.size(); ++mi )
      {
        QDomElement member = members.at( mi ).toElement();

        QString handle = member.attribute( "handle" );
        debug( "Member: " + handle );

        MembershipInfo *info = new MembershipInfo();
        info->id = member.attribute( "id" ).toInt();
        info->lastChanged = QDateTime::fromString( member.attribute( "lastChanged" ), Qt::ISODate );
        info->joinedDate  = QDateTime::fromString( member.attribute( "joinedDate"  ), Qt::ISODate );

        mship->addMember( getAccount( handle ), info );
      }
    }
  }

  // and the groups
  QHashIterator<Account*,QDomNodeList> git( accountGroups );
  while( git.hasNext() )
  {
    git.next();
    Account *account = git.key();
    QDomNodeList groups = git.value();

    for( int index = 0; index < groups.size(); ++index )
    {
      QDomElement groupElement( groups.at( index ).toElement() );
      QString guid = groupElement.attribute( "guid" );
      QString name = groupElement.attribute( "name" );

      debug( "Group has guid " + guid + "name" + name );
      QDomNodeList members( groupElement.firstChildElement( "Members" ).childNodes() );
      debug( "and " + QString::number(members.size()) + "members." );

      Group *group = new Group( account );
      group->setName( name );
      group->setGuid( guid );

      for( int memberi = 0; memberi < members.size(); ++memberi )
      {
        QDomElement member = members.at( memberi ).toElement();
        QString handle = member.attribute( "handle" );
        Account *newMember = getAccount( handle );
        if( newMember == 0 )
        {
          warning( "Group had member " + handle + " which doesn't exist!" );
        }
        else
        {
          group->addMember( newMember );
        }
      }

      account->groups_[ guid ] = group;
    }
  }

  // and save lastCid and configVersion again
  setSetting( "lastCid", cid );
  setSetting( "configVersion", LAST_CONFIG_VERSION );

  info( "Loaded" + QString::number(numaccounts) + "accounts." );
  info( "Loaded" + QString::number(numsettings) + "settings." );

  return true;
}



// Destructor
LiveService::~LiveService()
{
  QDir home( QDir::home() );
  home.mkpath( ".kmesstestserver" );
  home.cd( ".kmesstestserver" );

  QFile configFile( home.absolutePath() + "/liveservice.xml" );
  if( ! configFile.open( QIODevice::WriteOnly ) )
  {
    fatalError( "Unable to save configuration!" );
    return;
  }

  QTextStream out( &configFile );
  QDomDocument xml;

  xml.setContent( QString( "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n"
        "<LiveService>\n"
        "  <Accounts>\n"
        "  </Accounts>\n"
        "  <Settings>\n"
        "  </Settings>\n"
        "</LiveService>\n" ) );

  QDomElement body( xml.documentElement() );

  // Save all accounts
  QDomNode accountsList( body.firstChildElement( "Accounts" ) );

  int numaccounts = 0;
  foreach( Account *account, accounts_ )
  {
    // Don't save temporary accounts
    if( account->getProperty( "temporary" ).toBool() )
    {
      continue;
    }

    // Update the account because we're shutting down
    account->setProperty( "status", "FLN" );
    account->unsetProperty( "uux" );
    account->unsetProperty( "msnobject" );
    account->unsetProperty( "capabilities" );

    QDomElement accountElement( xml.createElement( "account" ) );
    accountsList.appendChild( accountElement );

    // Save properties
    QDomElement propertiesElement( xml.createElement( "Properties" ) );
    accountElement.appendChild( propertiesElement );

    QHashIterator<QString,QVariant> pit( account->getProperties() );
    while( pit.hasNext() )
    {
      pit.next();

      debug( account->getProperty( "handle" ).toString() + ": Saving property " + pit.key() + "valued" + pit.value().toString() );
      propertiesElement.appendChild( Xml::createTextElement( xml, pit.key(), pit.value().toString() ) );
    }

    // Save memberships
    QDomElement membershipsElement( xml.createElement( "Memberships" ) );
    accountElement.appendChild( membershipsElement );

    QHashIterator<MembershipGroup,Membership*> mit( account->getMembershipGroups() );
    while( mit.hasNext() )
    {
      mit.next();
      QString group;
      if( mit.key() == MEMBERSHIP_ALLOW )
      {
        group = "Allow";
      }
      else if( mit.key() == MEMBERSHIP_BLOCK )
      {
        group = "Block";
      }
      else if( mit.key() == MEMBERSHIP_REVERSE )
      {
        group = "Reverse";
      }
      else if( mit.key() == MEMBERSHIP_PENDING )
      {
        group = "Pending";
      }
      else
      {
        warning( "Unexpected membership type " + QString::number(mit.key()) + ", skipping save" );
        continue;
      }

      debug( account->getProperty( "handle" ).toString() + ": Saving membership group " + group );
      QDomElement membershipElement( xml.createElement( "Membership" ) );
      membershipElement.setAttribute( "group", group );
      membershipsElement.appendChild( membershipElement );

      Membership *mship = mit.value();
      const QList<Account*>& members( mship->members() );
      debug( "Group has " + QString::number(members.size()) + "members." );
      for( int mi = 0; mi < members.size(); ++mi )
      {
        Account *member = members[ mi ];
        debug( "Member: " + member->getPropertyString( "handle" ) );
        MembershipInfo *info = mship->getMembershipInfo( member );

        QDomElement memberElement( xml.createElement( "Member" ) );
        memberElement.setAttribute( "id", info->id );
        // type is commented out, because it's always passport now
        memberElement.setAttribute( "lastChanged", info->lastChanged.toString( Qt::ISODate ) );
        memberElement.setAttribute( "joinedDate", info->joinedDate.toString( Qt::ISODate ) );
        // expiration is commented out, because it's always 0 now
        memberElement.setAttribute( "handle", member->getPropertyString( "handle" ) );

        membershipElement.appendChild( memberElement );
      }
    }

    // And save all groups
    QDomElement groupsElement( xml.createElement( "Groups" ) );
    accountElement.appendChild( groupsElement );

    QHashIterator<QString,const Group*> git( account->getGroups() );
    debug( "Account has " + QString::number(account->getGroups().size()) + "groups!" );
    while( git.hasNext() )
    {
      git.next();
      QString guid = git.value()->guid();
      QString name = git.value()->name();

      debug( account->getProperty( "handle" ).toString() + ": Saving group " + name + "(" + guid + ")" );
      QDomElement groupElement( xml.createElement( "Group" ) );
      groupElement.setAttribute( "guid", guid );
      groupElement.setAttribute( "name", name );
      groupsElement.appendChild( groupElement );

      QDomElement membersElement( xml.createElement( "Members" ) );
      groupElement.appendChild( membersElement );

      const Group *group = git.value();
      const QList<Account*>& members( group->members() );
      debug( "Group has " + QString::number(members.size()) + "members." );
      for( int mi = 0; mi < members.size(); ++mi )
      {
        Account *member = members[ mi ];
        debug( "Member: " + member->getPropertyString( "handle" ) );

        QDomElement memberElement( xml.createElement( "Member" ) );
        memberElement.setAttribute( "handle", member->getPropertyString( "handle" ) );

        membersElement.appendChild( memberElement );
      }
    }

    numaccounts++;
  }

  // only *after* all accounts are done saving, of course:
  qDeleteAll( accounts_ );
  accounts_.clear();

  // Save all settings
  QDomNode settingsList( body.firstChildElement( "Settings" ) );
  int numsettings = 0;
  QHashIterator<QString,QVariant> it( settings_ );
  while( it.hasNext() )
  {
    it.next();

    debug( "Saving setting" + it.key() + "valued" + it.value().toString() );
    settingsList.appendChild( Xml::createTextElement( xml, it.key(), it.value().toString() ) );

    numsettings++;
  }

  out << xml.toString( 2 );
  out.flush();
  configFile.flush();
  configFile.close();

  info( "Saved" + QString::number(numaccounts) + "accounts and" + QString::number(numsettings) + "settings." );
}



/**
 * @brief Check the database for any errors.
 *
 * This method checks the integrity of the KMessTestServer database. It checks
 * if all user handles are valid and unique, if all account CID's (contact
 * ID's) are unique, if all accounts have valid groups and all groups have
 * valid members, if all members of all groups are correctly added into the
 * correct membership groups, and if all Reverse membership groups are also
 * set correctly.
 *
 * @param repair Whether the LiveService should try a repair
 * @returns whether the database is in a correct state, or if repair was
 * true, whether the method could bring it in a correct state again.
 */
bool LiveService::checkDatabase( bool repair )
{
  // TODO implement

  // emit logMessage( "Checking database integrity...", TYPE_WARNING );
  return true;
}



/**
 * @brief Delete an account.
 *
 * The account is deleted from store and so are its groups. The user is also
 * removed from all users in this users' reverse list. When this method is done,
 * all traces of the user account are gone.
 */
void LiveService::deleteAccount( Account *account )
{
  // First remove it from the users' lists
  Membership *reverse = account->getMembershipGroup( MEMBERSHIP_REVERSE );
  QList<Account*> reverseAccounts;
  if( reverse != 0 )
  {
    reverseAccounts = reverse->members();
  }

  QList<Group*> groupsToCheck;
  QList<Membership*> membershipsToCheck;
  for( int i = 0; i < reverseAccounts.size(); ++i )
  {
    Account *toCheck = reverseAccounts[ i ];
    if( toCheck != 0 )
    {
      membershipsToCheck << toCheck->getMembershipGroups().values();
      // Get a non-const group for all objects
      // we are the Live service, we can change the Group objects
      groupsToCheck << toCheck->groups_.values();
    }
  }

  for( int i = 0; i < membershipsToCheck.size(); ++i )
  {
    Membership *toCheck = membershipsToCheck[ i ];
    if( toCheck != 0 && toCheck->isMember( account ) )
    {
      toCheck->removeMember( account );
    }
  }

  for( int i = 0; i < groupsToCheck.size(); ++i )
  {
    Group *toCheck = groupsToCheck[ i ];
    if( toCheck != 0 && toCheck->isMember( account ) )
    {
      toCheck->removeMember( account );
    }
  }

  // TODO: there may still be notification connections which
  // have this contact in the ADL list. They will fail from this moment
  // on. Connected contacts should get some kind of message that an account
  // has been (forcefully) removed from their contact list, or they should
  // be disconnected (something like a XFR NS to the same server will probably
  // do).

  // The account is now deleted from all contacts.
  // We emit the signal that it's going to be deleted:
  emit userDeleted( account );
  // We remove the account from store:
  accounts_.remove( accounts_.key( account ) );
  // And then we delete it
  delete account;
}



/**
 * @brief Delete an account.
 *
 * @see deleteAccount( Account* )
 */
void LiveService::deleteAccount( const QString &handle )
{
  if( !hasAccount( handle ) )
  {
    return;
  }

  deleteAccount( getAccount( handle ) );
}



// Destroy the current live service instance
void LiveService::destroy()
{
  if( instance_ != 0 )
  {
    delete instance_;
    instance_ = 0;
  }
}



// Return an account
Account *LiveService::getAccount( const QString &handle )
{
  if( ! hasAccount( handle ) )
  {
    return 0;
  }

  return accounts_[ handle.toLower() ];
}



// Get some information on an account
QString LiveService::getHtmlAccountInfo( Account *account )
{
  QString admin( "No" );
  if( account->getPropertyBool( "admin" ) )
  {
    admin = "Yes";
  }

  QString info( "<table><tr><th colspan='2'>Account information</th></tr>" );
  info.append( "<tr><th>Handle</th><td>"        + account->getProperty( "handle"   ).toString() + "</td></tr>" );
  info.append( "<tr><th>Administrator</th><td>" + admin + "</td></tr>" );
  info.append( "<tr><th>CID</th><td>"           + account->getProperty( "cid"      ).toString() + "</tr></tr>" );
  info.append( "<tr><th>Status</th><td>"        + account->getProperty( "status"   ).toString() + "</td></tr>" );
  info.append( "<tr><th>Friendly name</th><td>" + account->getProperty( "friendly" ).toString() + "</td></tr>" );
  if( account->getProperty( "verified" ).toString() == "0" )
  {
    info.append( "<tr><th>Verified</th><td>Yes</td></tr>" );
  }
  else
  {
    info.append( "<tr><th>Verified</th><td>No</td></tr>" );
  }
  info.append( "</table>" );
  return info;
}



// Return whether a saved account with a certain handle is registered
bool LiveService::hasAccount( const QString &handle )
{
  if( accounts_.isEmpty() )
  {
    loadConfig();
  }

  return accounts_.contains( handle.toLower() );
}



// Return whether a saved account is registered
bool LiveService::hasAccount( Account *account )
{
  if( accounts_.isEmpty() )
  {
    loadConfig();
  }

  return accounts_.contains( account->getPropertyString( "handle" ).toLower() );
}



// Retrieve or create the current live service instance
LiveService *LiveService::instance()
{
  if( instance_ == 0 )
  {
    instance_ = new LiveService();
  }

  return instance_;
}



// Retrieve a setting value
QVariant LiveService::getSetting( const QString &name ) const
{
  if( !settings_.contains( name.toLower() ) )
  {
    // don't really add it into settings_, as [] does
    return QVariant();
  }
  return settings_[ name.toLower() ];
}



/**
 * @brief add information on a new account.
 *
 * This method adds a new account to the database. If the given handle already
 * exists, the old account is returned.
 *
 * @param handle The new account handle.
 * @param temporary Whether the account will not be saved - ie, when restarting
 *                  the server, there will be no trace left of the account if
 *                  this is set to true. Ignored if the account already exists.
 * @returns the new Account object, or an existing account if the handle
 * already existed.
 */
Account *LiveService::newAccount( const QString &handle, bool temporary )
{
  if( hasAccount( handle ) )
  {
    // already added!
    warning( "Account" + handle + "already exists!\n" );
    return getAccount( handle );
  }

  Account *account = new Account( handle );
  relayChildLogMessages( account );
  account->setProperty( "temporary", temporary );

  // get a new CID
  int cid = getSetting( "lastCid" ).toInt();
  if( cid < 0 )
  {
    cid = 0;
  }
  account->setProperty( "cid", ++cid );
  setSetting( "lastCid", cid );

  accounts_[ handle.toLower() ] = account;
  emit userAdded( account );

  return account;
}



// Change a setting
void LiveService::setSetting( const QString &name, const QVariant &value )
{
  settings_[ name.toLower() ] = value;
}



void LiveService::addMemberToMembershipGroup( Account *account, MembershipGroup group, Account *toAdd )
{
  if( !hasAccount( account ) || !hasAccount( toAdd ) )
  {
    warning( "Warning: addMemberToMembershipGroup got an account that isn't known in the LiveService!" );
    return;
  }

  // If the account also in the pending list, remove it first
  if( group != MEMBERSHIP_PENDING )
  {
    Membership *pending = account->getMembershipGroup( MEMBERSHIP_PENDING );
    if( pending->isMember( toAdd ) )
    {
      debug( "Account is member of PENDING list, removing first." );
      pending->removeMember( toAdd );
    }
  }

  debug( "Adding account to list " + group );
  Membership *list = account->getMembershipGroup( group );
  if( !list->isMember( toAdd ) )
  {
    list->addMember( toAdd );
  }

  // TODO: I think we should check if account is in toAdd's membership lists, and if not,
  // add it to Pending?
}



void LiveService::removeMemberFromMembershipGroup( Account *account, MembershipGroup group, Account *toRemove )
{
  if( !hasAccount( account ) || !hasAccount( toRemove ) )
  {
    warning( "Warning: addMemberToMembershipGroup got an account that isn't known in the LiveService!" );
    return;
  }

  // If the account also in the pending list, remove it first
  Membership *list = account->getMembershipGroup( group );
  if( list->isMember( toRemove ) )
  {
    list->removeMember( toRemove );
  }

  // TODO: Are members actually really removed from the membership groups, as in all traces gone?
  // Or are they simply added to a Deleted list etc, because they always still appear?
}
