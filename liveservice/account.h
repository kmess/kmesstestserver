/***************************************************************************
               account.h - Representation of an user account
                             -------------------
    begin                : Wed April 1, 2009
    copyright            : (C) 2009 by Valerio Pilo, Sjors Gielen
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <QHash>
#include <QObject>
#include <QString>
#include <QVariant>

#include "../logging/loggingobject.h"

class Group;
class Membership;

/**
 * @brief Membership enum.
 *
 * Since MSNP15, there are not only groups in the contact list, but also
 * "Membership lists", which are retrieved via SOAP. There is a membership
 * list for the Allowed, Blocked and Pending contacts, as well as a list for
 * Reverse contacts (contacts which have you in their list).
 */
enum MembershipGroup
{
  MEMBERSHIP_ALLOW,
  MEMBERSHIP_BLOCK,
  MEMBERSHIP_REVERSE,
  MEMBERSHIP_PENDING
};
// note: if you append to this list, update the saving code at LiveService!



/**
 * @brief Representation of an user account.
 *
 * This class contains an user account. Every contact known in the server has
 * an Account object. The object can be obtained by calling
 * LiveService::instance()->getAccount( QString handle );
 *
 * To get debugging information from an Account, use the
 * #LoggingObject::logMessage() signal.
 *
 * @author Valerio Pilo
 * @author Sjors Gielen
 */
class Account : public LoggingObject
{
  Q_OBJECT

  friend class LiveService;

  public:

    // Constructor
                           Account( const QString &handle );
    // Destructor
    virtual               ~Account();
    // Add a group to this accounts' contact list
    const Group*           addGroup( const QString &name );
    // Retrieve a group
//     Group                  getGroup( const QString &guid ) const;
    // Retrieve the membership group
    Membership                     *getMembershipGroup( MembershipGroup ) const;
    // Retrieve all membership groups
    const QHash<MembershipGroup,Membership*> &getMembershipGroups() const;
    // Retrieve all groups
    QHash<QString,const Group*>    getGroups() const;
    // Retrieve a group by its GUID
    const Group            *getGroup( const QString &guid ) const;
    // Return all properties
    const QHash<QString,QVariant>  &getProperties() const;
    // Retrieve a property value
    QVariant                getProperty( const QString &name ) const;
    // Change a property
    void                    setProperty( const QString &name, const QVariant &value );
    // Unset a property
    void                    unsetProperty( const QString &name );

    // inline property getters
    inline QString          getPropertyString( const QString &name ) const
    { return getProperty( name ).toString(); }
    inline QByteArray       getPropertyBA( const QString &name ) const
    { return getProperty( name ).toByteArray(); }
    inline int              getPropertyInt( const QString &name ) const
    { return getProperty( name ).toInt(); }
    inline bool             getPropertyBool( const QString &name ) const
    { return getProperty( name ).toBool(); }


  private: // Private properties

    // List of groups, sorted by guid
    QHash<QString,Group*>   groups_;
    // List of memberships, sorted by membership type
    QHash<MembershipGroup,Membership*> memberships_;
    // Hash list of name => value contact properties
    QHash<QString,QVariant> properties_;

  private: // Private methods

    // Return a list of valid property names
    QStringList     knownProperties() const;
};

#endif
