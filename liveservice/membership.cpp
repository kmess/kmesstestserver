/***************************************************************************
             membership.cpp - Representation of a membership
                             -------------------
    begin                : Sun Aug 9, 2009
    copyright            : (C) 2009 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "membership.h"
#include "liveservice.h"
#include "account.h"



// Constructor
Membership::Membership( Account *owner )
: Group( owner )
{
}



// Destructor
Membership::~Membership()
{
}



/**
 * @brief Add a member to this membership group.
 *
 * Apart from adding it as a member, some additional information will be saved
 * about the membership which is necessary for a valid membership response.
 */
void Membership::addMember( Account *account )
{
  if( account == 0 || isMember( account ) )
  {
    return;
  }

  Group::addMember( account );

  int lastMid = account->getPropertyInt( "lastMid" );
  if( lastMid < 0 )
    lastMid = 0;

  // Allocate membership info
  MembershipInfo *info = new MembershipInfo();
  info->id          = ++lastMid;
  // how does a membership end up to be MSHIP_EMAIL?
  //info->type        = MSHIP_PASSPORT;
  info->lastChanged = QDateTime::currentDateTime();
  info->joinedDate  = QDateTime::currentDateTime();

  //ug() << "lastMid = " << lastMid << "id = " << info->id;

  account->setProperty( "lastMid", lastMid );

  memberships_[ account->getPropertyString( "handle" ).toLower() ] = info;
}



/**
 * Add a membership with given membership info.
 */
void Membership::addMember( Account *account, MembershipInfo *info )
{
  if( account == 0 )
  {
    return;
  }
  
  if( !isMember( account ) )
  {
    Group::addMember( account );
  }

  memberships_[ account->getPropertyString( "handle" ).toLower() ] = info;
}


/**
 * Get information on a membership.
 */
MembershipInfo *Membership::getMembershipInfo( Account *account ) const
{
  return getMembershipInfo( account->getPropertyString( "handle" ) );
}



/**
 * Get information on a membership.
 */
MembershipInfo *Membership::getMembershipInfo( const QString& handle ) const
{
  if( !isMember( handle ) )
  {
    return 0;
  }

  return memberships_[ handle.toLower() ];
}



/**
 * @brief Remove a member from this membership.
 *
 * The saved membership info will be deleted and will become inaccessible.
 */
void Membership::removeMember( Account *account )
{
  if( account == 0 || !isMember( account ) )
  {
    return;
  }

  Group::removeMember( account );

  memberships_.remove( account->getPropertyString( "handle" ).toLower() );
}
