/***************************************************************************
           liveservice.h - Contacts and Settings host
                             -------------------
    begin                : Wed April 1, 2009
    copyright            : (C) 2009 by Valerio Pilo, Sjors Gielen
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LIVESERVICE_H
#define LIVESERVICE_H

#include <QHash>
#include <QObject>
#include <QString>
#include <QVariant>

#include "account.h"


enum MembershipGroup;

/**
 * @brief Contacts and Settings hosting system.
 *
 * This is the class responsible for management of client user accounts
 * and settings.
 *
 * It contains a list of accounts (identified by their handle)
 *
 * @author Valerio Pilo
 * @author Sjors Gielen
 */
class LiveService : public LoggingObject
{
  Q_OBJECT


  public: // Public methods
    // Return an account
    Account                *getAccount( const QString &handle );
    // Return whether a saved account with a certain handle is registered
    bool                    hasAccount( const QString &handle );
    // Return whether a saved account is known in this LiveService
    bool                    hasAccount( Account *account );
    // Create a new empty account and return it
    Account                *newAccount( const QString &handle, bool temporary = false );
    // Delete an account
    void                    deleteAccount( Account *account );
    void                    deleteAccount( const QString &handle );
    // Get a bit of information on an account in html
    QString                 getHtmlAccountInfo( Account *account );
    // Load configuration from file
    bool                    loadConfig();
    // Retrieve a setting value
    QVariant                getSetting( const QString &name ) const;
    // Change a setting
    void                    setSetting( const QString &name, const QVariant &value );
    // Check the database for errors
    bool                    checkDatabase( bool repair = false );
    // Remove or add a member to a membership group
    void                    addMemberToMembershipGroup( Account *account, MembershipGroup group, Account *toAdd );
    void                    removeMemberFromMembershipGroup( Account *account, MembershipGroup group, Account *toRemove );

    inline QString          getSettingString( const QString &name ) const
    { return getSetting( name ).toString(); }
    inline int              getSettingInt( const QString &name ) const
    { return getSetting( name ).toInt(); }

  public: // Public static methods

    // Retrieve or create the current live service instance
    static LiveService     *instance();
    // Destroy the current live service instance
    static void             destroy();


  private: // Private methods

    // Constructor
                            LiveService( QObject *parent = 0 );
    // Destructor
    virtual                ~LiveService();


  private: // Private static properties

    // The current singleton instance
    static LiveService     *instance_;


  private: // Private properties

    // List of known account names
    QHash<QString,Account*> accounts_;
    // List of network and server settings
    QHash<QString,QVariant> settings_;

  signals:
    void                    userAdded( Account *account );
    // Don't save this value - it will become incorrect just after the signal
    // is handled
    void                    userDeleted( Account *account );

};



#endif
