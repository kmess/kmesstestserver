/***************************************************************************
               membership.h - Representation of a membership
                             -------------------
    begin                : Sun Aug 9, 2009
    copyright            : (C) 2009 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MEMBERSHIP_H
#define MEMBERSHIP_H

#include "group.h"

#include <QString>
#include <QHash>
#include <QDateTime>

class Account;
struct MembershipInfo;


/**
 * @brief Representation of a membership.
 *
 * This class represents a membership. Since MSNP15, memberships (Allow,
 * Reverse, Blocked and Pending contacts) are sent via SOAP. This class
 * represents those memberships.
 *
 * @author Sjors Gielen
 */
class Membership : public Group
{

  friend class LiveService;

  public:

    // Constructor
                            Membership( Account *owner );
    // Destructor
    virtual                ~Membership();
    // Getters
    virtual MembershipInfo *getMembershipInfo( Account *account ) const;
    virtual MembershipInfo *getMembershipInfo( const QString& handle ) const;

  protected:
    // see Group
    virtual void            addMember( Account *account );
    virtual void            addMember( Account *account, MembershipInfo *info );
    virtual void            removeMember( Account *account );

  private: // Private properties
    QHash<QString,MembershipInfo*> memberships_;
};



/**
 * Type of membership.
 */
enum MembershipType
{
  MSHIP_PASSPORT,
  MSHIP_EMAIL
}; // if you add or remove, update the LiveServer saving code.



/**
 * Contains the required information about a membership.
 */
struct MembershipInfo
{
  /// The ID for this membership. Unique per membership for an account.
  int id;
  //MembershipType type;
  QDateTime lastChanged;
  QDateTime joinedDate;
  //QDateTime expiration; // probably always zero, not handled anyway.
}; // if you add or remove, update the LiveServer saving code.

#endif
