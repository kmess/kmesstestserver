/***************************************************************************
             account.cpp - Representation of an user account
                             -------------------
    begin                : Wed April 1, 2009
    copyright            : (C) 2009 by Valerio Pilo, Sjors Gielen
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "group.h"
#include "membership.h"
#include "account.h"

#include <QStringList>
#include <QUuid>

/**
 * Constructor. Requires you to give a handle, so one is always set.
 */
Account::Account( const QString &handle )
{
  setProperty( "handle", handle );

  memberships_[ MEMBERSHIP_ALLOW   ] = new Membership( this );
  memberships_[ MEMBERSHIP_BLOCK   ] = new Membership( this );
  memberships_[ MEMBERSHIP_REVERSE ] = new Membership( this );
  memberships_[ MEMBERSHIP_PENDING ] = new Membership( this );
}



// Destructor
Account::~Account()
{
  qDeleteAll( groups_ );
  qDeleteAll( memberships_ );
}



/**
 * @brief Add a group to this accounts' contact list
 * 
 * A GUID will be randomly generated and the new group will be returned. You
 * can add members to it via the LiveService.
 *
 * @see LiveService::addMemberToGroup()
 */
const Group *Account::addGroup( const QString &name )
{
  Group *group = new Group( this );
  group->setName( name );

  // Generate a new GUID
  // For MSN, they look like:
  // 14080d84-cb93-4f05-bfa9-01ecabfa33d7
  // QUuid generates a UUID for us, but it's not the same as the Microsoft
  // GUID the Live servers use. Let's hope Live Messenger accepts these,
  // otherwise we'll have to write our own algorithm.
  QUuid guid = QUuid::createUuid();
  // warn: toString leaves {} around the UUID, while the Live servers
  // explicitly *don't* send them. If Live Messenger chokes on this,
  // we'll have to remove them.
  group->setGuid( guid.toString() );

  groups_[ guid.toString() ] = group;
  return group;
}



/**
 * Get all groups for this account.
 */
QHash<QString,const Group*> Account::getGroups() const
{
  QHash<QString,const Group*> constGroups_;
  QHashIterator<QString,Group*> it( groups_ );
  while( it.hasNext() )
  {
    it.next();

    debug( "Returning all groups - adding " + it.key() );
    constGroups_[ it.key() ] = it.value();
  }

  return constGroups_;
}



/**
 * Get a group by its guid. Returns the Group object as const because
 * you must change it via the LiveService, not yourself.
 */
const Group *Account::getGroup( const QString &guid ) const
{
  return groups_[ guid ];
}



/**
 * Get the membership group for the given Membership.
 */
Membership *Account::getMembershipGroup( MembershipGroup ms ) const
{
  return memberships_[ ms ];
}



/**
 * Get all membership groups for this account.
 */
const QHash<MembershipGroup,Membership*>& Account::getMembershipGroups() const
{
  return memberships_;
}



// Retrieve all properties
const QHash<QString,QVariant>& Account::getProperties() const
{
  return properties_;
}



// Retrieve a property value
QVariant Account::getProperty( const QString &name ) const
{
  if( ! knownProperties().contains( name.toLower() ) )
  {
    warning( name + " is not a known property!" );
  }

  return properties_[ name.toLower() ];
}



// Return the known properties
QStringList Account::knownProperties() const
{
  QStringList properties;
  properties << "status" << "password" << "uux" << "msnobject"
             << "friendly" << "capabilities" << "verified"
             << "handle" << "temporary" << "cid" << "lastmid"
             << "admin";
  return properties;
}



// Change a property
void Account::setProperty( const QString &name, const QVariant &value )
{
  if( ! knownProperties().contains( name.toLower() ) )
  {
    warning( name + " is not a known property!" );
  }

  properties_[ name.toLower() ] = value;
}



// Unset a property
void Account::unsetProperty( const QString &name )
{
  if( !knownProperties().contains( name.toLower() ) )
  {
    warning( name + " is not a known property!" );
  }

  properties_.take( name.toLower() );
}

/**
<Member xsi:type="PassportMember">
    <MembershipId>4</MembershipId>
    <Type>Passport</Type>
    <State>Accepted</State>
    <Deleted>false</Deleted>
    <LastChanged>2008-09-27T17:40:41.187-07:00</LastChanged>
    <JoinedDate>1899-12-31T16:00:00-08:00</JoinedDate>
    <ExpirationDate>0001-01-01T00:00:00</ExpirationDate>
    <Changes/>
    <PassportName>amroth@coldshock.net</PassportName>
    <IsPassportNameHidden>false</IsPassportNameHidden>
    <PassportId>0</PassportId>
    <CID>3881855586234429570</CID>
    <PassportChanges />
</Member>
<Member xsi:type="EmailMember">
    <MembershipId>53</MembershipId>
    <Type>Email</Type>
    <State>Accepted</State>
    <Annotations>
      <Annotation>
      <Name>MSN.IM.BuddyType</Name>
      <Value>32:</Value></Annotation>
    </Annotations>
    <Deleted>false</Deleted>
    <LastChanged>2009-02-27T11:10:08.04-08:00</LastChanged>
    <JoinedDate>1899-12-31T16:00:00-08:00</JoinedDate>
    <ExpirationDate>0001-01-01T00:00:00</ExpirationDate>
    <Changes />
    <Email>kmess2009@yahoo.com</Email>
 </Member>
*/

