/***************************************************************************
               group.h - Representation of a group
                             -------------------
    begin                : Wed July 29, 2009
    copyright            : (C) 2009 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef GROUP_H
#define GROUP_H

#include <QString>
#include <QList>
#include <QStringList>

#include "../logging/loggingobject.h"

class Account;


/**
 * @brief Representation of a group.
 *
 * This class contains an user group. For example, the default groups
 * "Friends", or the new group "Favorites", are represented by this class.
 *
 * Memberships, groups of contacts associated with a contact, are represented
 * by the Membership class which derives from this one.
 *
 * @author Valerio Pilo
 * @author Sjors Gielen
 */
class Group : public LoggingObject
{
  Q_OBJECT

  friend class LiveService;

  public:

    // Constructor
                            Group( Account *owner );
    // Destructor
    virtual                ~Group();
    // Getters and setters
    const QString&          name() const;
    const QList<Account*>&  members() const;
    const QString&          guid() const;
    bool                    isMember( Account* ) const;
    bool                    isMember( const QString& ) const;

    void                    setName( const QString& );
    void                    setGuid( const QString& );

    Account                *owner() const;

  protected:
    // Modifying members in this group can only be done by the LiveService.
    virtual void            removeMember( Account* );
    void                    removeMember( const QString& );
    void                    clearMembers();
    virtual void            addMember( Account* );
    void                    addMember( const QString& );
    void                    addMembers( QList<Account*> );
    void                    addMembers( QStringList );

  private: // Private properties
    QString                 name_;
    QString                 guid_;
    QList<Account*>         members_;
    Account                *owner_;
};



#endif
