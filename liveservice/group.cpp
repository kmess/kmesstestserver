/***************************************************************************
             group.cpp - Representation of a group
                             -------------------
    begin                : Wed July 29, 2009
    copyright            : (C) 2009 by Valerio Pilo, Sjors Gielen
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "group.h"
#include "liveservice.h"



// Constructor
Group::Group( Account *owner )
: owner_( owner )
{
  if( owner_ == 0 )
  {
    warning( "Group created with null owner - the server may crash later!" );
  }
}



// Destructor
Group::~Group()
{
}



/**
 * Retrieve the group's GUID.
 */
const QString& Group::guid() const
{
  return guid_;
}



/**
 * Retrieve the group name.
 */
const QString& Group::name() const
{
  return name_;
}



/**
 * Retrieve the members list
 */
const QList<Account*>& Group::members() const
{
  return members_;
}



/**
 * Retrieve the owner of this group.
 */
Account *Group::owner() const
{
  return owner_;
}



/**
 * Retrieve whether the given member is in this group.
 */
bool Group::isMember( Account *account ) const
{
  return members_.contains( account );
}



/**
 * Retrieve whether the given member is in this group.
 *
 * @param handle will be used to get the correct Account object.
 */
bool Group::isMember( const QString &handle ) const
{
  Account *account = LiveService::instance()->getAccount( handle );

  if( account && isMember( account ) )
  {
    return true;
  }

  return false;
}



/**
 * Set the GUID of this group.
 */
void Group::setGuid( const QString &guid )
{
  guid_ = guid;
}



/**
 * Set the name of this group.
 */
void Group::setName( const QString &name )
{
  name_ = name;
}



/**
 * Remove given member from the group.
 */
void Group::removeMember( Account* account )
{
  if( account && members_.contains( account ) )
  {
    members_.removeAll( account );
  }
}



/**
 * Remove given member form the group.
 *
 * @param handle will be used to get the Account* to remove.
 */
void Group::removeMember( const QString &handle )
{
  Account *account = LiveService::instance()->getAccount( handle );
  if( account )
  {
    removeMember( account );
  }
}



/**
 * Clear the member list for this group.
 */
void Group::clearMembers()
{
  members_.clear();
}



/**
 * Add a new member to this group.
 */
void Group::addMember( Account* account )
{
  if( account && ! members_.contains( account ) )
  {
    members_.append( account );
  }
}



/**
 * Add a new member to this group.
 *
 * @param handle will be used to get the Account* object to add.
 */
void Group::addMember( const QString &handle )
{
  Account *account = LiveService::instance()->getAccount( handle );
  if( account )
  {
    addMember( account );
  }
}



/**
 * Add a list of members to this group.
 */
void Group::addMembers( QList<Account*> accountList )
{
  for( int i = 0; i < accountList.size(); ++i )
  {
    addMember( accountList[i] );
  }
}



/**
 * Add a list of members to this group.
 *
 * @apram accountList list of handles which will be turned into a list of Accounts to check.
 */
void Group::addMembers( QStringList accountList )
{
  for( int i = 0; i < accountList.size(); ++i )
  {
    addMember( accountList[i] );
  }
}
