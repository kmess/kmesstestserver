/***************************************************************************
                msnsession.h - KMess Testing MSN Server
                             -------------------
    begin                : Weg July 8, 2009
    copyright            : (C) 2009 by Diederik van der Boor
    email                : vdboor@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MSNSESSION_H
#define MSNSESSION_H

#include <QObject>
#include "logging/loggingobject.h"

/**
 * @brief A session data object for a single user login.
 *
 * @author Diederik van der Boor
 */
class ClientSession : public LoggingObject
{
  Q_OBJECT

  public:

    // Constructor
                          ClientSession( QObject *parent );
    // Destructor
    virtual              ~ClientSession();

    // Return the handle
    const QString &       handle() const;
    // Return whether the session is authenticated
    bool                  isAuthenticated() const;
    // Mark the session as authenticated
    void                  setAuthenticated( bool authenticated );
    // Assign the user
    void                  setUser( const QString &handle, const QString &remoteIp );


  private: // Private properties

    // Whether the session is authenticated
    bool                  authenticated_;
    // The actual handle of the user
    QString               handle_;
    // IP of the client
    QString               remoteIp_;
    // The roaming location
    QString               location_;
};



#endif
