/***************************************************************************
                tokenmanager.cpp - Testserver authentication token manager
                             -------------------
    begin                : Tue Aug 4, 2009
    copyright            : (C) 2009 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "tokenmanager.h"
#include "connections/adminconnection.h"



// Constructor
TokenManager::TokenManager( QObject *parent )
: LoggingObject( parent )
{
  setObjectName("TokenManager");
}



// Destructor
TokenManager::~TokenManager()
{
  debug( "Destroyed." );
}



// Request a new token
Token* TokenManager::requestToken( const QString &type, const QString &address, const QString &handle, int lifetimeSeconds )
{
  if( type != "urn:passport:legacy" && type != "urn:passport:compact" )
  {
    warning( "Unknown token type " + type + "requested!" );
  }

  // Generate the securityToken
  QByteArray nonce;
  for( int i = 0; i < 24; i++ )
  {
    nonce += (char)( ( rand() % 74 ) + 48 );
  }

  Token *token = new Token;
  token->type = type;
  token->address = address;
  token->created = QDateTime::currentDateTime();
  token->expires = token->created.addSecs( lifetimeSeconds );
  token->securityToken = nonce.toBase64();
  token->binarySecret  = "barbarfoofoo"; // ?

  token->handle = handle;

  tokens_.append( token );
  token->id = tokens_.size() - 1;

  debug( "Added a token for address " + address + " expiring at " + token->expires.toString() );

  // TODO: don't return token, but tokens_[foo], so we can return a const ref
  emit tokenAdded( token );
  return token;
}



// Find a token with a specific ID
Token *TokenManager::findToken( int tokenId ) const
{
  return tokens_[ tokenId ];
}



Token *TokenManager::findToken( const QByteArray &token ) const
{
  for( int i = 0; i < tokens_.size(); ++i )
  {
    if( tokens_[i]->securityToken == token )
    {
      return tokens_[i];
    }
  }

  return 0;
}



// Get info on a token
QString TokenManager::getHtmlTokenInfo( Token *token )
{
  QString info( "<html><table>" );
  info.append( "<tr><th colspan='2'>Token information</td></tr>" );
  info.append( "<tr><th>Token type</th><td>" + token->type + "</td></tr>" );
  info.append( "<tr><th>Address</th><td>" + token->address + "</td></tr>" );
  info.append( "<tr><th>Created at</th><td>" + token->created.toString( Qt::TextDate ) + "</td></tr>" );
  if( token->expires > QDateTime::currentDateTime() )
  {
    info.append( "<tr><th>Will expire at</th><td>" + token->expires.toString( Qt::TextDate ) + 
        " (in " + QString::number( QDateTime::currentDateTime().secsTo( token->expires ) ) + " seconds)</td></tr>" );
  }
  else
  {
    info.append( "<tr><th>Has expired at</th><td>" + token->expires.toString( Qt::TextDate ) +
        " (" + QString::number( token->expires.secsTo( QDateTime::currentDateTime() ) ) + " seconds ago)</td></tr>" );
  }
  info.append( "<tr><th>Associated handle</th><td>" + token->handle + "</td></tr>" );
  
  return info;
}



QString TokenManager::getHtmlTokenInfo( int tokenId )
{
  Token *token = findToken( tokenId );

  if( token == 0 )
  {
    return QString( "Can't find that token!" );
  }

  return getHtmlTokenInfo( token );
}



void TokenManager::tokensRequested( AdminConnection *c ) const
{
  c->sendTokens( tokens_ );
}

void TokenManager::tokenInformationRequested( AdminConnection *c,
                                              int tid ) const
{
  c->sendTokenInfo( findToken( tid ) );
}
