/***************************************************************************
         kmesstestserver.cpp - KMess Testing MSN Server
                             -------------------
    begin                : Sat March 28, 2009
    copyright            : (C) 2009 by Valerio Pilo, Sjors Gielen
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "kmesstestserver.h"
#include "msnservergroup.h"
#include "tokenmanager.h"
#include "liveservice/liveservice.h"
#include "servers/adminserver.h"

#include <QDateTime>
#include <QMessageBox>
#include <QCoreApplication>
#include <QFile>

#include <stdlib.h>

// Constructor
KMessTestServer::KMessTestServer( QString logFile )
: LoggingObject()
, logFile_()
{
  info( "Initializing..." );
  setObjectName( "KMessTestServer" );
  setLogFile( logFile );

  // Start the authentication token master
  tokenManager_ = new TokenManager( this );
  relayChildLogMessages( tokenManager_ );
  connect( tokenManager_,   SIGNAL(   tokenAdded( Token* ) ),
           this,            SLOT(       addToken( Token* ) ) );
  connect( tokenManager_,   SIGNAL( tokenExpired( Token* ) ),
           this,            SLOT(    removeToken( Token* ) ) );

  // Connect to the live service
  LiveService *lc = LiveService::instance();
  relayChildLogMessages( lc );
  connect( lc,              SIGNAL(    userAdded( Account* ) ),
           this,            SLOT(        addUser( Account* ) ) );
  connect( lc,              SIGNAL(  userDeleted( Account* ) ),
           this,            SLOT(     removeUser( Account* ) ) );
  if( !lc->loadConfig() )
  {
    fatalError( "Failed to load configuration. Exiting." );
    exit( 1 );
  }


  // Start the server.
  msnServerGroup_ = new MsnServerGroup( tokenManager_, this );
  relayChildLogMessages( msnServerGroup_ );
  connect( this,  SIGNAL( logMessage( const LoggingBase*, const QString&,
                                      LoggingBase::MessageType)),
           this,  SLOT  (  statusLog( const LoggingBase*, const QString&,
                                      LoggingBase::MessageType)));
  connect( msnServerGroup_->adminServer(), SIGNAL(   shutDown() ),
           this,                           SLOT  (   shutDown() ) );
  connect( msnServerGroup_->adminServer(),
                        SIGNAL( tokensRequested( AdminConnection* ) ),
           tokenManager_, SLOT( tokensRequested( AdminConnection* ) ) );
  connect( msnServerGroup_->adminServer(),
            SIGNAL( tokenInformationRequested( AdminConnection*, int ) ),
           tokenManager_,
              SLOT( tokenInformationRequested( AdminConnection*, int ) ) );
  // TODO:
  //void      contactsRequested( AdminConnection* );
  //void      contactInformationRequested( AdminConnection*, const QString& );

  msnServerGroup_->initialize();

  info( "Test server ready." );
}



// Destructor
KMessTestServer::~KMessTestServer()
{
  // MsnServerGroup is a QObject child.

  LiveService::destroy();

  // If this object died, the program can exit.
  QCoreApplication::quit();
}



// Add a token to the list
void KMessTestServer::addToken( Token *newToken )
{
  QString shortDescription = newToken->address + " " + newToken->handle;

  // TODO
  // tokens_->insertItem( newToken->id, shortDescription );
}



// Add a user to the list
void KMessTestServer::addUser( Account *newAccount )
{
  QString shortDescription = newAccount->getProperty( "handle" ).toString();

  accounts_.append( newAccount );

  // TODO
  //contacts_->insertItem( accounts_.size() - 1, shortDescription );
}



// Remove a token from the list
void KMessTestServer::removeToken( Token *oldToken )
{
  // TODO
  //tokens_->takeItem( oldToken->id );
}



// Remove a user from the list
void KMessTestServer::removeUser( Account *account )
{
  int id = accounts_.indexOf( account );

  // don't take the item from the list, it will invalidate the id's in tokens_
  accounts_[id] = 0;

  // TODO
  // contacts_->takeItem( id );
}



/**
 * @brief Set a new log filename.
 *
 * The open log file will be automatically reopened to the new file.
 * @see reopenLogFile()
 */
void KMessTestServer::setLogFile( const QString &file )
{
  logFileName_ = file;
  reopenLogFile();
}



// Shutdown the server
void KMessTestServer::shutDown()
{
  info("Shutting down.");
  deleteLater();
}



// Slot for servers to add messages
void KMessTestServer::statusLog( const LoggingBase *b, const QString &message,
                                 LoggingBase::MessageType type )
{
  QObject *sender = QObject::sender();
  if( sender == NULL )
  {
    // should use debug(), error(), etc from LoggingBase
    qWarning() << "statusLog() with NULL sender, ignoring";
    return;
  }

  QString objectName = b->loggingName();
  QString typeName   = LoggingBase::messageTypeToString( type );
  QDateTime dateTime = QDateTime::currentDateTime();

  // Send it over to the admin connection
  msnServerGroup_->adminServer()->sendLogMessage( dateTime, objectName,
   typeName, message );

  // Write it to the log file
  if( logFile_ )
  {
    logFile_->write( QString(dateTime.toString() + " [" + typeName + "] " +
                     objectName + ": " + message + "\n").toUtf8() );
  }

  // And also send it to the debug output
  qDebug() << objectName.toAscii().constData() << ":" << message;
}




  // The list of connection doesn't have this one, create it
  /* if( connectionsList->item( connectionId ) == 0 )
  {
    connectionsList->insertItem( connectionId,
                                 "(" + QString::number( connectionId ) + ") " +
                                 QTime::currentTime().toString() );
  } */

  /* header = QString( "<span style=\"color:silver;font-size:small;\"> %1 - %2: </span><br/>\n" )
                  .arg( QDateTime::currentDateTime().toString() )
                  .arg( sender() ? sender()->objectName() : "Main" );

  switch( logType )
  {
    case TYPE_NORMAL:
      htmlMessage = "<hr/>\n" +
                    header +
                    "<div>\n" +
                    message +
                    "</div>\n";
      break;

    case TYPE_STATUS:
      htmlMessage = "<hr/>\n" +
                    header +
                    "<div style=\"color:gray;\">\n" +
                    message +
                    "</div>\n";
      break;

    case TYPE_ERROR:
      htmlMessage = "<hr/>\n" +
                    header +
                    "<div style=\"color:#900;\">\n" +
                    message +
                    "</div>\n";
      break;

    case TYPE_INCOMING_TRAFFIC:
      htmlMessage = "<hr/>\n" +
                    header +
                    "<div style=\"color:#090;\">\n" +
                    message +
                    "</div>\n";
      break;

    case TYPE_OUTGOING_TRAFFIC:
      htmlMessage = "<hr/>\n" +
                    header +
                    "<div style=\"color:#009;\">\n" +
                    message +
                    "</div>\n";
      break;

    case TYPE_OUTGOING_ERROR:
      htmlMessage = "<hr/>\n" +
                    header +
                    "<div style=\"color:#990;\">\n" +
                    message +
                    "</div>\n";
      break;
  }

  if( logString != 0 )
    logString->append( htmlMessage );
} */



/**
 * @brief Reopen the log file.
 * @see setLogFile()
 */
void KMessTestServer::reopenLogFile()
{
  if( logFile_ != 0 )
  {
    logFile_->write( "*** Closing log file at "
      + QDateTime::currentDateTime().toString( Qt::TextDate ).toAscii() + "\n" );
    if( logFileName_.isEmpty() )
    {
      logFile_->write( "*** Warning: New log filename is empty!\n" );
    }
    logFile_->close();
    delete logFile_;
    logFile_ = 0;
  }

  if( logFileName_.isEmpty() )
    return;

  qDebug() << "Opening log file at " << logFileName_;

  logFile_ = new QFile( logFileName_ );
  logFile_->open( QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text
                | QIODevice::Unbuffered );
  logFile_->write( "*** Log file opened at "
      + QDateTime::currentDateTime().toString( Qt::TextDate ).toAscii() + "\n" );
}



