/***************************************************************************
               soapmessage.h - SOAP message wrapper
                             -------------------
    begin                : Fri July 17, 2009
    copyright            : (C) 2009 by Diederik van der Boor, Sjors Gielen
    email                : vdboor@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SOAPMESSAGE_H
#define SOAPMESSAGE_H

#include <QDomDocument>


/**
 * @brief SOAP message wrapper
 *
 * @author Diederik van der Boor
 * @author Sjors Gielen
 */
class SoapMessage
{

  public:
    // Constructor for responses
                       SoapMessage( const QDomDocument &envelope, const QString &action = QString() );
    // Constructor for raw responses
                       SoapMessage( const QByteArray &envelope, const QString &action = QString() );
    // Destructor
    virtual           ~SoapMessage();

    // Return the SOAP envelope (containing a body and a header)
    const QDomDocument &envelope() const;

    // Get the error string
    const QString&     error() const;
    // Set the error string. If error is empty, unset error
    void               setError( const QString &error );
    // Check whether this SoapMessage has an error
    bool               isError() const;
    // Convert the message back to a string
    QString            toString() const;
    // Convert the message back to a byte array
    QByteArray         toBytes() const;

    // Get the SOAP action
    const QString&     action() const;
    // Set the SOAP action
    void               setAction( const QString& );

  public: // static
    // Returns a soapmessage with an error set
    static SoapMessage errorMessage( QString error, QByteArray envelope = "<unknown-error/>" );
    static SoapMessage errorMessage( QString error, QDomDocument envelope );

  private:
    // The body element
    QDomDocument        xmlEnvelope_;
    // The HTTP error response ("404 Not Found")
    QString            error_;
    // The SOAP action
    QString            action_;

};


#endif
