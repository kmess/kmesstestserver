/***************************************************************************
                     abservice.cpp - AddressBook service SOAP handler
                             -------------------
    begin                : Tue Aug 4, 2009
    copyright            : (C) 2009 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "abservice.h"
#include "soapmessage.h"
#include "../liveservice/account.h"
#include "../liveservice/liveservice.h"
#include "../liveservice/membership.h"
#include "../util/xml.h"
#include "../tokenmanager.h"
#include "../msnservergroup.h"

#include <QDebug>
#include <QDomDocument>

// Constructor
AbService::AbService( MsnServerGroup *msnServerGroup, QObject *parent )
: SoapHandler( msnServerGroup, parent )
{
  setObjectName( "AbService" );
}



// Process the request
SoapMessage AbService::processRequest( const SoapMessage &message )
{
  QDomNode body = Xml::getNode( message.envelope(), "/soap:Envelope/soap:Body" );
  if( body.isNull() )
  {
    warning( "Node Envelope/Body not found!" );
  }

  if( ! body.firstChildElement( "FindMembership" ).isNull() )
  {
    return processFindMembershipRequest( message );
  }
  else if( !body.firstChildElement( "ABFindAll" ).isNull() )
  {
    return processAbFindAllRequest( message );
  }
  else if( !body.firstChildElement( "ABContactUpdate" ).isNull() )
  {
    return processAbContactUpdateRequest( message );
  }
  else if( !body.firstChildElement( "ABContactAdd" ).isNull() )
  {
    return processAbContactAdd( message );
  }

  warning( "Don't understand this request, can't handle it. Dumping:\n" +
         message.envelope().toString() );
  return SoapMessage::errorMessage( "404 Not Found" );
}

SoapMessage AbService::processAbFindAllRequest( const SoapMessage &message )
{
  Token *token = getTicketToken( message );
  if( token == 0 )
  {
    // TODO: return error
    warning( "Expired or invalid token!" );
    return SoapMessage::errorMessage( "400 Bad Request" );
  }
  Account *account = LiveService::instance()->getAccount( token->handle );
  if( account == 0 )
  {
    debug( "Account associated with ticket (" + token->handle + ") is unknown!" );
    return SoapMessage::errorMessage( "400 Bad Request" );
  }

  QByteArray responseBody(
      "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\n"
      "  <soap:Header>\n"
      "    <ServiceHeader xmlns=\"http://www.msn.com/webservices/AddressBook\">\n"
      "      <Version>14.02.2812.0000</Version>\n"
      "      <CacheKey>12355423</CacheKey>\n" // see comment below
      "      <CacheKeyChanged>true</CacheKeyChanged>\n"
      // Live servers send PreferredHostName and SessionId here
      "    </ServiceHeader>\n"
      "  </soap:Header>\n"
      "  <soap:Body>\n"
      "    <ABFindAllResponse xmlns=\"http://www.msn.com/webservices/AddressBook\">\n"
      "      <ABFindAllResult>\n"
      "        <groups>\n" );

  // TODO get account groups and print them
  responseBody.append(
      "          <Group>\n"
      "            <groupId>1337</groupId>\n"
      "            <groupInfo>\n"
      "              <annotations>\n"
      "                <Annotation>\n"
      "                  <Name>MSN.IM.Display</Name>\n"
      "                  <Value>1</Value>\n"
      "                </Annotation>\n"
      "              </annotations>\n"
      "              <groupType>c8529ce2-6ead-434d-881f341e17db3ff8</groupType>\n" // ? is this an ID or an enum value?
      "              <name>Leet Group</name>\n"
      "              <IsNotMobileVisible>false</IsNotMobileVisible>\n"
      "              <IsPrivate>false</IsPrivate>\n"
      "              <IsFavorite>false</IsFavorite>\n"
      "            </groupInfo>\n"
      "            <propertiesChanged />\n"
      "            <fDeleted>false</fDeleted>\n"
      "            <lastChange>0001-01-01T00:00:00</lastChange>\n"
      "          </Group>\n" );

  responseBody.append(
      "        </groups>\n"
      "        <contacts>\n" );
      
  // Send some information about ourselves.
  // KMess does a gotPersonalInformation( friendlyName, cid, blp ).
  responseBody.append(
      "          <Contact>\n"
      "            <contactId>12345</contactId>\n" // ?
      "            <contactInfo>\n"
      "              <annotations>\n"
      "                <Annotation><Name>MSN.IM.MBEA</Name><Value>0</Value></Annotation>\n" // ?
      "                <Annotation><Name>MSN.IM.GTC</Name><Value>1</Value></Annotation>\n" // ?
      // from the looks of MsnNotificationConnection::changePersonalProperties(),
      // blp can be 0 or something else. if 0, it sends BL in the BLP command,
      // otherwise AL. It links to: 
      // http://msnpiki.msnfanatic.com/index.php/Command:BLP
      "                <Annotation><Name>MSN.IM.BLP</Name><Value>0</Value></Annotation>\n" // ?
      "                <Annotation><Name>MSN.IM.RoamLiveProperties</Name><Value>1</Value></Annotation>\n" // ?
      "                <Annotation><Name>Live.Profile.Expression.LastChanged</Name><Value>0001-01-01T00:00:00</Value></Annotation>\n"
      "              </annotations>\n"
      "              <contactType>Me</contactType>\n" // information about ourselves
      "              <quickName>ME</quickName>\n"
      "              <firstName>Test Server User</firstName>\n"
      "              <passportName>" + account->getPropertyString( "handle" ) + "</passportName>\n"
      "              <IsPassportNameHidden>false</IsPassportNameHidden>\n"
      "              <displayName>" + account->getPropertyString( "friendly" ) + "</displayName>\n"
      "              <puid>0</puid>\n"
      "              <CID>" + account->getPropertyString( "cid" ) + "</CID>\n"
      "              <IsNotMobileVisible>true</IsNotMobileVisible>\n"
      "              <isMobileIMEnabled>false</isMobileIMEnabled>\n"
      "              <isMessengerUser>false</isMessengerUser>\n"
      "              <isFavorite>false</isFavorite>\n"
      "              <isSmtp>false</isSmtp>\n"
      "              <hasSpace>false</hasSpace>\n"
      "              <spotWatchState>NoDevice</spotWatchState>\n"
      "              <PublicDisplayName>Test Server User PublicDisplayName</PublicDisplayName>\n"
      "              <IsAutoUpdateDisabled>false</IsAutoUpdateDisabled>\n"
      "              <PropertiesChanged />\n"
      "            </contactInfo>\n"
      "            <propertiesChanged />\n"
      "            <fDeleted>false</fDeleted>\n"
      "            <CreateDate>0001-01-01T00:00:00</CreateDate>\n"
      "            <lastChange>0001-01-01T00:00:00</lastChange>\n"
      "            <lastModifiedBy>7</lastModifiedBy>\n" // ?
      "          </Contact>\n" );

  responseBody.append(
      // TODO fix this section, make it look like the section above:
      "          <Contact>\n"
      "            <contactId>contactid</contactId>\n"
      "            <contactInfo>\n"
      "              <contactType>Regular</contactType>\n"
      "              <displayName>I am a hardcoded contact :D</displayName>\n"
      "              <isMessengerUser>true</isMessengerUser>\n"
      "              <hasSpace>false</hasSpace>\n"
      "              <contactEmailType>Messenger3</contactEmailType>\n"
      "              <passportName>test2@kmess-msn.org</passportName>\n"
      "              <guid>1337</guid>\n"
      "            </contactInfo>\n"
      "          </Contact>\n" );

  responseBody.append(
      "        </contacts>\n"
      "        <ab>\n"
      "          <abId>00000000-0000-0000-0000-000000000000</abId>\n"
      "          <abInfo>\n"
      "            <ownerPuid>0</ownerPuid>\n"
      "            <OwnerCID>" + account->getPropertyString( "cid" ) + "</OwnerCID>\n"
      "            <ownerEmail>" + account->getPropertyString( "handle" ) + "</ownerEmail>\n"
      "            <fDefault>true</fDefault>\n"
      "            <joinedNamespace>false</joinedNamespace>\n"
      "            <IsBot>false</IsBot>\n"
      // and then some more, TODO
      "          </abInfo>\n"
      "          <lastChange>0001-01-01T00:00:00</lastChange>\n"
      "          <DynamicItemLastChanged>0001-01-01T00:00:00</DynamicItemLastChanged>\n"
      "          <RecentActivityItemLastChanged>0001-01-01T00:00:00</RecentActivityItemLastChanged>\n" // probably, this is what WLM puts in the personal message when you played a Live game or so
      "          <createDate>0001-01-01T00:00:00</createDate>\n"
      "          <propertiesChanged/>\n"
      "        </ab>\n"
      "      </ABFindAllResult>\n"
      "    </ABFindAllResponse>\n"
      "  </soap:Body>\n"
      "</soap:Envelope>\n" );

  SoapMessage response = SoapMessage( responseBody );
  
  debug( "Sending response on address book request." );

  return response;
}

SoapMessage AbService::processFindMembershipRequest( const SoapMessage &message )
{
  Token *token = getTicketToken( message );
  if( token == 0 )
  {
    // TODO: return error
    debug( "Expired or invalid token!" );
    return SoapMessage::errorMessage( "400 Bad Request" );
  }
  Account *account = LiveService::instance()->getAccount( token->handle );
  if( account == 0 )
  {
    debug( "Account associated with ticket (" + token->handle + ") is unknown!" );
    return SoapMessage::errorMessage( "400 Bad Request" );
  }

  QByteArray responseBody(
      "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" "
        "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
        "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\n"
      "  <soap:Header>\n"
      "    <ServiceHeader xmlns=\"http://www.msn.com/webservices/AddressBook\">\n"
      "      <Version>14.02.2812.0000</Version>\n" // ?
      "      <CacheKey>12345</CacheKey>\n" // TODO. I think the client first
      // sends a cache key, and then if any data changed, the server sends
      // the updated information with a new cachekey. We should generate and
      // save a cache key along with our data, and only send this all if it's
      // changed.
      "      <CacheKeyChanged>true</CacheKeyChanged>\n"
      // TODO: Live sends a PreferredHostName and a SessionId here.
      "    </ServiceHeader>\n"
      "  </soap:Header>\n"
      "  <soap:Body>\n"
      "    <FindMembershipResponse xmlns=\"http://www.msn.com/webservices/AddressBook\">\n"
      "      <FindMembershipResult>\n"
      "        <Services>\n"
      "          <Service>\n"
      "            <Memberships>\n" );

  QHashIterator<MembershipGroup,Membership*> it( account->getMembershipGroups() );

  while( it.hasNext() )
  {
    it.next();

    QString group;
    if( it.key() == MEMBERSHIP_ALLOW )
    {
      group = "Allow";
    }
    else if( it.key() == MEMBERSHIP_BLOCK )
    {
      group = "Block";
    }
    else if( it.key() == MEMBERSHIP_REVERSE )
    {
      group = "Reverse";
    }
    else if( it.key() == MEMBERSHIP_PENDING )
    {
      group = "Pending";
    }
    else
    {
      debug( "Unexpected membership type " + QString::number(it.key())
           + ", skipping sending membership list." );
      continue;
    }

    // Looks like the Live servers only send a membership list if it contains
    // members - at least for the Pending list I'm sure. And, so do we.
    if( it.value()->members().size() == 0 )
    {
      debug( "Membership list " + group + " is empty, skipping send." );
      continue;
    }

    responseBody.append(
      "              <Membership>\n"
      "                <MemberRole>" + group + "</MemberRole>\n"
      "                <Members>\n" );

    for( int mi = 0; mi < it.value()->members().size(); ++mi )
    {
      Account *member = it.value()->members().at( mi );
      MembershipInfo *info = it.value()->getMembershipInfo( member->getPropertyString( "handle" ) );

      responseBody.append(
      // hardcode type
      "                  <Member xsi:type=\"PassportMember\">\n"
      "                    <MembershipId>" + QString::number( info->id ) + "</MembershipId>\n"
      // hardcode type
      "                    <Type>Passport</Type>\n" // or Email
      "                    <State>Accepted</State>\n"
      "                    <Deleted>false</Deleted>\n" // is this ever true?
      "                    <LastChanged>"
        + info->lastChanged.toString( Qt::ISODate ) + "</LastChanged>\n"
      "                    <JoinedDate>"
        + info->joinedDate.toString( Qt::ISODate ) + "</JoinedDate>\n"
      "                    <ExpirationDate>0001-01-01T00:00:00</ExpirationDate>\n" // never
      "                    <Changes/>\n" // does this ever have a value here?
      "                    <PassportName>" + member->getPropertyString( "handle" ) + "</PassportName>\n"
      "                    <IsPassportNameHidden>false</IsPassportNameHidden>\n"
      "                    <PassportId>0</PassportId>\n" // seems to be always 0
      "                    <CID>"
        + member->getPropertyString( "cid" ) + "</CID>\n"
      "                    <PassportChanges/>\n" // does this ever have a value here?
      "                  </Member>\n" );
    }

    responseBody.append(
      "                </Members>\n"
      "                <MembershipIsComplete>true</MembershipIsComplete>\n" // is this ever false here?
      "              </Membership>\n" );

  }

  responseBody.append(
      "           </Memberships>\n"
      "           <Info>\n"
      "             <Handle>\n"
      "               <Id>2</Id>\n" // why 2?
      "               <Type>Messenger</Type>\n"
      "               <ForeignId />\n"
      "             </Handle>\n"
      "             <InverseRequired>true</InverseRequired>\n" // ?
      "             <AuthorizationCriteria>Everyone</AuthorizationCriteria>\n" // ?
      "             <IsBot>false</IsBot>\n"
      "           </Info>\n"
      "           <Changes/>\n" // ?
      "           <LastChange>0001-01-01T00:00:00</LastChange>\n" // TODO
      "           <CreatedDate>0001-01-01T00:00:00</CreatedDate>\n" // TODO
      "           <Deleted>false</Deleted>\n"
      "         </Service>\n"
      "       </Services>\n"
      "       <OwnerNamespace>\n"
      "         <Info>\n"
      // In my dumps this was sent literally like this:
      "           <Handle>\n"
      "             <Id>00000000-0000-0000-0000-000000000000</Id>\n"
      "             <IsPassportNameHidden>false</IsPassportNameHidden>\n"
      "             <CID>0</CID>\n"
      "           </Handle>\n"
      "           <CreatorPuid>0</CreatorPuid>\n"
      "           <CreatorCID>" + account->getPropertyString( "cid" ) + "</CreatorCID>\n" // CID of logged in contact
      "           <CreatorPassportName>" + account->getPropertyString( "handle" ) + "</CreatorPassportName>\n"
      "           <CircleAttributes>\n"
      "             <IsPresenceEnabled>false</IsPresenceEnabled>\n" // ?
      "             <Domain>WindowsLive</Domain>\n"
      "           </CircleAttributes>\n"
      "           <MessengerApplicationServiceCreated>false</MessengerApplicationServiceCreated>\n" // ?
      "         </Info>\n"
      "         <Changes/>\n" // ?
      "         <CreateDate>0001-01-01T00:00:00</CreateDate>\n" // TODO
      "         <LastChange>0001-01-01T00:00:00</LastChange>\n" // TODO
      "       </OwnerNamespace>\n"
      "     </FindMembershipResult>\n"
      "    </FindMembershipResponse>\n"
      "  </soap:Body>\n"
      "</soap:Envelope>\n" );

  SoapMessage response = SoapMessage( responseBody );
  
  debug( "Sending response on address book request." );

  return response;
}

SoapMessage AbService::processAbContactUpdateRequest( const SoapMessage &message )
{
  Token *token = getTicketToken( message );
  if( token == 0 )
  {
    // TODO: return error
    warning( "Expired or invalid token!" );
    return SoapMessage::errorMessage( "400 Bad Request" );
  }
  Account *account = LiveService::instance()->getAccount( token->handle );
  if( account == 0 )
  {
    debug( "Account associated with ticket (" + token->handle + ") is unknown!" );
    return SoapMessage::errorMessage( "400 Bad Request" );
  }

  const QDomDocument &envelope = message.envelope();
  QDomNode contacts            = Xml::getNode( envelope, "/soap:Envelope/soap:Body/ABContactUpdate/contacts" );

  int numContacts = contacts.childNodes().size();
  if( numContacts != 1 )
  {
    warning( "AbContactUpdate contains not one contact, but " +
             QString::number(numContacts) );
    return SoapMessage::errorMessage( "400 Bad Request" );
  }

  QDomNode contactNode = contacts.firstChild();
  QString contactType = Xml::getNodeValue( contactNode, "contactInfo/contactType" );
  if( contactType != "Me" )
  {
    warning( "AbContactUpdate contains a contact that's not Me, but " + contactType );
    return SoapMessage::errorMessage( "400 Bad Request" );
  }

  QString propertiesChanged = Xml::getNodeValue( contactNode, "propertiesChanged" );
  if( propertiesChanged != "DisplayName" )
  {
    warning( "AbContactUpdate wanted to change a property that's not DisplayName, but " + propertiesChanged );
    return SoapMessage::errorMessage( "400 Bad Request" );
  }

  QString displayName = Xml::getNodeValue( contactNode, "contactInfo/displayName" );

  account->setProperty( "friendly", displayName );

  QByteArray response(
      "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
      "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\n"
      "  <soap:Header>\n"
      "    <ServiceHeader xmlns=\"http://www.msn.com/webservices/AddressBook\">\n"
      "      <Version>14.02.2812.0000</Version>\n"
      "      <CacheKey>123135</CacheKey>\n" // TODO
      "      <CacheKeyChanged>true</CacheKeyChanged>\n"
      //"      <PreferredHostName>proxy-bay.contacts.msn.com</PreferredHostName>\n"
      //"      <SessionId>83830862-120b-4c04-9b58-cb7cc433d8a3</SessionId>\n"
      "    </ServiceHeader>\n"
      "  </soap:Header>\n"
      "  <soap:Body>\n"
      "    <ABContactUpdateResponse xmlns=\"http://www.msn.com/webservices/AddressBook\" />\n"
      "  </soap:Body>\n"
      "</soap:Envelope>\n" );

  return SoapMessage( response );
}

SoapMessage AbService::processAbContactAdd( const SoapMessage &message )
{
  Token *token = getTicketToken( message );
  if( token == 0 )
  {
    // TODO: return error
    warning( "Expired or invalid token!" );
    return SoapMessage::errorMessage( "400 Bad Request" );
  }
  Account *account = LiveService::instance()->getAccount( token->handle );
  if( account == 0 )
  {
    debug( "Account associated with ticket (" + token->handle + ") is unknown!" );
    return SoapMessage::errorMessage( "400 Bad Request" );
  }

  const QDomDocument &envelope = message.envelope();

  QString abId = Xml::getNodeValue( envelope, "/soap:Envelope/soap:Body/ABContactAdd/abId" );
  if( abId != "00000000-0000-0000-0000-000000000000" )
  {
    debug( "Address book ID in ABContactAdd is not zero, but: " + abId );
  }

  QDomNode contacts            = Xml::getNode( envelope, "/soap:Envelope/soap:Body/ABContactAdd/contacts" );

  int numContacts = contacts.childNodes().size();
  if( numContacts != 1 )
  {
    warning( "AbContactUpdate contains not one contact, but " + numContacts );
    return SoapMessage::errorMessage( "400 Bad Request" );
  }

  QDomNode contactNode = contacts.firstChild();
  QString contactType = Xml::getNodeValue( contactNode, "contactInfo/contactType" );
  QString passportName = Xml::getNodeValue( contactNode, "contactInfo/passportName" );
  QString allowListManagement = Xml::getNodeValue( envelope,
    "/soap:Envelope/soap:Body/ABContactAdd/options/EnableAllowListManagement" );

  if( contactType == "LivePending" )
  {
    debug( "Type = LivePending" );
    // Don't know what this means?
  }
  else if( contactType == "" )
  {
    warning( "No type" );
  }
  else
  {
    warning( "Type=" + contactType );
  }

  Account *toAdd = LiveService::instance()->getAccount( passportName );
  if( toAdd == 0 )
  {
    debug( "Unknown account to add. Returning internal server error." );
    // TODO right error
    return SoapMessage::errorMessage( "500 Internal Server Error" );
  }

  // I think the goal of this SOAP call is to remove someone from the Pending
  // list and add it into the Allowed list.
  // So that's exactly what we will do.

  // addMemberToMembershipGroup() will automatically remove the user from the
  // Pending list, so we don't have to do that.

  debug( "Adding" + passportName + " to the Allowed list" );
  LiveService::instance()->addMemberToMembershipGroup( account, MEMBERSHIP_ALLOW, toAdd );

  // No idea...
  QString responseBody(
      "<soap:Envelope><soap:Body><ABContactUpdateResponse/></soap:Body></soap:Envelope>" );

  return SoapMessage( responseBody.toLatin1() );
}
