/***************************************************************************
                       rst.h - Password RST SOAP handler
                             -------------------
    begin                : Fri July 17, 2009
    copyright            : (C) 2009 by Diederik van der Boor, Sjors Gielen
    email                : vdboor@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RST_H
#define RST_H

#include "soaphandler.h"


class MsnServerGroup;



/**
 * @brief Password RST SOAP handler.
 *
 * @author Diederik van der Boor
 * @author Sjors Gielen
 */
class Rst : public SoapHandler
{
  Q_OBJECT

  public:

    // Constructor
                       Rst( MsnServerGroup *msnServerGroup, QObject *parent = 0 );
    // Destructor
    //virtual           ~Rst();

    // Process a request
    virtual SoapMessage processRequest( const SoapMessage &message );

  private:
};



#endif
