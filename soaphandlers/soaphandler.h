/***************************************************************************
        soaphandler.h - SOAP request handler instance
                             -------------------
    begin                : Fri July 17, 2009
    copyright            : (C) 2009 by Diederik van der Boor, Sjors Gielen
    email                : vdboor@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SOAPHANDLER_H
#define SOAPHANDLER_H

#include <QObject>
#include "../logging/loggingobject.h"

class SoapMessage;
class SoapConnection;
class QDomElement;
class MsnServerGroup;
struct Token;

/**
 * @brief Base class for SOAP handlers.
 *
 * SOAP requests are always posted to a URL / path. For example, a request
 * could be sent to /RST.srf. For every incoming connection, a SoapConnection
 * is made and its constructor gets a list of serverPages, the available paths
 * available for that connection. For every path, it gets a SoapHandler. For
 * example, /RST.srf has its own specific SoapHandler.
 *
 * @author Diederik van der Boor
 * @author Sjors Gielen
 */
class SoapHandler : public LoggingObject
{
  Q_OBJECT

  public:

    // Constructor
                       SoapHandler( MsnServerGroup *msnServerGroup, QObject *parent = 0 );
    // Destructor
    virtual           ~SoapHandler();

    // Process a request
    virtual SoapMessage processRequest( const SoapMessage &message ) = 0;
    // Get the default SOAP response header
    virtual QByteArray getSoapHeader( const QString &handle ) const;
    Token             *getTicketToken( const SoapMessage &message ) const;

    // Get the msnservergroup
    MsnServerGroup    *msnServerGroup() const;

  private:

    // The msnservergruop
    MsnServerGroup    *msnServerGroup_;
};



#endif
