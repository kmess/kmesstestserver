/***************************************************************************
                     rst.cpp - Password RST SOAP handler
                             -------------------
    begin                : Fri July 17, 2009
    copyright            : (C) 2009 by Diederik van der Boor, Sjors Gielen
    email                : vdboor@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rst.h"
#include "soapmessage.h"
#include "../util/xml.h"
#include "../tokenmanager.h"
#include "../msnservergroup.h"

#include <QDebug>
#include <QDomDocument>

// Constructor
Rst::Rst( MsnServerGroup *msnServerGroup, QObject *parent )
: SoapHandler( msnServerGroup, parent )
{
  setObjectName( "RST" );
}



// Process the request
SoapMessage Rst::processRequest( const SoapMessage &message )
{
  // Note that login.live.com/RST.srf redirects to msnia.login.live.com/RST.srf.
  // So if Live Messenger chokes on this response, it *might* be because it expects
  // a redirection. We've seen more braindead things in WLM.

  if( message.toString().isEmpty() )
  {
    qWarning() << "Got an empty RST request message. Returning error.";
    return SoapMessage::errorMessage( "500 Internal Server Error", "" );
    // or rather, Invalid Request.
  }

  QDomElement xml = message.envelope().documentElement();

  // Get the username and password from the header
  const QDomElement &requestHeader( xml.firstChildElement( "soap:Header" ) );
  const QString &handle  ( Xml::getNodeValue( requestHeader, "wsse:Security/wsse:UsernameToken/wsse:Username" ) );
  const QString &password( Xml::getNodeValue( requestHeader, "wsse:Security/wsse:UsernameToken/wsse:Password" ) );

  debug( "Login request has handle " + handle );

  // Get the list of requested security tokens from the body
  QList<Token*> responseTokens;
  const QDomNodeList &tokens( xml.elementsByTagName( "wsa:Address" ) );
  for( int index = 0; index < tokens.count(); index++ )
  {
    const QString& address( tokens.item( index ).toElement().text() );

    debug( "Login request has address" + address );

    // Actually, these addresses use urn:passport:legacy instead of urn:passport:compact:
    // - http://Passport.NET/tb
    // -
    // But we use urn:passport:compact for all.
    Token *token = msnServerGroup()->tokenManager()->requestToken( "urn:passport:compact", address, handle, 3600 * 24 );
     
    /* if( address == "http://Passport.NET/tb" )
    {
      token = msnServerGroup()->tokenManager()->requestToken( "urn:passport:legacy", address, 3600 * 24 );
    }
    else
    {
      token = msnServerGroup()->tokenManager()->requestToken( "urn:passport:compact", address, 3600 * 24 );
    } */

    responseTokens.append( token );

    debug( "Got a token!" );
  }

  QByteArray responseBody( getSoapHeader( handle ) );

  responseBody.append( "  <S:Body>\n"
                       "    <wst:RequestSecurityTokenResponseCollection>\n" );

  int nextSecurityTokenId = 0;

  for( int i = 0; i < responseTokens.size(); ++i )
  {
    Token *token = responseTokens[ i ];
    debug( "Adding a response for token for " + token->address );

    /* bool isLegacy = false;
    if( token.type == "urn:passport:legacy" )
    {
      isLegacy = true;
    } */

    responseBody.append(
        "      <wst:RequestSecurityTokenResponse>\n"
        "        <wst:TokenType>" + token->type.toAscii() + "</wst:TokenType>\n"
        "        <wsp:AppliesTo>\n"
        "          <wsa:EndpointReference>\n"
        "            <wsa:Address>" + token->address.toAscii() + "</wsa:Address>\n"
        "          </wsa:EndpointReference>\n"
        "        </wsp:AppliesTo>\n"
        "        <wst:LifeTime>\n"
        "          <wsu:Created>" + token->created.toString( Qt::ISODate ).toAscii() + "</wsu:Created>\n"
        "          <wsu:Expires>" + token->expires.toString( Qt::ISODate ).toAscii() + "</wsu:Expires>\n"
        "        </wst:LifeTime>\n"
        "        <wst:RequestedSecurityToken>\n" );

    // The BinarySecurityToken can be written as EncryptedData too. This happens for the
    // http://Passport.NET/tb login.

    /* Example of encrypteddata:
                <wst:RequestedSecurityToken>
                    <EncryptedData Id="BinaryDAToken0" Type="http://www.w3.org/2001/04/xmlenc#Element">
                        <EncryptionMethod Algorithm="http://www.w3.org/2001/04/xmlenc#tripledes-cbc">
                        </EncryptionMethod>
                        <ds:KeyInfo>
                            <ds:KeyName>http://Passport.NET/STS</ds:KeyName>
                        </ds:KeyInfo>
                        <CipherData>
                            <CipherValue>AbPvEJMru5nxs5pkYqh/b1Gt+oyVPmoUxVjbH1jVtBSq0hdp4VhP+zaxD/eQ9TTQQgVITb4EuZsNEgym775IutNFZmjFgiTieGHR8Rz9oCPNiD0UQCdyNX0SgOwP3YwW/QZ+nBsJzH8zJA8PjSGj4QvjFE0vsCQLay+pL0Oqvj991onlpwDXhHTA/KLY34ZOqn+6KqH9k/4QIo7J6YcFX/DIQ9zUdmmzR5XbFxH5w9i3idyABss3RicGNedba4eab0LSr9igBxZWbdH0dY/CxKR55+ujaiPVk5iDcunmsI08pgBupitxL+4Kwo4cSoPFNSbYaOcFsNKN+0lWMg5kuOLLMzBmmpTN//8i6IHNTBaoOgmvo53xPM6Zru2Rr5GihKh8Sl8bS+ItGsIid0OTblgfp1NkfT7aO/F3o7ZclxkAjkCamqvtpYKtcCiKLsAq4MC4oaZ81sZZaOj/vr80JvHYXrT42AWR/U/9N2lOvsQ81r9lXvfaR7OiJVb3ZQwhc6qUZ2U=</CipherValue>
                        </CipherData>
                    </EncryptedData>
                </wst:RequestedSecurityToken>
 */
    /* Example of another urn:passport:legacy binarysecuritytoken:
                    <wsse:BinarySecurityToken Id="PPToken2">t=9y1jJ0QA7AhcSWSB38OIxz9eDdjf9R3*YxRpxbL!WuevQ6xDWNew10x4Wl17SXC!BnFmU!Ah!AnMqgN2QK0i6wp154PzDzNTUBfuKcalnybEWPLji064tMOenliKs5goOJqGtLDf5DnmI$&amp;p=9v*ge3KkTB8CPLuH5aTspc8fwOHF9d8mw71KRMVLj59LzB4GxUetcdi0BmWxSiTgNaMds6tn4Xa9R02XTw7KD*INrkqr8g67zpbq9iQ98CtCF2rrnq*NWItJAFuQb3k5syLnbAZJE8E!38udAtLiecvmflD1*5HbNPkNAqvPsqEG8t56NVGHvagFZp3FN*F6iFyDTy6liRD7PV*WCMc8CKYQ$$</wsse:BinarySecurityToken>
                    */
    /* Example of an urn:passport binary security token:
                    <wsse:BinarySecurityToken Id="Compact1">t=EwCAAswbAQAUs1/VcBU2sH7mwYy3BysWZ71CRDGAAKbBJ5UHKaeQmK2/zRoWvbLNnsJRLc00im2x31iJzfe6lq3fUIJNuyJ6UNpkT0p/4E01LXp/t3A3UvQzNg/e1hhct9/k1v7lH+hy6BTH+P6D1YSBc+kPHdet1Q5t29Q4jHcR/6MiXvoO/Wv+KLFohCEvW5oUVvGG7Y7getwxOTKuA2YAAAg2E9jksYOz9dABvZVOCsi3aZeanruHdCUecPIiz8V+y2hf1ee81KJjXzWI91yw4+AqCQamJBY64llXRxYlCLcSbpbqSvgpXGFaw3HEMCV3HizDCypxvnehXxQsiJ7NTwvp+D7X5WYdZmgsIJ+hmPuZmcbqtQN1/rgTlCzGxQGcFLNvNMT2ynKkpNsfvHsu/A7/ZhquiNDNUhVEi3qyL4cjRpYx6Huz6ManQ1ZrznVd/VlRZ8eF/EZ/FkZfJ14ddjZXQXmlhIWI2SGECnhlDLhqetiTiLDG0OyJab+JbpxA4X8FthzaElfAQ/VfVYJfq0mIPCr/eLf8xF6O6VkF7+BuwYX+q155WDcY7SmRXE9sCi/zaIaNsvDptDhpBFkhamtPehp4yDfhDCaN30f6Qmj1STU93aGbo/thIhkBcnpycQHVv2I0Ghl0ct7FaFmMxikEDYmiDu88N7DN6nXthVCV1HTvftqX5wUJV/4S4KYcYbQzXJvJ8a9ow8be1C7yXxT4Jww5ry36fadA3qXNbOomxKWbvjIpGKgR4n0YKGB6jh3fKPR8Fp718F83YMQ+cE/E2faCsxvAOPT4B0k1YB575qKrD2URM6dR5zm+//K08D0h1Mb79xPj1cPXAQ==&amp;p=</wsse:BinarySecurityToken>
                    */

    QByteArray securityTokenId = "Compact" + nextSecurityTokenId++;
    QByteArray keyIdentifier = token->type.toAscii();

    responseBody.append( // TODO: securityToken to base64 below
        "          <wsse:BinarySecurityToken Id=\"" + securityTokenId + "\">" + token->securityToken + "</wsse:BinarySecurityToken>\n" );

    responseBody.append(
        "        </wst:RequestedSecurityToken>\n"
        "        <wst:RequestedTokenReference>\n"
        "          <wsse:KeyIdentifier ValueType=\"urn:passport\"></wsse:KeyIdentifier>\n" // could be urn:passport:legacy too.
        "          <wsse:Reference URI=\"#" + securityTokenId + "\"></wsse:Reference>\n"
        "        </wst:RequestedTokenReference>\n"
        "        <wst:RequestedProofToken>\n"
        "          <wst:BinarySecret>" + token->binarySecret + "</wst:BinarySecret>\n"
        "        </wst:RequestedProofToken>\n"
        "      </wst:RequestSecurityTokenResponse>\n" );
  }

  //return Response( "200 OK", responseBody );

  responseBody.append( "    </wst:RequestSecurityTokenResponseCollection>\n"
                       "  </S:Body>\n"
                       "</S:Envelope>" );

  SoapMessage response = SoapMessage( responseBody );
  
  debug( "Sending response on login request." );

  return response;
}


