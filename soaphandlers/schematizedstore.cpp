/***************************************************************************
                  schematizedstore.cpp - Live Storage SOAP handler
                             -------------------
    begin                : Thu Aug 5, 2010
    copyright            : (C) 2010 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "schematizedstore.h"
#include "soapmessage.h"
#include "../util/xml.h"
#include "../util/shared.h"
#include "../msnservergroup.h"

#include <QDebug>
#include <QDomDocument>

// Constructor
SchematizedStore::SchematizedStore( MsnServerGroup *msnServerGroup,
                                    QObject *parent )
: SoapHandler( msnServerGroup, parent )
{
  setObjectName( "SchematizedStore" );
}



QByteArray SchematizedStore::getSoapHeader( const QString &ticketToken ) const
{
  return QByteArray(
  "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
    " xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\""
    " xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\n"
  " <soap:Header>\n"
  "  <StorageUserHeader xmlns=\"http://www.msn.com/webservices/storage/w10\">\n"
  "   <Puid>0</Puid>\n"
  "   <Cid>0</Cid>\n"
  "   <ApplicationId>0</ApplicationId>\n"
  "   <TicketToken>" + Shared::htmlEscape( ticketToken ).toAscii() + "</TicketToken>\n"
  "   <IsAdmin>false</IsAdmin>\n"
  "  </StorageUserHeader>\n"
  " </soap:Header>\n"
  );
}



// Process the request
SoapMessage SchematizedStore::processRequest( const SoapMessage &message )
{
  if( message.toString().isEmpty() )
  {
    qWarning() << "Got an empty request message. Returning error.";
    return SoapMessage::errorMessage( "500 Internal Server Error", "" );
    // or rather, Invalid Request.
  }

  QDomNode body = Xml::getNode( message.envelope(), "/soap:Envelope/soap:Body" );
  if( body.isNull() )
  {
    warning( "Node Envelope/Body not found!" );
    return SoapMessage::errorMessage( "400 Bad Request", "");
  }

  QDomElement xml = message.envelope().documentElement();

  // Get the ticket token from the header (it needs to be sent back too)
  QDomElement requestHeader( xml.firstChildElement( "soap:Header" ) );
  QString token( Xml::getNodeValue( requestHeader, "TicketToken" ) );

  QByteArray responseBody( getSoapHeader( token ) );

  if( ! body.firstChildElement( "UpdateProfile" ).isNull() )
  {
    responseBody.append(
    " <soap:Body>\n"
    "  <UpdateProfileResponse xmlns=\"http://www.msn.com/webservices/storage/w10\">\n"
    "   <NoClueWhatThisShouldSay/>\n" // KMess ignores it anyway
    "  </UpdateProfileResponse>\n"
    " </soap:Body>\n"
    "</soap:Envelope>\n" );

    SoapMessage response = SoapMessage( responseBody );

    debug( "Sending response to Live storage update request." );

    return response;
  }

  QString storedDisplayName = "Stored display name";
  QString storedDisplayPicturePath = "http://byfiles.storage.msn.com/y1m4VvSq"
                      "MbQlbppwHklwMP7GEsH8Lr8VLKfjOEc8epwOu94R9AodJZ8-UUfU-y"
                      "KoK10cVikq-qfdRoqyheef68cBw";
  int     storedDisplayPictureSize = 1661;
  QString storedDisplayPictureMime = "image/jpeg";

  responseBody.append(
  " <soap:Body>\n"
  "  <GetProfileResponse xmlns=\"http://www.msn.com/webservices/storage/w10\">\n"
  "   <GetProfileResult>\n"
  "    <ResourceID>0000000000000000!101</ResourceID>\n"
  "    <DateModified>1970-01-01T00:00:00-00:00</DateModified>\n"
  "    <ExpressionProfile>\n"
  "     <ResourceID>0000000000000000!102</ResourceID>\n"
  "     <Version>1</Version>\n"
  "     <DateModified>1970-01-01T00:00:00-00:00</DateModified>\n"
  "     <Photo>\n"
  "      <ResourceID>0000000000000000!253</ResourceID>\n"
  "      <DateModified>1970-01-01T00:00:00-00:00</DateModified>\n"
  "      <DocumentStreams>\n"
  "       <DocumentStream>\n"
  "        <DocumentStreamName>UserTileStatic</DocumentStreamName>\n"
  "        <MimeType>" + Shared::htmlEscape( storedDisplayPictureMime ) + "</MimeType>\n"
  "        <DataSize>" + QString::number( storedDisplayPictureSize ) + "</DataSize>\n"
  "        <PreAuthURL>" + Shared::htmlEscape( storedDisplayPicturePath ) + "</PreAuthURL>\n"
  "        <WriteMode>Overwrite</WriteMode>\n"
  "        <Genie>false</Genie>\n"
  "        <StreamVersion>0</StreamVersion>\n"
  "        <DocumentStreamType>UserTileStatic</DocumentStreamType>\n"
  "       </DocumentStream>\n"
  "      </DocumentStreams>\n"
  "     </Photo>\n"
  "     <DisplayName>" + storedDisplayName + "</DisplayName>\n"
  "     <DisplayNameLastModified>1970-01-01T00:00:00-00:00</DisplayNameLastModified>\n"
  "     <StaticUserTilePublicURL>" + Shared::htmlEscape( storedDisplayPicturePath ) + "</StaticUserTilePublicURL>\n"
  "    </ExpressionProfile>\n"
  "   </GetProfileResult>\n"
  "  </GetProfileResponse>\n"
  " </soap:Body>\n"
  "</soap:Envelope>\n" );

  SoapMessage response = SoapMessage( responseBody );

  debug( "Sending response to Live storage request." );

  return response;
}


