/***************************************************************************
                      abservice.h - AddressBook service SOAP handler
                             -------------------
    begin                : Tue Aug 4, 2009
    copyright            : (C) 2009 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ABSERVICE_H
#define ABSERVICE_H

#include "soaphandler.h"


class MsnServerGroup;



/**
 * @brief Addressbook service SOAP handler.
 *
 * @author Sjors Gielen
 */
class AbService : public SoapHandler
{
  Q_OBJECT

  public:

    // Constructor
                       AbService( MsnServerGroup *msnServerGroup, QObject *parent = 0 );
    // Destructor
    //virtual           ~AbService();

    // Process a request
    virtual SoapMessage processRequest( const SoapMessage &message );
    SoapMessage       processFindMembershipRequest( const SoapMessage &message );
    SoapMessage       processAbFindAllRequest( const SoapMessage &message );
    SoapMessage       processAbContactUpdateRequest( const SoapMessage &message );
    SoapMessage       processAbContactAdd( const SoapMessage &message );

  private:
};



#endif
