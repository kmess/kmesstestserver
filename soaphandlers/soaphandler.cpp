/***************************************************************************
                   soaphandler.cpp - SOAP handler instance
                             -------------------
    begin                : Fri July 17, 2009
    copyright            : (C) 2009 by Diederik van der Boor, Sjors Gielen
    email                : vdboor@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "soapmessage.h"
#include "soaphandler.h"
#include "../tokenmanager.h"
#include "../msnservergroup.h"
#include "../util/xml.h"

#include <QDomElement>
#include <QDomDocument>
#include <QDateTime>

// Constructor
SoapHandler::SoapHandler( MsnServerGroup *msnServerGroup, QObject *parent )
: LoggingObject( parent )
, msnServerGroup_( msnServerGroup )
{
  setObjectName( "SoapHandler" );
}



// Destructor
SoapHandler::~SoapHandler()
{
  debug( "Deleted." );
}


// Get the default SOAP response header
QByteArray SoapHandler::getSoapHeader( const QString& handle ) const
{
  QByteArray responseBody( "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
      "<S:Envelope>\n"
      "  <S:Header>\n"
      "    <psf:pp>\n"
      "      <psf:serverVersion>1</psf:serverVersion>\n"
      "      <psf:PUID>00037FFE86C4A450</psf:PUID>\n" // ?
      "      <psf:configVersion>6.0.11409.0</psf:configVersion>\n"
      "      <psf:uiVersion>3.100.2179.0</psf:uiVersion>\n"
      "      <psf:authstate>0x48803</psf:authstate>\n" // ?
      "      <psf:reqstatus>0x0</psf:reqstatus>\n" // ?
      "      <psf:serverInfo Path=\"Live1\" RollingUpgradeStatus=\"ExclusiveNew\" LocVersion=\"0\" ServerTime=\"" + QDateTime::currentDateTime().toString( Qt::ISODate ).toAscii() + "\">BY1IDSPLGN1I04 2009.06.23.15.50.59</psf:serverInfo>\n" // !
      "      <psf:cookies/>\n"
      "      <psf:browserCookies/>\n"
      "      <psf:credProperties>\n"
      "        <psf:credProperty Name=\"MainBrandID\">MSFT</psf:credProperty>\n"
      "        <psf:credProperty Name=\"BrandIDList\"></psf:credProperty>\n"
      "        <psf:credProperty Name=\"IsWinLiveUser\">true</psf:credProperty>\n"
      "        <psf:credProperty Name=\"CID\">35df1f16b6de7082</psf:credProperty>\n"
      "        <psf:credProperty Name=\"AuthMembername\">" + handle.toAscii() + "</psf:credProperty>\n"
      "      </psf:credProperties>\n"
      "      <psf:extProperties/>\n"
      "      <psf:response/>\n"
      "    </psf:pp>\n"
      "  </S:Header>\n" );

  return responseBody;
}



Token *SoapHandler::getTicketToken( const SoapMessage &message ) const
{
  QByteArray token = Xml::getNodeValue( message.envelope(),
      "soap:Envelope/soap:Header/ABAuthHeader/TicketToken" ).toAscii();

  info( "Found ticket token: " + token );

  Token *ticket = msnServerGroup()->tokenManager()->findToken( token );

  return ticket;
}

MsnServerGroup *SoapHandler::msnServerGroup() const
{
  return msnServerGroup_;
}
