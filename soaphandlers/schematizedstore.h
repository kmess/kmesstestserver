/***************************************************************************
                   schematizedstore.h - Live Storage SOAP handler
                             -------------------
    begin                : Thu Aug 5, 2010
    copyright            : (C) 2010 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SCHEMATIZEDSTORE_H
#define SCHEMATIZEDSTORE_H

#include "soaphandler.h"


class MsnServerGroup;



/**
 * @brief Live Storage SOAP handler
 *
 * @author Sjors Gielen
 */
class SchematizedStore : public SoapHandler
{
  Q_OBJECT

  public:

    // Constructor
                        SchematizedStore( MsnServerGroup *msnServerGroup, QObject *parent = 0 );
    // Destructor
    //virtual          ~SchematizedStore();

    // Process a request
    virtual SoapMessage processRequest( const SoapMessage &message );

  private:
    virtual QByteArray  getSoapHeader( const QString &ticketToken ) const;
};



#endif
