/***************************************************************************
             soapmessage.cpp - SOAP message wrapper
                             -------------------
    begin                : Fri July 17, 2009
    copyright            : (C) 2009 by Diederik van der Boor, Sjors Gielen
    email                : vdboor@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "soapmessage.h"

#include <QDebug>

// Constructor for responses
SoapMessage::SoapMessage( const QDomDocument &envelope, const QString &action )
: xmlEnvelope_( envelope )
, action_( action )
{
}


// Constructor for raw responses
SoapMessage::SoapMessage( const QByteArray &body, const QString &action )
: action_( action )
{
  QString errorMsg; int errorLine = 0; int errorColumn = 0;
  xmlEnvelope_.setContent( body, false, &errorMsg, &errorLine, &errorColumn);
  if( !errorMsg.isEmpty() )
  {
    qDebug() << "Error in SoapMessage XML: " << errorMsg << ", line" << errorLine << "column" << errorColumn;
    qDebug() << body;
  }
}

// Destructor
SoapMessage::~SoapMessage()
{
}

// Return the SOAP envelope (containing a body and a header)
const QDomDocument &SoapMessage::envelope() const
{
  return xmlEnvelope_;
}

// Get the SOAP action
const QString& SoapMessage::action() const
{
  return action_;
}

// Get the error string
const QString& SoapMessage::error() const
{
  return error_;
}

// Returns a soapmessage with an error set
SoapMessage SoapMessage::errorMessage( QString error, QByteArray envelope )
{
  Q_ASSERT( !envelope.isEmpty() );
  SoapMessage message( envelope );
  message.setError( error );
  return message;
}

SoapMessage SoapMessage::errorMessage( QString error, QDomDocument envelope )
{
  Q_ASSERT( !envelope.isNull() );
  SoapMessage message( envelope );
  message.setError( error );
  return message;
}

// Set the action string
void SoapMessage::setAction( const QString &action )
{
  action_ = action;
}

// Set the error string. If error is empty, unset error
void SoapMessage::setError( const QString &error )
{
  error_ = error;
}

// Check whether this SoapMessage has an error
bool SoapMessage::isError() const
{
  return error_.isEmpty();
}

// Convert the message back to a string
QString SoapMessage::toString() const
{
  return xmlEnvelope_.toString();
}

// Convert the message back to a byte array
QByteArray SoapMessage::toBytes() const
{
  return xmlEnvelope_.toByteArray();
}
