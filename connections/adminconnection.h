/***************************************************************************
           adminconnection.h - Testserver administration connection
                             -------------------
    begin                : Tue Aug 19, 2009
    copyright            : (C) 2009 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ADMINCONNECTION_H
#define ADMINCONNECTION_H

#include "msnpconnection.h"

class MsnServerGroup;
class Account;
struct Token;



/**
 * @brief KMessTestServer administration and control server connection
 *
 * This class represents a connection from the administration and
 * control client. 
 *
 * @author Sjors Gielen
 */
class AdminConnection : public MsnpConnection
{
  Q_OBJECT


  public:

    // Constructor for the server
                    AdminConnection( int connectionId, MsnServerGroup *msnServerGroup );
    // Destructor
    virtual        ~AdminConnection();
    // Get the current logged in handle
    const QString  &handle() const;

  public slots:
    void            incomingCommand( const QStringList &command,
                                     const QByteArray &payload );
    void            sendLogMessage( const QDateTime &time,
                                    const QString &sender,
                                    const QString &logType,
                                    const QString &message );
    void            sendTokens( QList<Token*> tokens );
    void            sendTokenInfo( Token *token );

  private: // Private properties

    // The global server object this server is part of.
    MsnServerGroup    *msnServerGroup_;
    // Current transaction (command) ID
    int           trId_;
    // Current handle
    QString       handle_;
    // Whether this is the server or the client
    bool          isServer_;

  protected slots:

    // The socket was closed (set user status to FLN etc)
    virtual void  connectionClosed();

  signals:
    void          shutDown();
    void          tokensRequested( AdminConnection* );
    void          contactsRequested( AdminConnection* );
    void          tokenInformationRequested( AdminConnection*, int );
    void          contactInformationRequested( AdminConnection*, const QString& );
};


#endif
