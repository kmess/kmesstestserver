/***************************************************************************
        abstractconnection.h - TCP connection instance
                             -------------------
    begin                : Thu July 9, 2009
    copyright            : (C) 2009 by Diederik van der Boor
    email                : vdboor@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ABSTRACTCONNECTION_H
#define ABSTRACTCONNECTION_H

#include "../logging/loggingobject.h"

#include <QStringList>
#include <QObject>



/**
 * @brief Base class for TCP connections.
 *
 * @author Diederik van der Boor
 */
class AbstractConnection : public LoggingObject
{
  Q_OBJECT

  public:

    // Constructor
                       AbstractConnection( int id, QObject *parent = 0 );
    // Destructor
    virtual           ~AbstractConnection();
    // Retrieve the connection client name
    virtual QString    client() const = 0;
    // Close the connection to the client
    virtual void       closeConnection() = 0;
    // Get the connection ID
    int                connectionId() const;
    // A connection was initiated from a client.
    virtual void       initialize( int socketDescriptor ) = 0;


  protected slots:

    // The socket was closed
    virtual void       connectionClosed();


  private: // Private properties

    // The connection ID
    int                connectionId_;

};



#endif
