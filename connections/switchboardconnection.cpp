/***************************************************************************
           switchboardconnection.cpp - Faux Switchboard Server connection
                             -------------------
    begin                : Tue Aug 11, 2009
    copyright            : (C) 2009 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "switchboardconnection.h"
#include "../liveservice/liveservice.h"
#include "../msnservergroup.h"   // so the cast to QObject* can be done.

#include <QDomDocument>
#include <QUrl>



// Constructor
SwitchboardConnection::SwitchboardConnection( int connectionId, MsnServerGroup *parent )
: MsnpConnection( connectionId, parent )
, trId_( -1 )
{
  setObjectName( "SwitchboardConnection#" + QString::number( connectionId ) );
}



// Destructor
SwitchboardConnection::~SwitchboardConnection()
{
}



/**
 * The connection was closed. Send a message to all others that this user left.
 */
void SwitchboardConnection::connectionClosed()
{
  MsnpConnection::connectionClosed();

  emit userLeft();
}



/**
 * Return the handle.
 */
const QString &SwitchboardConnection::handle()
{
  return handle_;
}



// A command was received from a connection
void SwitchboardConnection::incomingCommand( const QStringList &command, const QByteArray &payload )
{
  if( payload.isEmpty() )
  {
    debug_protocol( "Received " + command[0] + " command:\n" + command.join( " " ), true );
  }
  else
  {
    debug_protocol( "Received " + command[0] + " payload command, with " +
                        QString::number( payload.size() ) +
                        "bytes payload:<br/>" + command.join(" "), true );
  }

  bool hasTrId = false;
  if( command.size() > 1 )
  {
    command[1].toInt( &hasTrId );
    if( hasTrId == true )
      trId_ = command[1].toInt();
  }

  QStringList replyCommand;

  // Set the command for the reply to the name and transaction id, so we can fill it with some other stuff
  for( int i = 0; i < 2; i++ )
  {
    if( command.size() <= i )
    {
      break;
    }
    replyCommand.append( command[ i ] );
  }

  // Check if we got USR yet
  QStringList allowedAnonCommands;
  allowedAnonCommands << "USR" << "ANS" << "OUT";
  if( handle_.isEmpty() && !allowedAnonCommands.contains( command[0] ) )
  {
    // error out: we need to log in first.
    warning( "Before USR was succesfully completed, got command " + command[0] + ". This is invalid, erroring out..." );
    // TODO: send error number
    closeConnection();
    return;
  }

  // Reply to the message, if it contains a known command
  if( command[0] == "USR" )
  {
    if( !handle_.isEmpty() )
    {
      warning( "Got USR but already logged in!" );
      closeConnection();
      return;
    }

    int trId              = command[1].toInt();
    const QString &handle = command[2];
    const QString &token  = command[3];

    emit requestAuthenticationCheck( trId, handle, token, -1 );
  }
  else if( command[0] == "ANS" )
  {
    if( !handle_.isEmpty() )
    {
      warning( "Got ANS but already logged in!" );
      closeConnection();
      return;
    }

    int trId              = command[1].toInt();
    const QString &handle = command[2];
    const QString &token  = command[3];
    int chatId            = command[4].toInt();

    emit requestAuthenticationCheck( trId, handle, token, chatId );
  }
  else if( command[0] == "OUT" )
  {
    closeConnection();
  }
  else if( command[0] == "CAL" )
  {
    // Inviting a contact into the switchboard connection
    const QString &handle = command[2];

    emit inviteContact( handle );
    // What would that number be for? KMess ignores it...
    sendCommand( replyCommand << "RINGING" << "540734642" );
  }
  else if( command[0] == "MSG" )
  {
    QString replyType = command[2];
    const QByteArray &message   = payload;

    if( replyType != "U" && replyType != "N" && replyType != "A" )
    {
      warning( "Invalid replyType " + replyType + ", assuming U." );
      replyType = "U";
    }

    emit messageToSend( replyType, message );
  }
  else
  {
    warning( "Received unsupported command:" + command[0]
          + "closing connection." );
    closeConnection();
  }
}



/**
 * Send the authentication result to the user.
 */
void SwitchboardConnection::postAuthenticationResult( int trId, bool ok, Account *me, int chatId, const QStringList& contactsInChat )
{
  QStringList command;
  if( contactsInChat.size() == 0 )
  {
    command << "USR";
  }
  else
  {
    command << "ANS";
  }

  if( ok )
  {
    const QString &handle   = me->getPropertyString( "handle" );
    const QString &friendly = me->getPropertyString( "friendly" );

    handle_ = handle;
    sendCommand( command << QString::number( trId ) << "OK" << handle
        << QUrl::toPercentEncoding( friendly ) );
  }
  else
  {
    // TODO error properly
    sendCommand( command << QString::number( trId ) << "NopeNotToday" );
    return;
  }

  // For ANS, also send an initial list of contacts now.
  if( command[0] == "ANS" )
  {
    for( int i = 0; i < contactsInChat.size(); ++i )
    {
      const QString &handle   = contactsInChat[ i ];
      Account *account        = LiveService::instance()->getAccount( handle );
      const QString &friendly = account->getPropertyString( "friendly" );
            QString  caps     = account->getPropertyString( "capabilities" );

      if( caps.isEmpty() )
      {
        caps = "0";
      }

      command.clear();
      // reverse engineered from KMess' parsing code, so I don't know
      // argument 1, 2 & 3. I do know: 4=handle, 5=friendly name, 6=capabilities
      sendCommand( command << "IRO" << "" << "" << ""
          << handle << friendly << caps );
    }
  }
}



/**
 * Send a message to this member of the conversation
 */
void SwitchboardConnection::sendMessage( const QString &fromHandle, const QString &fromDisplayName, const QByteArray &message )
{
  QStringList command( "MSG" );

  sendCommand( command << fromHandle << QUrl::toPercentEncoding( fromDisplayName ) << QString::number( message.size() ), message );
}



/**
 * Someone joined in this conversation.
 */
void SwitchboardConnection::someoneJoined( Account *someone )
{
  QStringList command( "JOI" );
  const QString &handle   = someone->getPropertyString( "handle" );
  const QString &friendly = someone->getPropertyString( "friendly" );
  const QString &caps     = someone->getPropertyString( "capabilities" );

  sendCommand( command << handle << QUrl::toPercentEncoding( friendly )
     << caps );
}
