/***************************************************************************
        dispatchconnection.h - Faux Dispatch Server connection
                             -------------------
    begin                : Fri July 10, 2009
    copyright            : (C) 2009 by Diederik van der Boor
    email                : vdboor@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef DISPATHCONNECTION_H
#define DISPATHCONNECTION_H

#include "msnpconnection.h"

class MsnServerGroup;


/**
 * @brief Faux Dispatch Server connection
 *
 * This class manages a MSN Dispatch server connection
 , the central interface to the
 * service for any MSN client.
 *
 * @author Diederik van der Boor
 */
class DispatchConnection : public MsnpConnection
{
  Q_OBJECT


  public:

    // Constructor
                  DispatchConnection( int connectionId, MsnServerGroup *msnServerGroup );
    // Destructor
    virtual      ~DispatchConnection();


  protected:

    // A command was received from a connection
    virtual void  incomingCommand( const QStringList &command, const QByteArray &payload );


  private: // Private properties

    // The global server object this server is part of.
    MsnServerGroup    *msnServerGroup_;
    // Current transaction (command) ID
    int           trId_;


};


#endif
