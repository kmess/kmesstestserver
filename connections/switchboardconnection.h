/***************************************************************************
           switchboardconnection.h - Faux Switchboard Server connection
                             -------------------
    begin                : Tue Aug 11, 2009
    copyright            : (C) 2009 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SWITCHBOARDCONNECTION_H
#define SWITCHBOARDCONNECTION_H

#include "msnpconnection.h"

class MsnServerGroup;
class Account;



/**
 * @brief Faux Switchboard Server connection
 *
 * This class represents a connection to a faux Switchboard server.
 *
 * @author Sjors Gielen
 */
class SwitchboardConnection : public MsnpConnection
{
  Q_OBJECT


  public:

    // Constructor
                  SwitchboardConnection( int connectionId, MsnServerGroup *msnServerGroup );
    // Destructor
    virtual      ~SwitchboardConnection();
    // Get the handle at the other end of this connection
    const QString &handle();

  public slots:
    // Get the authentication result
    void          postAuthenticationResult( int trId, bool ok, Account *me, int chatId, const QStringList &contactsInChat );
    // Someone joined the conversation
    void          someoneJoined( Account *someone );
    // Send a message to the users in this notification connection
    void          sendMessage( const QString &fromHandle, const QString &fromDisplayName, const QByteArray &message );

  protected:

    // A command was received from a connection
    virtual void  incomingCommand( const QStringList &command, const QByteArray &payload );


  private: // Private properties

    // The global server object this server is part of.
    MsnServerGroup    *msnServerGroup_;
    // Current transaction (command) ID
    int           trId_;
    // Current handle
    QString       handle_;

  protected slots:

    // The socket was closed (set user status to FLN etc)
    virtual void  connectionClosed();

  signals:

    // Request authentication check
    void          requestAuthenticationCheck( int trId, const QString &handle, const QString &token, int chatId );
    // Invite a contact
    void          inviteContact( const QString &handle );
    // Send a message
    void          messageToSend( const QString &replyType, const QByteArray &message );
    // The user left the convo
    void          userLeft();

};


#endif
