/***************************************************************************
           adminconnection.h - Testserver administration connection
                             -------------------
    begin                : Tue Aug 19, 2009
    copyright            : (C) 2009 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <QDomDocument>
#include <QUrl>
#include <QDateTime>
#include <QDebug>

#include "adminconnection.h"
#include "../liveservice/liveservice.h"
#include "../tokenmanager.h"
#include "../msnservergroup.h"



// Constructor
AdminConnection::AdminConnection( int connectionId, MsnServerGroup *parent )
: MsnpConnection( connectionId, parent )
, trId_( -1 )
{
  setObjectName( "AdminConnection#" + QString::number( connectionId ) );
  setLoggingSilent( true );
}



// Destructor
AdminConnection::~AdminConnection()
{
}



/**
 * The connection was closed. Signal that an administrator left.
 */
void AdminConnection::connectionClosed()
{
  MsnpConnection::connectionClosed();
}



/**
 * Return the handle.
 */
const QString &AdminConnection::handle() const
{
  return handle_;
}



// A command was received from a connection
void AdminConnection::incomingCommand( const QStringList &command, const QByteArray &payload )
{
  if( payload.isEmpty() )
  {
    debug_protocol( "Received " + command[0] + " command:\n" + command.join( " " ), true );
  }
  else
  {
    debug_protocol( "Received " + command[0] + " payload command, with " +
                        QString::number( payload.size() ) +
                        "bytes payload:<br/>" + command.join(" "), true );
  }

  bool hasTrId = false;
  if( command.size() > 1 )
  {
    command[1].toInt( &hasTrId );
    if( hasTrId == true )
      trId_ = command[1].toInt();
  }

  QStringList replyCommand;

  // Set the command for the reply to the name and transaction id, so we can fill it with some other stuff
  for( int i = 0; i < 2; i++ )
  {
    if( command.size() <= i )
    {
      break;
    }
    replyCommand.append( command[ i ] );
  }

  // Check if we got USR yet
  QStringList allowedAnonCommands;
  allowedAnonCommands << "VER" << "USR" << "OUT";
  if( handle_.isEmpty() && !allowedAnonCommands.contains( command[0] ) )
  {
    // error out: we need to log in first.
    qDebug() << "Before USR was succesfully completed, got command "
             << command[0] << ". This is invalid, erroring out...";
    // TODO: send error number
    closeConnection();
    return;
  }

  // Reply to the message, if it contains a known command
  if( command[0] == "VER" )
  {
    if( command.size() < 3 || command[2] != "KMADM2" )
    {
      sendCommand( replyCommand << "NAK" );
      // TODO error?
      closeConnection();
      return;
    }
    
    sendCommand( replyCommand << "KMADM2" );
  }
  else if( command[0] == "USR" )
  {
    if( !handle_.isEmpty() )
    {
      qDebug() << "Got USR but already logged in!";
      closeConnection();
      return;
    }

    if( command.size() != 3 )
    {
      sendCommand( replyCommand << "INV" );
      closeConnection();
      return;
    }

    int trId              = command[1].toInt();
    const QString &handle = command[2];
#warning TODO: check password in adminconnection
    const Account *admin  = LiveService::instance()->getAccount( handle );

    if( admin == 0 || !admin->getPropertyBool( "admin" ) )
    {
      sendCommand( replyCommand << "NAK" );
      closeConnection();
      return;
    }

    handle_ = handle;
    sendCommand( replyCommand << "OK" );
  }
  else if( command[0] == "OUT" )
  {
    closeConnection();
  }
  else if( command[0] == "SHD" )
  {
    // shutdown command
    qDebug() << "Got shutdown command from " << handle_;
    emit shutDown();
  }
  else if( command[0] == "TKN" )
  {
    if( command.size() != 4 )
    {
      sendCommand( replyCommand << "INV" );
      closeConnection();
      return;
    }

    if( command[2] == "*" )
    {
      emit tokensRequested( this );
    }
    else
    {
      // information on a specific TKN was requested
      emit tokenInformationRequested( this, command[1].toInt() );
    }
  }
  else if( command[0] == "CNT" )
  {
    if( command.size() != 3 )
    {
      sendCommand( replyCommand << "INV" );
      closeConnection();
      return;
    }

    if( command[2] == "*" )
    {
      emit contactsRequested( this );
    }
    else
    {
      // information on a specific CNT was requested
      emit contactInformationRequested( this, command[1] );
    }
  }
  else if( command[0] == "LMG" ) {
    if( command.size() != 3 )
    {
      sendCommand( replyCommand << "INV" );
      closeConnection();
      return;
    }
    // TODO: Send all log messages if command[2] == "*"
    // TODO: Otherwise, send a subset
    // For now, pretend there are none
  }
  else if(command[0] == "CNL" ) {
    if( command.size() != 3 )
    {
      sendCommand( replyCommand << "INV" );
      closeConnection();
      return;
    }
    // TODO: Send connection logs if command[2] == "*"
    // TODO: Otherwise, send a subset
    // For now, pretend there are none
  }
  else if(command[0] == "CCN" ) {
    if( command.size() != 3 )
    {
      sendCommand( replyCommand << "INV" );
      closeConnection();
      return;
    }
    // TODO: Send all current connections if command[2] == "*"
    // TODO: Otherwise, send a subset
    // For now, pretend there are none
  }
  else
  {
    qDebug() << "Received unsupported command:" << command[0]
             << "closing connection.";
    closeConnection();
  }
}

/// Send a log message
void AdminConnection::sendLogMessage( const QDateTime &time,
                                      const QString &sender,
                                      const QString &logType,
                                      const QString &message )
{
  // Don't send when we're not logged in yet
  if( !handle_.isEmpty() )
  {
    QByteArray rawMessage( message.toUtf8() );
    sendCommand( QStringList() << "LOG" << sender << time.toString(Qt::ISODate)
                               << logType
                               << QString::number( rawMessage.size() ),
                                  rawMessage );
  }
}

void AdminConnection::sendTokenInfo( Token *token )
{
  QByteArray tokenInfo;

  QByteArray securityToken = token->securityToken;
  securityToken.replace( '\n', "" );
  securityToken.replace( '\r', "" );

  QByteArray binarySecret = token->binarySecret;
  binarySecret.replace( '\n', "" );
  binarySecret.replace( '\r', "" );

  tokenInfo.append( token->handle.toUtf8() + "\n" );
  tokenInfo.append( token->type.toAscii() + "\n" );
  tokenInfo.append( token->address.toAscii() + "\n" );
  tokenInfo.append( token->created.toString() + "\n" );
  tokenInfo.append( token->expires.toString() + "\n" );
  tokenInfo.append( securityToken + "\n" );
  tokenInfo.append( binarySecret + "\n" );

  sendCommand( QStringList() << "TKN" << "INFO" << QString::number(token->id)
                             << QString::number( tokenInfo.size() ),
                                tokenInfo );
}

void AdminConnection::sendTokens( QList<Token*> tokens )
{
  QByteArray tokenList;

  foreach(Token *t, tokens)
  {
    tokenList.append( QByteArray::number( t->id ) + " "
                    + t->address.toAscii() + " "
                    + t->handle.toUtf8() + "\n" );
  }

  sendCommand( QStringList() << "TKN" << "LIST"
                             << QString::number( tokenList.size() ),
                                tokenList );
}
