/***************************************************************************
        abstractconnection.h - TCP connection instance
                             -------------------
    begin                : Thu July 9, 2009
    copyright            : (C) 2009 by Diederik van der Boor
    email                : vdboor@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "abstractconnection.h"

#include <QHostAddress>


// Constructor
AbstractConnection::AbstractConnection( int id, QObject *parent )
: LoggingObject( parent )
, connectionId_( id )
{
  setObjectName( "AbstractConnection#" + QString::number( id ) );
}



// Destructor
AbstractConnection::~AbstractConnection()
{
  debug( "Deleted." );
}



// The socket was closed
void AbstractConnection::connectionClosed()
{
  debug( "Connection closed." );
}



// Get the connection ID
int AbstractConnection::connectionId() const
{
  return connectionId_;
}

