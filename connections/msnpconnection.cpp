/***************************************************************************
          msnpconnection.cpp - TCP connection Instance
                             -------------------
    begin                : Sat March 28, 2009
    copyright            : (C) 2009 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "msnpconnection.h"

#include <QHostAddress>
#include <QTcpSocket>


// Static class members declaration
QStringList MsnpConnection::payloadCommands_;



// Constructor
MsnpConnection::MsnpConnection( int id, QObject *parent )
: AbstractConnection( id, parent )
, nextPayloadSize_( 0 )
, tcpSocket_( 0 )
{
  setObjectName( "MsnpConnection#" + QString::number( id ) );

  if( payloadCommands_.isEmpty() )
  {
    payloadCommands_ << "MSG" << "ADL" << "UUX" << "UUN";
  }
}



// Destructor
MsnpConnection::~MsnpConnection()
{
  delete tcpSocket_;
}



// Retrieve the SSL connection client name
QString MsnpConnection::client() const
{
  if( tcpSocket_ == 0 || tcpSocket_->peerAddress().isNull() )
  {
    return QString( "*not connected*" );
  }

  return ( tcpSocket_->peerAddress().toString() + ":" + tcpSocket_->peerPort() );
}



// Close the connection to the client
void MsnpConnection::closeConnection()
{
  tcpSocket_->close();
}



// The socket was closed
void MsnpConnection::connectionClosed()
{
  AbstractConnection::connectionClosed();

  // Although the socket may be closing, we must not delete it until
  // the delayed close is done.
  // When the socket is deleted, this connection will be also deleted.
  if( tcpSocket_->state() == QAbstractSocket::ClosingState )
  {
    connect( tcpSocket_, SIGNAL( disconnected() ),
             this,       SLOT  (  deleteLater() ) );
    tcpSocket_ = 0;
  }
  else
  {
    deleteLater();
  }
}



// A socket error has been received
void MsnpConnection::socketError( QAbstractSocket::SocketError socketError )
{
  // Make sure Qt doesn't try to send data over this just-closed error
  // on mac, not doing this causes a crash when an adminconnection closes,
  // because logMessage below results in the testserver writing data to this
  // adminconnection.
  if( socketError == QAbstractSocket::RemoteHostClosedError )
    closeConnection();

  error( "Error id " + QString::number( socketError ) +
                   ": \"" + tcpSocket_->errorString() +
                   "\" received!" );
}



// A connection was initiated. Start the SSL encryption mode
void MsnpConnection::initialize( int socketDescriptor )
{
  // Start the SSL handshake
  tcpSocket_ = new QTcpSocket( this );
  if( ! tcpSocket_->setSocketDescriptor( socketDescriptor ) )
  {
    error( "TCP Connection failed" );
    tcpSocket_->close();
    return;
  }

  // Connect the socket's signals
  connect( tcpSocket_, SIGNAL(     disconnected()                             ),
           this,       SLOT  ( connectionClosed()                             ) );
  connect( tcpSocket_, SIGNAL(        readyRead()                             ),
           this,       SLOT  (      readTcpData()                             ) );
  connect( tcpSocket_, SIGNAL(            error(QAbstractSocket::SocketError) ),
           this,       SLOT  (      socketError(QAbstractSocket::SocketError) ) );

  debug( "Opened TCP connection..." );
}



// Incoming data from the socket
void MsnpConnection::readTcpData()
{
  bool        gotPayloadCommand  = false;
  bool        hasCompletePayload = false;
  QByteArray  rawCommandLine;
  QByteArray  payloadData;
  QStringList command;
  QString     commandLine;
  bool        isNumeric = false;

  while( true )
  {
    // Read the next command if we're not waiting for a payload.
    if( nextPayloadSize_ == 0 )
    {
      // Stop when socket didn't receive a full line yet (Qt does the buffering, thanks Qt4!)
      if( ! tcpSocket_->canReadLine() )
      {
        break;
      }

      // Read the next line.
      rawCommandLine = tcpSocket_->readLine();

      // Parse the command, split in separate fields
      commandLine = QString::fromUtf8( rawCommandLine.data(), rawCommandLine.size() );
      command     = commandLine.trimmed().split( ' ' );

      // See if it's a payload command or an error
      if( ( command[0].toInt( &isNumeric ) != 0 ) && isNumeric == true )
      {
        // If the command contains a payload length parameter, also parse it
        gotPayloadCommand = ( command.size() > 2 );
      }
      else
      {
        gotPayloadCommand = payloadCommands_.contains( command[0] );
      }

      if( gotPayloadCommand )
      {
        nextPayloadSize_    = command[ command.count() - 1 ].toInt();  // Last arg is payload length.
        nextPayloadCommand_ = command;
      }
    }


    // See if we can read the payload
    if( nextPayloadSize_ > 0 )
    {
      // Check if the full payload is received.
      // Otherwise, just wait until "dataReceived" is called again.
      hasCompletePayload = ( tcpSocket_->bytesAvailable() >= nextPayloadSize_ );
      if( ! hasCompletePayload )
      {
        break;
      }

      // Full payload received.
      payloadData = tcpSocket_->read( nextPayloadSize_ );

      // Copy state from previous loop if needed.
      if( ! gotPayloadCommand )
      {
        gotPayloadCommand = true;
        command           = nextPayloadCommand_;
      }

      // Reset state for next loop.
      nextPayloadSize_ = 0;
    }
    else
    {
      payloadData.clear();
    }

    // Received the full request!
    // Deliver to the derived class
    incomingCommand( command, payloadData );
  }
}



// Send data to the connected client
void MsnpConnection::sendCommand( const QStringList &command, const QByteArray &payload )
{
  if( tcpSocket_ == 0 || ! tcpSocket_->isOpen() )
  {
    debug("Unable to write on a non ready socket!");
    return;
  }

  debug_protocol( "Sending " + command[0] + " command with " +
    QString::number( payload.size() ) + " bytes payload:<br/>" + command.join( " " ), false );
  tcpSocket_->write( QString( command.join( " " ) + "\r\n" + payload ).toUtf8() );
}


