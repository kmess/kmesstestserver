/***************************************************************************
    notificationconnection.h - Faux Notification Server connection
                             -------------------
    begin                : Fri July 10, 2009
    copyright            : (C) 2009 by Diederik van der Boor
    email                : vdboor@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef NOTIFICATIONCONNECTION_H
#define NOTIFICATIONCONNECTION_H

#include "msnpconnection.h"

class MsnServerGroup;
class Account;



/**
 * @brief Faux Notification Server connection
 *
 * This class manages a MSN Notification server connection
 , the central interface to the
 * service for any MSN client.
 *
 * @author Diederik van der Boor
 */
class NotificationConnection : public MsnpConnection
{
  Q_OBJECT


  public:

    // Constructor
                  NotificationConnection( int connectionId, MsnServerGroup *msnServerGroup );
    // Destructor
    virtual      ~NotificationConnection();
    // Get the handle
    const QString& handle();
    // Is this user logged in?
    bool          isLoggedIn();

  public slots:
    // The logged in user connected from another location
    void          loggedInFromOtherLocation();
    // A contact in the logged on users' list has changed status
    void          contactChangedStatus( Account *member, QString name = QString() );
    // ...or has changed friendly name
    void          contactChangedFriendlyName( Account *member, const QString &name );
    // ...or uux information
    void          contactChangedUux( Account *member );
    // Give back the requested switchboard server
    void          createdSwitchboardServer( int trId, const QString &address, const QString &token );
    // Contact was invited into a chat
    void          invitedToSwitchboard( const QString &address, const QString &token, const QString &handle, const QString &friendly );

  protected:

    // A command was received from a connection
    virtual void  incomingCommand( const QStringList &command, const QByteArray &payload );


  private: // Private properties

    // The global server object this server is part of.
    MsnServerGroup    *msnServerGroup_;
    // Current transaction (command) ID
    int           trId_;
    // MSN Protocol version (unset if VER was not completed succesfully yet)
    QString       protocolVersion_;
    // Logged in user handle (unset if USR was not completed succesfully yet)
    QString       handle_;
    // Whether the initial status was already sent
    bool          initialSent_;
    // List of contacts in various groups (sent to us by the client, the ADL command)
    QHash<QString,QPair<int,int> > contactList_;

  protected slots:

    // The socket was closed (set user status to FLN etc)
    virtual void  connectionClosed();

  signals:

    // The user logged in (handle can be obtained using ->handle()).
    void          loggedIn();
    // The user changed his friendly name
    void          changedFriendlyName( const QString &name );
    // The user changed his status / capabilities / msnobject
    void          changedStatus( const QString &status, const QString &capabilities, const QString &msnobject );
    // The user changed his UUX information
    void          changedUux( const QString &newUux );
    // The user wants to start a chat and needs a switchboard
    void          switchboardRequested( int ackid );

};


#endif
