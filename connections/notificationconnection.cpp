/***************************************************************************
  notificationconnection.cpp - Faux Notification Server connection
                             -------------------
    begin                : Fri July 10, 2009
    copyright            : (C) 2009 by Diederik van der Boor
    email                : vdboor@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "notificationconnection.h"
#include "../liveservice/liveservice.h"
#include "../msnservergroup.h"   // so the cast to QObject* can be done.

#include <QDomDocument>
#include <QUrl>



// Constructor
NotificationConnection::NotificationConnection( int connectionId, MsnServerGroup *parent )
: MsnpConnection( connectionId, parent )
, trId_( -1 )
, initialSent_( false )
{
  setObjectName( "NotificationConnection#" + QString::number( connectionId ) );
}



// Destructor
NotificationConnection::~NotificationConnection()
{
}



/**
 * The connection was closed. Update account status if an account was logged
 * in.
 */
void NotificationConnection::connectionClosed()
{
  MsnpConnection::connectionClosed();

  // only send FLN if the client sent an initial status (ie is not FLN now)
  if( initialSent_ )
  {
    emit changedStatus( "FLN", "0", "" );
  }
}



/**
 * A member in the logged on accounts' list has changed his status.
 *
 * @param member The contact that changed its status
 * @param name Optionally, the new friendly name
 */
void NotificationConnection::contactChangedStatus( Account *member, QString name )
{
  QStringList command;
  const QString& status = member->getPropertyString( "status" );

  // If he went offline or hidden, just send FLN handle, otherwise send NLN
  if( status == "FLN" || status == "HDN" )
  {
    command << "FLN" << member->getPropertyString( "handle" );
  }
  else
  {
    if( name.isEmpty() )
    {
      name = member->getPropertyString( "friendly" );
    }
    if( name.isEmpty() )
    {
      name = member->getPropertyString( "handle" );
    }

    command << "NLN"
      << member->getPropertyString( "status" )
      << member->getPropertyString( "handle" )
      << member->getPropertyString( "verified" )
      << QUrl::toPercentEncoding( name )
      << member->getPropertyString( "capabilities" )
      << member->getPropertyString( "msnobject" );
  }

  sendCommand( command );
}



/**
 * A member in the contacts' list changed friendly name.
 */
void NotificationConnection::contactChangedFriendlyName( Account *member, const QString &name )
{
  // Is sent in the same command as the status change. Therefore, we simply send
  // NLN (this method is not called if status is FLN / HDN)
  contactChangedStatus( member, name );
}



/**
 * A member in the contacts' list changed uux information.
 */
void NotificationConnection::contactChangedUux( Account *member )
{
  QStringList ubxCommand( "UBX" );
  const QByteArray &uux = member->getPropertyBA( "uux" );
  const QString &handle = member->getPropertyString( "handle" );

  // I don't know what the 1 there is for... But KMess doesn't use it,
  // so I guess it can just stay at 1.
  // (if someone knows, change it at the initial UBX report too)
  sendCommand( ubxCommand << handle << QString::number( 1 )
    << QString::number( uux.size() ), uux );
}



/**
 * This connection previously requested a switchboard server and just got it.
 */
void NotificationConnection::createdSwitchboardServer( int trId, const QString &address, const QString &token )
{
  QStringList cmd( "XFR" );
  sendCommand( cmd << QString::number( trId ) << "SB" << address << "CKI" <<
       token << "U" << "messenger.msn.com" << "1" );
}

/**
 * Return the handle.
 */
const QString &NotificationConnection::handle()
{
  return handle_;
}



// A command was received from a connection
void NotificationConnection::incomingCommand( const QStringList &command, const QByteArray &payload )
{
  if( payload.isEmpty() )
  {
    debug_protocol( "Received " + command[0] + " command:\n" + command.join( " " ), true );
  }
  else
  {
    debug_protocol( "Received " + command[0] + " payload command, with " +
                        QString::number( payload.size() ) +
                        "bytes payload:<br/>" + command.join(" "), true );
  }

  bool hasTrId = false;
  if( command.size() > 1 )
  {
    command[1].toInt( &hasTrId );
    if( hasTrId == true )
      trId_ = command[1].toInt();
  }

  QStringList replyCommand;

  // Set the command for the reply to the name and transaction id, so we can fill it with some other stuff
  for( int i = 0; i < 2; i++ )
  {
    if( command.size() <= i )
    {
      break;
    }
    replyCommand.append( command[ i ] );
  }

  // Check if we got VER yet
  if( protocolVersion_.isEmpty() && command[0] != "VER" )
  {
    // error out: we need to get VER first.
    warning( "Before VER was sent, got command " + command[0] + ". This is invalid, erroring out..." );
    // TODO: send error number
    closeConnection();
    return;
  }

  // Check if we got USR yet
  QStringList allowedAnonCommands;
  allowedAnonCommands << "VER" << "CVR" << "USR" << "OUT";
  if( handle_.isEmpty() && !allowedAnonCommands.contains( command[0] ) )
  {
    // error out: we need to log in first.
    warning( "Before USR was succesfully completed, got command " + command[0] + ". This is invalid, erroring out..." );
    // TODO: send error number
    closeConnection();
    return;
  }

  // Reply to the message, if it contains a known command

  if( command[0] == "VER" )
  {
    // Initial version info
    // Check received protocols with our supported protocols and find first
    // match.

    // Our supported list - start with latest, then count down
    // for example: << "MSNP15" << "MSNP14" << "MSNP13" << "CVR0"
    QStringList supportedServerProtocols;
    supportedServerProtocols << "MSNP15" << "MSNP18";

    // client supported protocols start at 2, go on until end
    for( int client = 2; client < command.size(); ++client )
    {
      for( int server = 0; server < supportedServerProtocols.size(); ++server )
      {
        if( command[client] == supportedServerProtocols[server] )
        {
          // Found a match!
          protocolVersion_ = command[client];
        }
      }
    }

    if( protocolVersion_.isEmpty() )
    {
      error( "Unable to find a protocol match between client and server. "
            "Client sent: " + command.join(",") + "; server supports: "
           + supportedServerProtocols.join(",") );
      // TODO: error number
      closeConnection();
      return;
    }

    debug("Communicating using protocol version " + protocolVersion_);

    sendCommand( replyCommand << protocolVersion_ );
  }
  else if( command[0] == "CVR" )
  {
    sendCommand( replyCommand << "2.0.0000" << "2.0.0000" << "2.0.0000"
                              << "http://www.kmess.org/download/" << "http://www.kmess.org/" );
  }
  else if( command[0] == "USR" )
  {
    if( command[2] != "SSO" )
    {
      warning( "Client requested non-SingleSignOn authentication, this server only supports SSO at this moment." );
      // TODO return valid error
      closeConnection();
      return;
    }

    const QString handle( command[4] );

    Account *account = LiveService::instance()->getAccount( handle );

    if( ! account )
    {
      // For now, close if the account is unknown
      // TODO: return valid error response
      warning( "Account " + handle + " not found, terminating connection." );
      closeConnection();
      return;
    }

    debug( "Account " + handle + " found, starting client authentication." );

    // the MSN servers send a GCF command here with a payload that contains
    // the "words you can't say in MSN" in base64. We don't have that bullcrap,
    // but if WLM chokes on this, maybe we should send an empty GCF command.
    
    // the Live servers actually want to *authenticate* their client, so they
    // send a reply like this: USR \d SSO S MBI_KEY_OLD [Base64Nonce]
    // the MBI_KEY_OLD is the "policy" which goes to the PassportLoginService,
    // and then there's the nonce that's saved in MsnNotificationConnection.
    // The PassportLoginService then logs in to SOAP, re-sends the USR with the
    // Messenger login token, and *then* the "authentication OK" respond
    // is sent: USR \d OK handle 1 [verified?0:1]
    // We can actually implement this real authentication now SOAP
    // authentication works; then we can check the ticket for its validity
    // (correct handle? not expired?) using the TokenManager, and send OK or
    // whatever is sent as "NOT OK".

    // The second USR gets two tokens. The first one is the token that was
    // returned in the RequestSecurityTokenResponse for messengerclear.live.com,
    // in de-htmlized form. The second token does not appear in the security
    // token response, so there's some converting going on. This should be
    // visible somewhere in KMess itself.

    // However, full authentication support is a TODO at this moment, we just
    // accept the authentication always.
    // (if we do check auth, *don't* set handle_ until auth is complete; if we
    // need to remember handle (and we do), add loggingInHandle_).

    // accept the user request
    handle_ = handle;
    sendCommand( replyCommand << "OK" << handle << "1" << account->getProperty( "verified" ).toString() );

    emit loggedIn();

    // If we keep e-mail for this user, we also send a payloaded MSG message
    // here. But we don't, atm.
  }
  else if( command[0] == "OUT" )
  {
    closeConnection();
  }
  else if( command[0] == "BLP" )
  {
    QString privacySetting = command[2];

    // privacySetting is always either BL or AL - I think it means whether an
    // unknown contact is by default in the allowed or blocked list. We just
    // reply with "ok, whatever", this is unimplemented anyway.
    sendCommand( replyCommand << privacySetting );
  }
  else if( command[0] == "PRP" )
  {
    // Property change.
    // See docs for KMess' MsnNotificationConnection::changeProperty() for a
    // detailed list of properties etc.
    QString propertyName = command[2];
    QString propertyValue = QUrl::fromPercentEncoding( command[3].toAscii() );

    QStringList ignoredProperties;
    ignoredProperties << "PHH" << "PHW" << "PHM" << "MOB" << "MBE"
      << "WWE" << "HSB";

    if( propertyName == "MFN" )
    {
      // Friendly name
      // TODO: de-htmlentities-alize propertyValue - or don't and save its
      // format clearly!
      // Important: Don't update the AbService about this change! If it needs
      // to be remembered, the client will send an ABContactUpdate SOAP request
      // to the AbService.

      emit changedFriendlyName( propertyValue );
      debug(handle_ + " changed friendlyname to " + propertyValue);
    }
    else if( ! ignoredProperties.contains( propertyName ) )
    {
      // Unknown property!
      warning( "Unknown PRP property " + propertyName );
    }

    // But just say it changed, anyway
    sendCommand( replyCommand << propertyName << propertyValue );
  }
  else if( command[0] == "ADL" )
  {
    // add to contact list
    // sent initially by kmess just after it receives the current contact list.
    // also sent later to add people into the contact list.

    // I think this command is sent to let the notification connection know all
    // contacts in the contact list. This way, it doesn't have to request it
    // itself. The list kept in this class (object) will therefore *not* be
    // synced with the AbService or the actual Account class, it is just for
    // this session.

    // The payload for this message contains XML data; a <ml> tag containing,
    // for each contact, a <d n="domain.com"> tag containing a
    // <c n="user" ...other.../> tag. Could be that the c's should actually
    // be grouped so there's one <d> for each domain, but this is how KMess
    // sends it, anyway, and the Live server seems to eat it.

    QDomDocument mlist;
    if( !mlist.setContent( payload, false ) )
    {
      warning( "Received invalid membership list in notification connection."
        " Closing connection." );
      // TODO valid error
      closeConnection();
    }

    QDomElement ml = mlist.namedItem( "ml" ).toElement();
    if( ml.attribute( "l" ) != "1" )
    {
      warning("Attribute l (list?) to ml (membership list?) is not 1.");
    }

    QDomNodeList domains = ml.childNodes();
    for( int di = 0; di < domains.size(); ++di )
    {
      QDomElement domain = domains.at( di ).toElement();

      QString domainPart = domain.attribute( "n" );
      if( domainPart.isEmpty() )
      {
        warning("Got a <d> tag in the membership list without a domain name!");
        continue;
      }

      QDomNodeList contacts = domain.childNodes();

      for( int ci = 0; ci < contacts.size(); ++ci )
      {
        QDomElement contact = contacts.at( ci ).toElement();

        QString handle = contact.attribute( "n" );
        if( handle.isEmpty() )
        {
          warning("Got a <c> tag in the membership list for domain name "
            + domainPart + "without a local part!");
          continue;
        }

        handle.append( "@" + domainPart );
        QString list = contact.attribute( "l" );
        QString type = contact.attribute( "t" );

        debug( "ADL: " + handle + ": " + list + " " + type );

        contactList_[ handle.toLower() ].first  = list.toInt();
        contactList_[ handle.toLower() ].second = type.toInt();
      }
    }

    sendCommand( replyCommand << "OK" );
  }
  else if( command[0] == "PNG" )
  {
    // Pong!
    // The reply command is QNG. Other than that, KMess doesn't check any
    // syntax.
    replyCommand[0] = "QNG";
    sendCommand( replyCommand );
  }
  else if( command[0] == "CHG" )
  {
    // User is changing his status
    QString newStatus = command[2];
    QString capabilities = command[3];
    QString msnObject = command[4];

    debug( "User changes status to " + newStatus + ", capabilities to " + capabilities );
    debug( "MsnObject: " + msnObject );

    emit changedStatus( newStatus, capabilities, msnObject );

    sendCommand( replyCommand << newStatus << capabilities << msnObject );

    // If this is the initial CHG, we now send some ILN's.
    if( ! initialSent_ )
    {
      // By now, we received the contact list in ADL. This list was saved in
      // contactList_ and will allow us to keep track of users' status now.
      // For each contact in that list, we will get their initial status, and
      // if not FLN, we will send ILN (initial status report) and UBX (media,
      // personal message, etc) for them.

      QHashIterator<QString,QPair<int,int> > it( contactList_ );
      while( it.hasNext() )
      {
        it.next();
        const QString &handle = it.key();
        Account *account = LiveService::instance()->getAccount( handle );

        if( account == 0 )
        {
          // Unknown account, skip
          continue;
        }

        const QString &status = account->getPropertyString( "status" );
        if( status != "FLN" && status != "HDN" )
        {
          // not so sure about the fourth argument, but it could be "!verified"
          // - it's always 1 here, I need an unverified account to check
          QStringList ilnCommand( "ILN" );

          sendCommand( ilnCommand << QString::number( 0 ) << status << handle
              << QString::number( 1 )
              << QUrl::toPercentEncoding( account->getPropertyString( "friendly" ) )
              << account->getPropertyString( "capabilities" )
              << account->getPropertyString( "msnobject" ) );

          const QByteArray &uux = account->getPropertyString( "uux" ).toAscii();
          if( !uux.isEmpty() )
          {
            QStringList ubxCommand( "UBX" );

            // I don't know what the 1 there is for... But KMess doesn't use it,
            // so I guess it can just stay at 1.
            // (if someone knows, change it at contactChangedUux too)
            sendCommand( ubxCommand << handle << QString::number( 1 )
                << QString::number( uux.size() ), uux );
          }
        }
      }

      initialSent_ = true;
    }
  }
  else if( command[0] == "URL" )
  {
    // unhandled by us, currently, because we don't do e-mail.
    // The argument to this seems to be something like a group or object type
    // ("INBOX", "COMPOSE", "PROFILE", "PERSON") and the server response is
    // something that looks like a post URL to directly post changes to, and a
    // login or Live SSO login page, it seems.

    // Don't send a reply here, and let's hope the client takes that. It
    // shouldn't have sent the URL in the first place.
  }
  else if( command[0] == "UUN" )
  {
    // MSNP18 client announces it supports the multiple login feature,
    // and gives its handle, type and some payload
    if( protocolVersion_ != "MSNP18" ) {
      warning( "Non-MSNP18 client sent UUN command" );
      closeConnection();
      return;
    }
    // otherwise, send nothing
  }
  else if( command[0] == "UUX" )
  {
    // We get an updated PSM + currentmedia in the payload.
    debug( "User changes media or personal message in payload." );

    emit changedUux( payload );

    // The 0 below seems to indicate that even though UUX is a payload
    // command, it has no payload here, since it's just an ACK.
    sendCommand( replyCommand << "0" );
  }
  else if( command[0] == "XFR" )
  {
    QString requestedServer = command[2];

    if( requestedServer != "SB" )
    {
      warning( "Client requested non-switchboard type server, type=" + requestedServer );
      // TODO return error
      closeConnection();
      return;
    }

    // Switchboard connection requested!
    // The NotificationServer will react to this signal by creating a
    // listening SwitchboardServer. The NS will then call the
    // createdSwitchboardServer slot in this class so we can send a response.
    emit switchboardRequested( command[1].toInt() );
  }
  else if( command[0] == "QRY" )
  {
    // Response to a Live Messenger "CHL" challenge. We don't send those,
    // so there's no real way to check validity (actually they are already
    // invalid because it's a response to CHL). Maybe WLM requires us to send
    // CHL's, in that case we should send one now and then. KMess'
    // MsnChallengeHandler contains the code to generate a QRY response to CHL, 
    // so we could use that to check this response. For now, just accept it.
    sendCommand( replyCommand );
  }
  else
  {
    error( "Received unsupported command:" + command[0] + "closing connection." );
    closeConnection();
  }
}



/**
 * Contact was invited into a switchboard.
 */
void NotificationConnection::invitedToSwitchboard( const QString &address, const QString &token, const QString &handle, const QString &friendly )
{
  QStringList command( "RNG" );
  // The 1234 is the chat ID. We don't use it ourselves ATM, so it's basically
  // random.
  sendCommand( command << "1234" << address << "CKI" << token << handle
      << QUrl::toPercentEncoding( friendly ) << "U" << "messenger.msn.com"
      << "1" );
}



bool NotificationConnection::isLoggedIn()
{
  return !handle_.isEmpty();
}



/**
 * The user logged in from another location, log him out here.
 */
void NotificationConnection::loggedInFromOtherLocation()
{
  // TODO error properly
  closeConnection();
}
