/***************************************************************************
      dispatchserver.cpp - Faux Dispatch Server
                             -------------------
    begin                : Mon March 30, 2009
    copyright            : (C) 2009 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "dispatchconnection.h"
#include "../liveservice/liveservice.h"
#include "../msnservergroup.h"



// Constructor
DispatchConnection::DispatchConnection( int connectionId, MsnServerGroup *msnServerGroup )
: MsnpConnection( connectionId, msnServerGroup )
, trId_( -1 )
{
  setObjectName( "DispatchConnection#" + QString::number( connectionId ) );
}



// Destructor
DispatchConnection::~DispatchConnection()
{
}


// A command was received from a connection
void DispatchConnection::incomingCommand( const QStringList &command, const QByteArray &payload )
{
  if( payload.isEmpty() )
  {
    debug_protocol( "Received " + command[0] + " command:\n" + command.join( " " ), true );
  }
  else
  {
    debug_protocol( "Received " + command[0] + " payload command, with " +
                        QString::number( payload.size() ) +
                        " bytes payload:<br/>" + command.join(" "), true );
  }

  bool hasTrId = false;
  if( command.size() > 1 )
  {
    command[1].toInt( &hasTrId );
    if( hasTrId == true )
      trId_ = command[1].toInt();
  }

  QStringList replyCommand;
  QByteArray  payloadData;

  // Set the command for the reply to the name and transaction id, so we can fill it with some other stuff
  for( int i = 0; i < 2; i++ )
  {
    if( command.size() <= i )
    {
      break;
    }
    replyCommand.append( command[ i ] );
  }

  // Reply to the message, if it contains a known command

  if( command[0] == "VER" )
  {
    // Initial version info

    // Only reply with "VER <ackID> <MSNP version>"

    sendCommand( replyCommand << command[2] );
  }
  else if( command[0] == "CVR" )
  {
    sendCommand( replyCommand << "2.0.0000" << "2.0.0000" << "2.0.0000"
                              << "http://www.kmess.org/download/" << "http://www.kmess.org/" );
  }
  else if( command[0] == "USR" )
  {
    replyCommand[0] = "XFR";
    QString hostname( LiveService::instance()->getSettingString( "hostname" ) );
    hostname.append( ":1864" );
    sendCommand( replyCommand << "NS" << hostname << "U" << "D" );
  }
  else if( command[0] == "OUT" )
  {
    closeConnection();
  }
  else
  {
    warning( "Received unsupported command:" + command[0]
           + " - closing connection." );
    closeConnection();
  }
}


