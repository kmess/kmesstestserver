/***************************************************************************
            msnpconnection.h - TCP connection instance
                             -------------------
    begin                : Sat March 28, 2009
    copyright            : (C) 2009 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MSNPCONNECTION_H
#define MSNPCONNECTION_H

#include "abstractconnection.h"

#include <QStringList>
#include <QAbstractSocket>

class QTcpSocket;


/**
 * @brief TCP connection.
 *
 * This class handles a simple TCP connection, which relays data to and from
 * the manager class.
 *
 * @author Valerio Pilo
 */
class MsnpConnection : public AbstractConnection
{
  Q_OBJECT


  public:

    // Constructor
                       MsnpConnection( int id, QObject *parent = 0 );
    // Destructor
    virtual           ~MsnpConnection();
    // Retrieve the connection client name
    virtual QString    client() const;
    // Close the connection to the client
    virtual void       closeConnection();
    // A connection was initiated from a client.
    virtual void       initialize( int socketDescriptor );
    // Send commands to the connected client
    void               sendCommand( const QStringList &command, const QByteArray &payload = QByteArray() );

  protected:

    // A request was received
    virtual void       incomingCommand( const QStringList &command, const QByteArray &payload ) = 0;


  private slots:

    // A socket error has been received
    void               socketError( QAbstractSocket::SocketError error );
    // Incoming data from the socket
    void               readTcpData();

  protected slots:

    // The socket was closed
    virtual void       connectionClosed();

  private: // Private properties

    // Size of the payload command which is being received
    QStringList        nextPayloadCommand_;
    // Size of the payload which is being received
    int                nextPayloadSize_;


  private: // Static private properties

    // Accepted payload commands
    static QStringList payloadCommands_;
    // The TCP socket
    QTcpSocket        *tcpSocket_;

};



#endif
