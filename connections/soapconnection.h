/***************************************************************************
            soapconnection.h - SOAP Connection Instance
                             -------------------
    begin                : Sat March 28, 2009
    copyright            : (C) 2009 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SOAPCONNECTION_H
#define SOAPCONNECTION_H

#include "abstractconnection.h"

#include <QAbstractSocket>
#include <QMap>
#include <QObject>

// Forward declarations
class QSslSocket;
class SoapHandler;



/**
 * @brief HTTP over SSL connection.
 *
 * This class handles a simple HTTP over SSL connection, which relays data to and
 * from the manager class.
 *
 * @author Valerio Pilo
 * @ingroup debug
 */
class SoapConnection : public AbstractConnection
{
  Q_OBJECT


  public:

    // Constructor
                       SoapConnection( int id, QObject *parent, QMap<QString,SoapHandler*> *serverPages = 0 );
    // Destructor
                      ~SoapConnection();
    // Retrieve the SSL connection client name
    virtual QString    client() const;
    // Close the connection to the client
    virtual void       closeConnection();
    // A connection was initiated from a client. Start the SSL encryption mode
    virtual void       initialize( int socketDescriptor );
    // Send data to the connected client
    void               sendResponse( const QByteArray &xmlBody );


  private slots:

    // The socket was closed
    virtual void       connectionClosed();
    // A socket error has been received
    void               socketError( QAbstractSocket::SocketError error );
    // Incoming data from the socket
    void               readHttpData();
    // Parse the received HTTP POST data
    void               readSoapData();
    // The socket is ready for secure transmission
    void               ready();


  private:

    // Send an HTTP error message
    void               sendHttpError( const QString &preamble );
    // Send a HTTP server response (the most low level function here)
    void               sendHttpResponse( const QString &preamble, const QString &contentType, const QByteArray &body );


  private: // Private properties
    // Buffer used to temporarily store the HTTP request body
    QByteArray         requestBody_;
    // Size of a request which is being received
    int                requestBodySize_;
    // Buffer used to temporarily store the HTTP header
    QByteArray         requestHeader_;
    // A list of all server pages found at the system.
    QMap<QString,SoapHandler*> *serverPages_;
    // The internal ssl (de-)ciphering device
    QSslSocket        *sslSocket_;


  signals:

    // A request was received
    void               incomingRequest( const QByteArray &header, const QByteArray &body );

};



#endif
