/***************************************************************************
          soapconnection.cpp - SOAP Connection Instance
                             -------------------
    begin                : Sat March 28, 2009
    copyright            : (C) 2009 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "soapconnection.h"
#include "../servers/soapserver.h"
#include "../soaphandlers/soapmessage.h"
#include "../soaphandlers/soaphandler.h"
#include "../util/shared.h"

#include <QHostAddress>
#include <QSslCertificate>
#include <QSslKey>
#include <QSslSocket>
#include <QCoreApplication>



// Constructor
SoapConnection::SoapConnection( int id, QObject *parent, QMap<QString,SoapHandler*> *serverPages )
: AbstractConnection( id, parent )
, requestBodySize_( 0 )
, serverPages_( serverPages )
, sslSocket_( 0 )
{
  setObjectName( "SoapConnection#" + QString::number( id ) );

  if( serverPages_ == 0 )
  {
    qWarning() << "Got zero serverPages_ in SoapConnection constructor. "
                  "This SoapConnection will be unable to handle requests.";
    // TODO is this right? needed for soapserver.cpp:94
    serverPages_ = new QMap<QString,SoapHandler*>;
  }
}



// Destructor
SoapConnection::~SoapConnection()
{

}



// Retrieve the SSL connection client name
QString SoapConnection::client() const
{
  if( sslSocket_ == 0 || sslSocket_->peerAddress().isNull() )
  {
    return QString( "*not connected*" );
  }

  return ( sslSocket_->peerAddress().toString() + ":" + sslSocket_->peerPort() );
}



// Close the connection to the client
void SoapConnection::closeConnection()
{
  if( sslSocket_ == 0 )
  {
    warning( "Socket not initialized!" );
    return;
  }

  sslSocket_->close();
}



// The socket was closed
void SoapConnection::connectionClosed()
{
  debug( "...SSL connection closed." );

  // Although the socket may be closing, we must not delete it until
  // the delayed close is done.
  // When the socket is deleted, this connection will be also deleted.
  if( sslSocket_->state() == QAbstractSocket::ClosingState )
  {
    connect( sslSocket_, SIGNAL( disconnected() ),
             sslSocket_, SLOT  (  deleteLater() ) );
  }
  else
  {
    sslSocket_->deleteLater();
  }
}



// A socket error has been received
void SoapConnection::socketError( QAbstractSocket::SocketError socketError )
{
  error( "Error id " + QString::number( socketError ) +
         ": \"" + sslSocket_->errorString() +
         "\" received!" );
}



// A connection was initiated. Start the SSL encryption mode
void SoapConnection::initialize( int socketDescriptor )
{
  // Apply an SSL socket over the incoming socket handle.
  sslSocket_ = new QSslSocket( this );
  connect( sslSocket_, SIGNAL(   destroyed(QObject*) ),
           this,       SLOT  ( deleteLater()         ) );

  sslSocket_->setProtocol( QSsl::AnyProtocol );

  // The SSL server needs a key and a certificate, which I generated on the fly locally.
  QSslKey key( QByteArray(
    "-----BEGIN RSA PRIVATE KEY-----\n"
    "MIIEogIBAAKCAQEAvseyi8vkBO+b2zjhSUueGezg08BQAvxOAceTuBwtqu3oiEdC\n"
    "qcyKA2iJQr1axN/Y/5kAdGmm+iHL8054d3ZC/CN123WtYd9brN99H53S3BJMrK/M\n"
    "PBpPlugCmgIYfglYZFPtz8medxi6pEnK6ljkTzbZ5iX0zib1JWZcd3lTBcxfEHMy\n"
    "ek7JLzZ812jmOOuRVyy000UF9PF0W+OXhyW1QiHx8mSmvHZ+QMz7HhUNdGW1Ios3\n"
    "WjJnnGBOkYb68UWafg24Fs6gg0OT8xuu0TeuKVG8Kxoa7ESmolo4CjXk35SwtJfX\n"
    "BcCkhPmm8hPiwzdv0ePMripVQPrRXeNM4xOAKwIBIwKCAQEAgtIUCBbIPeYhuuUs\n"
    "xIufnL+wHCwZm6Wx1VWnHylD4uxHree4rvKnx9KnQ7ULCqgfxTW3K0EiCprU/pw1\n"
    "WTsmnkQzjywfHoqIAYNOfBRzVRPczi9neaukSjixU6mqZQ249FbO9OIGQwmkjel1\n"
    "MvsgNlF/eT6ZPOeDh1wiJgoNC0kfILLNVJ+EmgUHB/+hfauwJ9+XzXI0BNHpK2R3\n"
    "AI5+3nR2FAzvR4jFccKcAlEwmRrPnYzH5OHWkPDL/1ONVwi0BmGO5Vq14B9ROXVE\n"
    "DSTQ2Uy9yFB+IXhgiC9R0pRTmXzi6U4oJ1YAh7z6iBu7GVrudjbUDwdmBkwW1z04\n"
    "+m/7SwKBgQD08xkFt6bmAlK8/UU6CC7YfbDiziBbgAE9upAXn5tKsLWHGloThgdW\n"
    "lHvBI/jGS/9dzhEZmIAwFOrcHmYHZ8B4IHkkHMgIk7DH9CSHFlYOIFmUVGe0c0cw\n"
    "gHfB7jTkTWAFJg4qrHpv9xgjgJpCuSWQI8BOsmzRN9mBgyN+QJEuZQKBgQDHYwAW\n"
    "pz9BzEcVmSQ2BCJX9KsUpDNz7ejCl2D9BrqcN60+pXe2Q3JdERVrzBCK88nojrd6\n"
    "PpOJSWueSXHFaphxKZujljekCLp1iywkfQvvdr2TEpJZMtiUBfLJc5Nb/Qk1+k0H\n"
    "H+jJHUNAwFEvL8KZQb4zZdK56p0D4Nvbip8DTwKBgBT+5OM7oJdfSOugrimLrD5x\n"
    "LGs2PUmr4tlnwzU5kPfGAO5St0OAhEk/7VmyoEuC21/P1ZR6ySixVfWcNKGMjNcY\n"
    "ue0nCdTZdY17U5aM4s4CxdmDk9xEZTALAvNdj4FIdfHQD9fFpBg5wD2OriL57Ube\n"
    "fjKhlExygFuO5cj+OFRvAoGBAJnQB2Huu8ULTMeExCmravNk+QFLeCY7L+aSCP20\n"
    "u9eYqjekN8ceFmUNLcgvtP16PKwWUwaIC2noLnLK8V3HQmXs5cdWn/ORtGlOGr0Q\n"
    "AeSdbav4YkTPc9/nVOSMW7v9tqX7moHWyYUz1MuNC22LPlj4M6P+F5a8TT2Bk6lc\n"
    "TsgJAoGAN2nWZgao+55HlyFHu5+n+dQgDOzMivUCdZb+gbBWVeXo60jTmxdL94ZY\n"
    "FDe3G3fEusg6h4X7K3CiIAdGHYNdt6RjLwhqG3cNJoO5CJ+Xe2jGCVHacT39RlJS\n"
    "005rkG3TIf2P/k4vBHjjgK8tHlBQ5a1yhG557ZUCrygCOja4vRU=\n"
    "-----END RSA PRIVATE KEY-----\n"
      ), QSsl::Rsa );
  QSslCertificate cert( QByteArray(
    "-----BEGIN CERTIFICATE-----\n"
    "MIIC7DCCAdQCCQDwGpSxd3zsIDANBgkqhkiG9w0BAQUFADA5MQswCQYDVQQGEwJJ\n"
    "VDEOMAwGA1UECBMFSXRhbHkxGjAYBgNVBAoTEVRoZSBLTWVzcyBQcm9qZWN0MB4X\n"
    "DTA5MDMyOTAxNTcwMFoXDTE5MDMyNzAxNTcwMFowOTELMAkGA1UEBhMCSVQxDjAM\n"
    "BgNVBAgTBUl0YWx5MRowGAYDVQQKExFUaGUgS01lc3MgUHJvamVjdDCCASAwDQYJ\n"
    "KoZIhvcNAQEBBQADggENADCCAQgCggEBAL7HsovL5ATvm9s44UlLnhns4NPAUAL8\n"
    "TgHHk7gcLart6IhHQqnMigNoiUK9WsTf2P+ZAHRppvohy/NOeHd2Qvwjddt1rWHf\n"
    "W6zffR+d0twSTKyvzDwaT5boApoCGH4JWGRT7c/JnncYuqRJyupY5E822eYl9M4m\n"
    "9SVmXHd5UwXMXxBzMnpOyS82fNdo5jjrkVcstNNFBfTxdFvjl4cltUIh8fJkprx2\n"
    "fkDM+x4VDXRltSKLN1oyZ5xgTpGG+vFFmn4NuBbOoINDk/MbrtE3rilRvCsaGuxE\n"
    "pqJaOAo15N+UsLSX1wXApIT5pvIT4sM3b9HjzK4qVUD60V3jTOMTgCsCASMwDQYJ\n"
    "KoZIhvcNAQEFBQADggEBABlLQY8qDi2bWkshv1ovnwkLtUBslQ6tGcnr36HUfe5N\n"
    "XlPRl8j/7k08EWrK8mOWecw1vZYxYDg7aEIrYHFmwVR+U5HlrXyuXLWldNadWHAT\n"
    "onPbiXMzf2cG9OatL0h0rJaMX3PAk4o97rmCr2L0NWOtjvg3eoMxpijp+8ov6q4a\n"
    "214UJHm1T02sQkXbSYiy1ruddqBnt4wxkSDx7XfjDdMTnMKKMxjSkUsT7keded+o\n"
    "CJ2qFPl73B7KzzfLdrbhb84hxfR9QmlF/F8PVX/dJf4axVtOnzT12H5Inqy5hqVx\n"
    "pdeMdam134DxBvEQB1CgXfzOw8xjgxdEDENcGmkDKcs=\n"
    "-----END CERTIFICATE-----\n"
      ) );
  sslSocket_->setLocalCertificate( cert );
  sslSocket_->setPrivateKey( key );

  // Start the SSL handshake
  if( ! sslSocket_->setSocketDescriptor( socketDescriptor ) )
  {
    error( "SSL Connection failed" );
    delete sslSocket_;
    return;
  }

  // Connect the SSL socket's signals
  connect( sslSocket_, SIGNAL(        sslErrors(const QList<QSslError>&)      ),
           sslSocket_, SLOT  (  ignoreSslErrors()                             ) );
  connect( sslSocket_, SIGNAL(        encrypted()                             ),
           this,       SLOT  (            ready()                             ) );
  connect( sslSocket_, SIGNAL(     disconnected()                             ),
           this,       SLOT  ( connectionClosed()                             ) );
  connect( sslSocket_, SIGNAL(        readyRead()                             ),
           this,       SLOT  (     readHttpData()                             ) );
  connect( sslSocket_, SIGNAL(            error(QAbstractSocket::SocketError) ),
           this,       SLOT  (      socketError(QAbstractSocket::SocketError) ) );

  // Start the ciphered session
  sslSocket_->startServerEncryption();

//   emit logMessage( "Starting SSL Connection", TYPE_STATUS );
}



// Incoming data from the socket
void SoapConnection::readHttpData()
{
  // If the header wasn't received yet, wait for it
  int headerEnd = requestHeader_.indexOf( "\r\n\r\n" );
  if( requestHeader_.isEmpty() || headerEnd < 0 )
  {
    requestHeader_.append( sslSocket_->readAll() );

    // Did we reach the end of the header yet?
    headerEnd = requestHeader_.indexOf( "\r\n\r\n" );
    if( requestHeader_.isEmpty() || headerEnd < 0 )
    {
      // No, wait for the rest of it
      return;
    }
    // Yes, fall through to parse the header and move extraneous
    // data to the body
  }

  // Find out how much data the request contains
  if( requestBodySize_ == 0 )
  {
    QRegExp sizeHeader( "Content-Length: (\\d+)\r\n" );

    if( sizeHeader.indexIn( requestHeader_ ) < 0 )
    {
      warning( "Client sent an invalid request!" );
      sendHttpError("400 Bad Request");
      return;
    }
    requestBodySize_ = sizeHeader.cap( 1 ).toInt();
    requestBody_.reserve( requestBodySize_ );

    // Include the last two newlines in the header
    headerEnd += 4;

    requestBody_.append( requestHeader_.mid( headerEnd ) );
    requestHeader_.truncate( headerEnd );
  }

  requestBody_.append( sslSocket_->readAll() );

  // Still waiting for the full request
  if( requestBody_.size() < requestBodySize_ )
  {
    return;
  }
  // TODO: else if( requestBody_.size() > requestBodySize_ )
  //    { feed it back into the socket } in case we get two requests at the same
  //    time.


  // Received the full request, parse SOAP message
  readSoapData();

  // Clean up for next request
  requestBody_.clear();
  requestHeader_.clear();
  requestBodySize_ = 0;
}



// Parse the received HTTP POST data
void SoapConnection::readSoapData()
{
  // Log the incoming message as well
  debug_protocol( requestHeader_ + requestBody_, true );
  Q_ASSERT( !requestHeader_.isEmpty() );
  Q_ASSERT( !requestBody_.isEmpty()   );

  // Parse the HTTP header to find the requested site + path.
  QRegExp queryHeader( "([A-Z]+) ([^\\s]+) HTTP/(\\d\\.\\d)\r\n" );
  QRegExp hostHeader( "Host: ([^\\r]+)\r\n" );
  QRegExp soapHeader( "SOAPAction: ([^\\r]+)\r\n" );

  if( queryHeader.indexIn( requestHeader_ ) < 0
  ||  hostHeader.indexIn( requestHeader_ ) < 0 )
  {
    warning( "Client sent an invalid request!" );
    sendHttpError("400 Bad Request");
    return;
  }

  // Get the standard HTTP headers
  QString method  = queryHeader.cap( 1 );
  QString path    = queryHeader.cap( 2 );
  QString version = queryHeader.cap( 3 );  // assume 1.1 for now.
  QString host    = hostHeader.cap( 1 );

  // Get SOAPAction, is optional though...
  QString soapAction;
  if( soapHeader.indexIn( requestHeader_ ) >= 0 )
  {
    soapAction = soapHeader.cap(1);
  }

  // Assume POST
  if( method != "POST" )
  {
    warning( "Client send unexpected request method: " + method );
    sendHttpError("405 Method Not Allowed");
    return;
  }

  debug( "Looking for serverPage for address: " + host + path );
  SoapHandler *soapHandler = serverPages_->value( host + path );
  if( soapHandler == 0 )
  {
    // Page not found.
    warning( "Did not find server page for address: " + host + path );
    sendHttpError("404 Not Found");
    return;
  }

  // Found a soap server instance which handles the request for this HTTP page.
  SoapMessage request( requestBody_, soapAction );
  SoapMessage response = soapHandler->processRequest( request );

  // Send the SOAP response in serialized format back over HTTP.
  sendHttpResponse( "200 OK", "text/xml; charset=utf-8", response.toBytes() );
}



// The socket is ready for secure transmission
void SoapConnection::ready()
{
  debug( "Opened SSL connection..." );
}



// Send an HTTP error message
void SoapConnection::sendHttpError( const QString &preamble )
{
  sendHttpResponse( preamble, "text/html; charset=utf-8", "Unable to process the request." );
}



// Send a HTTP server response (the most low level function here)
void SoapConnection::sendHttpResponse( const QString &preamble, const QString &contentType, const QByteArray &body )
{
  if( sslSocket_ == 0 || ! sslSocket_->isOpen() || ! sslSocket_->isEncrypted() )
  {
    if( sslSocket_ )
    {
      warning( "Unable to write on a non ready socket!"
               "Is encryption on? " +
               sslSocket_->isEncrypted() ? "yes" : "no" );
    }
    else
    {
      warning( "Socket not initialized!" );
    }
    return;
  }

  // Build a complete HTTP response
  QByteArray header( QString("HTTP/1.1 " + preamble + "\r\n"
                             "Content-Type: " + contentType + "\r\n"
                             "Content-Length: " + QString::number( body.size() ) + "\r\n"
                             "Connection: close\r\n"
                             "Server: " + qApp->applicationName() + "/" + qApp->applicationVersion() + "\r\n"
                             "X-Powered-By: Qt/" + qVersion() + "\r\n"
                             "\r\n"
                             ).toLatin1()
                      );

  debug_protocol( header + body, false );

  // Send it, and close the connection.
  sslSocket_->write( header + body );
  sslSocket_->flush();
  sslSocket_->close();
}



// Send data to the connected client
void SoapConnection::sendResponse( const QByteArray &xmlBody )
{
  sendHttpResponse("200 OK", "text/xml; charset=utf-8", xmlBody);
}


