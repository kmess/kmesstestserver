/**
 * @file loggingbase.h
 * @author Sjors Gielen <sjors@kmess.org>
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LOGGINGBASE_H
#define LOGGINGBASE_H

#include <QtCore/QObject>

/**
 * @brief A base class for logging capabilities.
 */
class LoggingBase
{
  public:
    /**
     * @brief Constructor.
     */
    LoggingBase();

    /**
     * @brief Message type for #_sendLogMessage().
     */
    enum MessageType {
      /// Outgoing protocol message.
      OutgoingProtocolMessageType,
      /// Incoming protocol message.
      IncomingProtocolMessageType,
      /// A debug message.
      DebugMessageType,
      /// An information message.
      InfoMessageType,
      /// A warning message.
      WarningMessageType,
      /// An error message.
      ErrorMessageType,
      /// A fatal error message.
      FatalErrorMessageType,
    };

    /**
     * @brief Return message type string for given message type.
     */
    static QString messageTypeToString( MessageType type );
    /**
     * @brief Logging name of this object.
     */
    virtual QString loggingName() const = 0;
    /**
     * @brief Whether this class should silence log messages.
     * @see setLoggingSilent()
     */
    bool loggingSilent() const;
    /**
     * @brief Set whether this class should silence log messages.
     * @see loggingSilent()
     */
    void setLoggingSilent( bool loggingSilent );

  protected slots:
    /**
     * @brief Send a log message. Override this method in your implementation.
     */
    virtual void _sendLogMessage( const QString&, MessageType type ) const = 0;

    /**
     * @brief Add a protocol debug message to log.
     * @param incoming whether the new message is incoming (false if outgoing)
     */
    void debug_protocol(const QString&, bool incoming) const;

    /**
     * @brief Add a debug message to log.
     */
    void debug(const QString&) const;

    /**
     * @brief Add an info message to log.
     */
    void info(const QString&) const;

    /**
     * @brief Add a warning message to log.
     */
    void warning(const QString&) const;

    /**
     * @brief Add an error message to log.
     */
    void error(const QString&) const;

    /**
     * @brief Add a fatal error message to log.
     */
    void fatalError(const QString&) const;

  private:
    bool loggingSilent_;
};

#endif
