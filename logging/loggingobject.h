/**
 * @file loggingobject.h
 * @author Sjors Gielen <sjors@kmess.org>
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LOGGINGOBJECT_H
#define LOGGINGOBJECT_H

#include <QtCore/QObject>
#include "loggingbase.h"

class LoggingTcpServer;

/**
 * @brief A QObject-derived class with LoggingBase functionality
 */
class LoggingObject : public QObject, public LoggingBase
{
  Q_OBJECT

  public:
    /**
     * @brief Constructor.
     */
    LoggingObject( QObject *parent = 0 );
    /**
     * @brief Logging name of this object.
     */
    virtual QString loggingName() const;

  protected:
    /**
     * @brief Relay log messages from childs through this one.
     */
    void relayChildLogMessages( const LoggingObject *obj );

    /**
     * @brief Relay log messages from childs through this one.
     */
    void relayChildLogMessages( const LoggingTcpServer *obj );

  private:
    /**
     * @brief Log message implementor.
     */
    void _sendLogMessage( const QString &message,
                          LoggingBase::MessageType type ) const;

  signals:
    /**
     * @brief This object emitted a log message.
     *
     * Use QObject::objectName() on the QObject::sender() to retrieve the
     * name of the object that sent the message.
     */
    void logMessage( const LoggingBase *sender, const QString &message,
                     LoggingBase::MessageType type ) const;
};



#endif
