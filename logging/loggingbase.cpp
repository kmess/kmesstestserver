/**
 * @file loggingbase.cpp
 * @author Sjors Gielen <sjors@kmess.org>
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "loggingbase.h"

LoggingBase::LoggingBase()
{}

QString LoggingBase::messageTypeToString( MessageType type )
{
  switch(type)
  {
  case OutgoingProtocolMessageType:
    return "Outgoing";
    break;
  case IncomingProtocolMessageType:
    return "Incoming";
    break;
  case DebugMessageType:
    return "Debug";
    break;
  case InfoMessageType:
    return "Info";
    break;
  case WarningMessageType:
    return "Warning";
    break;
  case ErrorMessageType:
    return "Error";
    break;
  case FatalErrorMessageType:
    return "Fatal";
    break;
  default:
    return "UNKNOWNTYPE";
  }
}

bool LoggingBase::loggingSilent() const
{
  return loggingSilent_;
}

void LoggingBase::setLoggingSilent( bool silent )
{
  loggingSilent_ = silent;
}

void LoggingBase::debug_protocol(const QString &message, bool incoming) const
{
  if( !loggingSilent() )
    _sendLogMessage( message, incoming ? IncomingProtocolMessageType
                                       : OutgoingProtocolMessageType );
}

void LoggingBase::debug(const QString &message) const
{
  if( !loggingSilent() )
    _sendLogMessage( message, DebugMessageType );
}

void LoggingBase::info(const QString &message) const
{
  if( !loggingSilent() )
    _sendLogMessage( message, InfoMessageType );
}

void LoggingBase::warning(const QString &message) const
{
  if( !loggingSilent() )
    _sendLogMessage( message, WarningMessageType );
}

void LoggingBase::error(const QString &message) const
{
  if( !loggingSilent() )
    _sendLogMessage( message, ErrorMessageType );
}

void LoggingBase::fatalError(const QString &message) const
{
  if( !loggingSilent() )
    _sendLogMessage( message, FatalErrorMessageType );
}

