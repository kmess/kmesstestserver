/**
 * @file loggingobject.cpp
 * @author Sjors Gielen <sjors@kmess.org>
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "loggingobject.h"
#include "loggingtcpserver.h"

LoggingObject::LoggingObject( QObject *parent )
: QObject( parent ), LoggingBase()
{}

void LoggingObject::relayChildLogMessages(const LoggingObject *obj)
{
  connect(  obj, SIGNAL(logMessage(const LoggingBase*, const QString&,
                                   LoggingBase::MessageType)),
           this, SIGNAL(logMessage(const LoggingBase*, const QString&,
                                   LoggingBase::MessageType)));
}

void LoggingObject::relayChildLogMessages(const LoggingTcpServer *obj)
{
  connect(  obj, SIGNAL(logMessage(const LoggingBase*, const QString&,
                                   LoggingBase::MessageType)),
           this, SIGNAL(logMessage(const LoggingBase*, const QString&,
                                   LoggingBase::MessageType)));
}

void LoggingObject::_sendLogMessage( const QString &message, MessageType type ) const
{
  emit logMessage( this, message, type );
}

QString LoggingObject::loggingName() const
{
  return objectName();
}

