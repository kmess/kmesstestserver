/**
 * @file loggingtcpserver.cpp
 * @author Sjors Gielen <sjors@kmess.org>
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "loggingtcpserver.h"
#include "loggingobject.h"

LoggingTcpServer::LoggingTcpServer( QObject *parent )
: QTcpServer( parent ), LoggingBase()
{}

void LoggingTcpServer::relayChildLogMessages(const LoggingObject *obj)
{
  connect(  obj, SIGNAL(logMessage(const LoggingBase*, const QString&,
                                   LoggingBase::MessageType)),
           this, SIGNAL(logMessage(const LoggingBase*, const QString&,
                                   LoggingBase::MessageType)));
}

void LoggingTcpServer::relayChildLogMessages(const LoggingTcpServer *obj)
{
  connect(  obj, SIGNAL(logMessage(const LoggingBase*, const QString&,
                                   LoggingBase::MessageType)),
           this, SIGNAL(logMessage(const LoggingBase*, const QString&,
                                   LoggingBase::MessageType)));
}

void LoggingTcpServer::_sendLogMessage( const QString &message, MessageType type ) const
{
  emit logMessage( this, message, type );
}

QString LoggingTcpServer::loggingName() const
{
  return objectName();
}

