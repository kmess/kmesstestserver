/***************************************************************************
           clientsession.cpp - KMess Testing MSN Server
                             -------------------
    begin                : Weg July 8, 2009
    copyright            : (C) 2009 by Diederik van der Boor
    email                : vdboor@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "clientsession.h"



// Constructor
ClientSession::ClientSession( QObject *parent )
: LoggingObject( parent )
, authenticated_( false )
{
  setObjectName("ClientSession[???]");
}



// Destructor
ClientSession::~ClientSession()
{
  debug( "Destroying " + objectName() );
}



// Return the handle
const QString & ClientSession::handle() const
{
  return handle_;
}



// Return whether the session is authenticated
bool ClientSession::isAuthenticated() const
{
  return authenticated_;
}



// Mark the session as authenticated
void ClientSession::setAuthenticated( bool authenticated )
{
  authenticated_ = authenticated;
}


// Assign the user
void ClientSession::setUser( const QString &handle, const QString &remoteIp )
{
  handle_ = handle;
  remoteIp_ = remoteIp;

  setObjectName("ClientSession[" + handle + "]");
}

