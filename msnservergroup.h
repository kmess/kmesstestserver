/***************************************************************************
            msnservergroup.h - KMess Testing MSN Server
                             -------------------
    begin                : Weg July 8, 2009
    copyright            : (C) 2009 by Diederik van der Boor
    email                : vdboor@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MSNSERVERGROUP_H
#define MSNSERVERGROUP_H

#include <QObject>
#include <QMap>

#include "logging/loggingobject.h"

class MsnpServer;
class SoapServer;
class TokenManager;
class Account;
class NotificationServer;
class AdminServer;



/**
 * @brief The KMess Testing MSN Server.
 *
 * This is a testing application, providing two kinds of network services:
 * a SOAP over SSL server and a TCP server.
 *
 * The purpose of these is offering server connections to an MSN client on
 * which the addresses specified for the initial connection have been properly
 * changed with appropriate values. Having in mind the specific goal of
 * servicing MSN clients, therefore, all network services have been finetuned
 * to be used with the MSN Protocol, currently supporting version 13 and above.
 *
 * The MSN Server 'clones' that this application effectively offers are:
 * - A Dispatch Server     (listening on port 1863)
 * - A Notification Server (listening on port 1864)
 * - A Switchboard Server  (port is dynamically generated)
 * - A SOAP server         (listening on port 4430)
 *
 * These servers are able to create by themselves some useful connection data,
 * for example a casually generated contact list. They are also able to parse
 * a KMess debugging log and use the logged info, such as the contact list, to
 * create the connection data.
 *
 * @author Valerio Pilo
 */
class MsnServerGroup : public LoggingObject
{
  Q_OBJECT

  public:

    // Constructor
                          MsnServerGroup( TokenManager *tokenManager, QObject *parent = 0 );
    // Destructor
    virtual              ~MsnServerGroup();

    // Initialize
    void                  initialize();
    // Get the token manager in this MsnServerGroup
    TokenManager         *tokenManager() const;
    // Allocate / deallocate a port to use for switchboard servers
    quint16               allocateSwitchboardPort();
    void                  deallocateSwitchboardPort( quint16 );
    // Get the notification server in this MsnServerGroup
    NotificationServer   *notificationServer() const;
    // Get the admin server in this MsnServerGroup
    AdminServer          *adminServer() const;


  private: // Private properties

    // Our fake Dispatch server
    MsnpServer           *dispatchServer_;
    // Our fake Notification server
    NotificationServer   *notificationServer_;
    // Our fake SOAP server
    SoapServer           *soapServer_;
    // Our real administration server
    AdminServer          *adminServer_;
    // Token manager
    TokenManager         *tokenManager_;
    // Allocated switchboard ports
    QList<quint16>        allocatedSwitchboardPorts_;
};



#endif
