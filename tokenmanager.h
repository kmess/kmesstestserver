/***************************************************************************
                    tokenmanager.h - Testserver authentication token manager
                             -------------------
    begin                : Tue Aug 4, 2009
    copyright            : (C) 2009 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TOKENMANAGER_H
#define TOKENMANAGER_H

#include <QObject>
#include <QDateTime>
#include "logging/loggingobject.h"

struct Token;
class AdminConnection;

/**
 * @brief The KMess Testing MSN Server token manager.
 *
 * This class manages authentication tokens handed out and used for SOAP.
 *
 * @author Sjors Gielen
 */
class TokenManager : public LoggingObject
{
  Q_OBJECT

  public:

    // Constructor
                          TokenManager( QObject *parent = 0 );
    // Destructor
    virtual              ~TokenManager();
    // Request a token
    Token                *requestToken( const QString &type, const QString &address, const QString &handle, int lifetimeSeconds );
    // Request info on a token
    QString               getHtmlTokenInfo( Token *token );
    QString               getHtmlTokenInfo( int id );
    // Get a token with a specific ID
    Token                *findToken( int id ) const;
    Token                *findToken( const QByteArray &token ) const;

  public slots:
    void      tokensRequested( AdminConnection* ) const;
    void      tokenInformationRequested( AdminConnection*, int tid ) const;

  private: // Private properties
    QList<Token*>         tokens_;

  signals:
    void                  tokenAdded( Token* );
    void                  tokenExpired( Token* );
};



struct Token
{
  int id;
  QString type; // ie. "urn:passport:legacy"
  QString address; // ie. "http://Passport.NET/tb"
  QDateTime created;
  QDateTime expires;
  QByteArray securityToken;
  QByteArray binarySecret;

  // Associated data:
  QString handle;
};

#endif
