/***************************************************************************
             switchboardserver.cpp - Faux Switchboard Server
                             -------------------
    begin                : Tue Aug 11, 2009
    copyright            : (C) 2009 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "switchboardserver.h"
#include "notificationserver.h"
#include "../msnservergroup.h"
#include "../connections/notificationconnection.h"
#include "../liveservice/liveservice.h"
#include "../liveservice/account.h"
#include "../liveservice/membership.h"



// Constructor
SwitchboardServer::SwitchboardServer( quint16 listeningPort, MsnServerGroup *msnServerGroup )
: MsnpServer( listeningPort, msnServerGroup )
{
  setObjectName( "SwitchboardServer" );
}



// Destructor
SwitchboardServer::~SwitchboardServer()
{
  msnServerGroup()->deallocateSwitchboardPort( listeningPort() );
}



/**
 * Do an authentication check of this token and handle. Post the result
 * back to the sender.
 */
void SwitchboardServer::doAuthCheck( int trId, const QString &handle, const QString &token, int chatId )
{
  SwitchboardConnection *sc = (SwitchboardConnection*)sender();

  if( expectedConnections_[ handle.toLower() ] != token )
  {
    info( "Auth check failed: Token wasn't known for this user." );
    sc->postAuthenticationResult( trId, false, 0, chatId, membersInConvo_.keys() );
    return;
  }

  // drop the "old one"
  if( membersInConvo_.contains( handle.toLower() ) )
  {
    debug( "Warning: A member was already in the convo with this handle, removing old member." );
    // TODO proper behaviour?
    membersInConvo_[ handle.toLower() ]->closeConnection();
  }

  // notify it's all ok
  Account *me = LiveService::instance()->getAccount( handle );
  sc->postAuthenticationResult( trId, true, me, chatId, membersInConvo_.keys() );

  // add it
  // (only add it after calling postAuthenticationResult since this makes
  // membersInConvo_ one larger and postAuthenticationResult checks if it's
  // empty or not)
  info( "Auth check succeeded - adding " + handle + " to conversation." );
  membersInConvo_[ handle.toLower() ] = sc;
  expectedConnections_.remove( handle.toLower() );

  // and notify all other members someone joined
  QHashIterator<QString,SwitchboardConnection*> it( membersInConvo_ );
  while( it.hasNext() )
  {
    it.next();

    if( it.value() != sc )
    {
      // notify someone joined
      it.value()->someoneJoined( me );
    }
  }
}



// A command was received from a connection
SwitchboardConnection* SwitchboardServer::createConnection( int connectionId )
{
  SwitchboardConnection *sc = new SwitchboardConnection( connectionId, msnServerGroup() );

  connect( sc,   SIGNAL( requestAuthenticationCheck( int, const QString&, const QString&, int ) ),
           this, SLOT(                  doAuthCheck( int, const QString&, const QString&, int ) ) );
  connect( sc,   SIGNAL(              inviteContact( const QString& ) ),
           this, SLOT(                inviteContact( const QString& ) ) );
  connect( sc,   SIGNAL(              messageToSend( const QString&, const QByteArray& ) ),
           this, SLOT(                messageToSend( const QString&, const QByteArray& ) ) );

  return sc;
}



/**
 * @brief Expect a connection from a client.
 *
 * Only invited contacts and the contact that requested the chat may join a
 * server. They get a random token from the Notification server. When
 * connecting to the switchboard server, they "log in" using the USR command
 * and give this random token. This method tells the SwitchboardServer to
 * expect such a client and saves the access token.
 */
void SwitchboardServer::expectConnection( const QString &handle, const QString &token )
{
  if( expectedConnections_.contains( handle.toLower() ) )
  {
    warning( "Warning: This server is already expecting a connection from " + handle
           + "\nWill overwrite the previous access token with the new one." );
  }

  expectedConnections_[ handle.toLower() ] = token;
}



/**
 * @brief Process a contact invitation.
 *
 * Someone invited a contact into this conversation. Process the request and
 * get him there.
 */
void SwitchboardServer::inviteContact( const QString &handle )
{
  SwitchboardConnection *sc  = (SwitchboardConnection*)sender();
  NotificationServer *ns = msnServerGroup()->notificationServer();
  NotificationConnection *nc = ns->getNotificationConnection( handle );

  Account *account = LiveService::instance()->getAccount( sc->handle() );

  // Generate an access token for the new account
  // Here, tokens look like this:
  // 104194101.9520122
  int first  = ( rand() % 899999999 ) + 1000000000;
  int second = ( rand() % 8999999   ) + 1000000;
  QString token( QString::number( first ) + "." + QString::number( second ) );

  // Remember to expect a connection from that handle
  expectConnection( handle, token );

  // Send an invite to the user
  // TODO IP
  nc->invitedToSwitchboard(
      QString( LiveService::instance()->getSettingString( "hostname" )
       + QString::number( listeningPort() ) ),
      token,
      sc->handle(),
      account->getPropertyString( "friendly" ) );
}



/**
 * @brief Send a message to all other participants.
 */
void SwitchboardServer::messageToSend( const QString &replyType, const QByteArray &message )
{
  Q_UNUSED( replyType );

  SwitchboardConnection *sc = (SwitchboardConnection*)sender();

  const QString &from     = sc->handle();
  Account *fromAccount    = LiveService::instance()->getAccount( from );
  const QString &friendly = fromAccount->getPropertyString( "friendly" );

  if( from.isEmpty() )
  {
    warning( "Would send a message from nobody! Skipping send." );
    return;
  }

  // Send it to everybody
  info( "Sending a message from " + from + "to all members..." );
  QHashIterator<QString,SwitchboardConnection*> it( membersInConvo_ );
  while( it.hasNext() )
  {
    it.next();

    // Don't send the message back to the same connection
    if( it.value() != sc )
    {
      it.value()->sendMessage( from, friendly, message );
    }
  }
}
