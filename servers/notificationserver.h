/***************************************************************************
    notificationserver.h - Faux Notification Server
                             -------------------
    begin                : Mon March 30, 2009
    copyright            : (C) 2009 by Valerio Pilo, Diederik van
                           der Boor, Sjors Gielen
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef NOTIFICATIONSERVER_H
#define NOTIFICATIONSERVER_H

#include "msnpserver.h"
#include "../connections/notificationconnection.h"



/**
 * @brief Faux Notification Server.
 *
 * This class manages a MSN Notification server, the central interface to the
 * service for any MSN client.
 *
 * @author Valerio Pilo
 * @author Diederik van der Boor
 * @author Sjors Gielen
 */
class NotificationServer : public MsnpServer
{
  Q_OBJECT

  public:
    // Constructor
                                      NotificationServer( quint16 listeningPort, MsnServerGroup *msnServerGroup );
    // Get the connection object for a handle
    NotificationConnection           *getNotificationConnection( const QString &handle ) const;

  protected:

    // A command was received from a connection
    virtual NotificationConnection   *createConnection( int connectionId );

  private slots:

    // all of these must be defined in msnpserver too!
    void                              loggedIn();
    void                              loggedOut();
    void                              changedStatus( const QString&, const QString&, const QString& );
    void                              changedFriendlyName( const QString& );
    void                              changedUux( const QString& );
    void                              createSwitchboard( int trId );

  private: // Private attributes

    QHash<QString, NotificationConnection*> connectedAccounts_;
};



#endif
