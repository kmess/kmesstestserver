/***************************************************************************
      dispatchserver.cpp - Faux Dispatch Server
                             -------------------
    begin                : Mon March 30, 2009
    copyright            : (C) 2009 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "dispatchserver.h"



// Constructor
DispatchServer::DispatchServer( quint16 listeningPort, MsnServerGroup *msnServerGroup )
: MsnpServer( listeningPort, msnServerGroup )
{
  setObjectName( "DispatchServer" );
}



// A command was received from a connection
MsnpConnection * DispatchServer::createConnection( int connectionId )
{
  return new DispatchConnection( connectionId, msnServerGroup() );
}



