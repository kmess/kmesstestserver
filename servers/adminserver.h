/***************************************************************************
                   adminserver.h - TestServer administration server
                             -------------------
    begin                : Wed Aug 19, 2009
    copyright            : (C) 2009 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ADMINSERVER_H
#define ADMINSERVER_H

#include "msnpserver.h"
#include "../connections/adminconnection.h"



/**
 * @brief KMessTestServer administration and control server.
 *
 * This class describes a server for administration and control. Previously,
 * the KMessTestServer had an UI for administration, but we removed it
 * and replaced it with its own application.
 *
 * @author Sjors Gielen
 */
class AdminServer : public MsnpServer
{
  Q_OBJECT

  public:
    // Constructor
                               AdminServer( quint16 listeningPort, MsnServerGroup *msnServerGroup );
    // Destructor
    virtual                   ~AdminServer();

  public slots:
    void                       sendLogMessage( const QDateTime &time,
                                               const QString &sender,
                                               const QString &logType,
                                               const QString &message );

  protected:

    // A command was received from a connection
    virtual AdminConnection   *createConnection( int connectionId );

  signals:
    void      shutDown();
    void      sendLogMessage_( const QDateTime &time, const QString &sender,
                               const QString &logType, const QString &message );
    void      tokensRequested( AdminConnection* );
    void      contactsRequested( AdminConnection* );
    void      tokenInformationRequested( AdminConnection*, int tid );
    void      contactInformationRequested( AdminConnection*, const QString& );
};



#endif
