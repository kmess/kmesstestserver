/***************************************************************************
               switchboardserver.h - Faux Switchboard Server
                             -------------------
    begin                : Tue Aug 11, 2009
    copyright            : (C) 2009 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SWITCHBOARDSERVER_H
#define SWITCHBOARDSERVER_H

#include "msnpserver.h"
#include "../connections/switchboardconnection.h"



/**
 * @brief Faux Switchboard Server.
 *
 * This class manages a MSN Switchboard server, the controller of a MSN chat.
 *
 * @author Sjors Gielen
 */
class SwitchboardServer : public MsnpServer
{
  Q_OBJECT

  public:
    // Constructor
                                     SwitchboardServer( quint16 listeningPort, MsnServerGroup *msnServerGroup );
    // Destructor
    virtual                         ~SwitchboardServer();
    // Expect a connection from a client
    void                             expectConnection( const QString &handle, const QString &token );

  protected:

    // A command was received from a connection
    virtual SwitchboardConnection   *createConnection( int connectionId );

  private slots:

    void                             doAuthCheck( int, const QString&, const QString&, int );
    void                             inviteContact( const QString &handle );
    void                             messageToSend( const QString&, const QByteArray& );


  private: // Private attributes

    QHash<QString, SwitchboardConnection*> membersInConvo_;
    QHash<QString, QString>          expectedConnections_;
};



#endif
