/***************************************************************************
                soapserver.h - SOAP over HTTPS Connection Manager
                             -------------------
    begin                : Sat March 28, 2009
    copyright            : (C) 2009 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SSLMANAGER_H
#define SSLMANAGER_H

#include <QMap>
#include "../logging/loggingtcpserver.h"


// Forward declarations
class SoapConnection;
class MsnServerGroup;
class SoapMessage;
class SoapHandler;
class TokenManager;



/**
 * @brief Server to receive SOAP messages from a HTTPS connection.
 *
 * This class manages a simple HTTPS server which
 * can be used to send and receive SOAP requests.
 *
 * @author Valerio Pilo
 */
class SoapServer : public LoggingTcpServer
{
  Q_OBJECT


  public:

    // Constructor
                              SoapServer( MsnServerGroup *msnServerGroup );
    // Destructor
    virtual                  ~SoapServer();
    // Initialize the server
    virtual void              initialize();

  private slots:

    // A connection was initiated from a client. Start a secure connection
    void                      incomingConnection( int socketDescriptor );
    // A connection was destroyed
    void                      connectionDestroyed();


  private: // Private properties

    // The number of the last initiated connection
    int                       lastSslConnectionIndex_;
    // The list of active SSL connections
    QMap<int,SoapConnection*> sslConnections_;
    // The standard list of soap request handlers
    QMap<QString,SoapHandler*> standardServerPages_;
    // The MsnServerGroup
    MsnServerGroup           *msnServerGroup_;
};



#endif
