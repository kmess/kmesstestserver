/***************************************************************************
      notificationserver.cpp - Faux Notification Server
                             -------------------
    begin                : Mon March 30, 2009
    copyright            : (C) 2009 by Valerio Pilo, Diederik van
                           der Boor, Sjors Gielen
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "notificationserver.h"
#include "switchboardserver.h"
#include "../msnservergroup.h"
#include "../liveservice/liveservice.h"
#include "../liveservice/account.h"
#include "../liveservice/membership.h"



// Constructor
NotificationServer::NotificationServer( quint16 listeningPort, MsnServerGroup *msnServerGroup )
: MsnpServer( listeningPort, msnServerGroup )
{
  setObjectName( "NotificationServer" );
}



// A command was received from a connection
NotificationConnection* NotificationServer::createConnection( int connectionId )
{
  NotificationConnection *nc = new NotificationConnection( connectionId, msnServerGroup() );

  connect( nc,   SIGNAL(            loggedIn()                ),
           this, SLOT(              loggedIn()                ) );
  connect( nc,   SIGNAL(           destroyed()                ),
           this, SLOT(             loggedOut()                ) );
  connect( nc,   SIGNAL(       changedStatus( const QString&, const QString&, const QString& ) ),
           this, SLOT(         changedStatus( const QString&, const QString&, const QString& ) ) );
  connect( nc,   SIGNAL( changedFriendlyName( const QString& ) ),
           this, SLOT(   changedFriendlyName( const QString& ) ) );
  connect( nc,   SIGNAL(          changedUux( const QString& ) ),
           this, SLOT(            changedUux( const QString& ) ) );
  connect( nc,   SIGNAL(switchboardRequested( int )            ),
           this, SLOT(     createSwitchboard( int )            ) );

  return nc;
}



void NotificationServer::changedStatus( const QString &status, const QString &capabilities, const QString &msnobject )
{
  NotificationConnection *nc = (NotificationConnection*)sender();
  Account *account           = LiveService::instance()->getAccount( nc->handle() );
  const QString &handle      = account->getPropertyString( "handle" );

  // TODO: make NotificationConnection not allow changing status to FLN
  // yourself.

  account->setProperty( "status",       status );
  if( status == "FLN" )
  {
    account->unsetProperty( "capabilities" );
    account->unsetProperty( "msnobject" );
  }
  else
  {
    account->setProperty( "capabilities", capabilities );
    account->setProperty( "msnobject",    msnobject );
  }

  // Send an update to all Reverse contacts
  Membership *reverse = account->getMembershipGroup( MEMBERSHIP_REVERSE );
  for( int i = 0; i < reverse->members().size(); ++i )
  {
    Account *member  = reverse->members().at( i );
    QString  mHandle = member->getPropertyString( "handle" );

    debug( "Thinking about sending an update to " + mHandle );

    if( connectedAccounts_.contains( mHandle.toLower() ) )
    {
      debug( "Sending an update to member " + mHandle
           + " that account " + handle + " changed its status." );

      NotificationConnection *toUpdate = connectedAccounts_[ mHandle.toLower() ];
      toUpdate->contactChangedStatus( account );
    }
  }

  if( status == "FLN" )
  {
    connectedAccounts_.remove( handle.toLower() );
  }
}



void NotificationServer::changedFriendlyName( const QString &name )
{
  NotificationConnection *nc = (NotificationConnection*)sender();
  Account *account           = LiveService::instance()->getAccount( nc->handle() );

  // DO NOT call ->setProperty(), we don't want to *remember* this change. If
  // a client wants to make this change permanent, he uses the
  // ABContactUpdate SOAP call in the AbService.

  const QString &handle      = account->getPropertyString( "handle" );
  const QString &status      = account->getPropertyString( "status" );
  if( status == "FLN" || status == "HDN" )
  {
    return;
  }

  // Send an update to all Reverse contacts
  Membership *reverse = account->getMembershipGroup( MEMBERSHIP_REVERSE );
  for( int i = 0; i < reverse->members().size(); ++i )
  {
    Account *member  = reverse->members().at( i );
    QString  mHandle = member->getPropertyString( "handle" );

    debug( "Thinking about sending an update to " + mHandle );

    if( connectedAccounts_.contains( mHandle.toLower() ) )
    {
      debug( "Sending an update to member " + mHandle
        + " that account " + handle + " changed its status." );

      NotificationConnection *toUpdate = connectedAccounts_[ mHandle.toLower() ];
      toUpdate->contactChangedFriendlyName( account, name );
    }
  }
}



void NotificationServer::changedUux( const QString &uux )
{
  NotificationConnection *nc = (NotificationConnection*)sender();
  Account *account           = LiveService::instance()->getAccount( nc->handle() );

  account->setProperty( "Uux", uux );

  const QString &handle      = account->getPropertyString( "handle" );
  const QString &status      = account->getPropertyString( "status" );
  if( status == "FLN" || status == "HDN" )
  {
    return;
  }

  // Send an update to all Reverse contacts
  Membership *reverse = account->getMembershipGroup( MEMBERSHIP_REVERSE );
  for( int i = 0; i < reverse->members().size(); ++i )
  {
    Account *member  = reverse->members().at( i );
    QString  mHandle = member->getPropertyString( "handle" );

    debug( "Thinking about sending an update to " + mHandle );

    if( connectedAccounts_.contains( mHandle.toLower() ) )
    {
      debug( "Sending an update to member " + mHandle
           + " that account " + handle + " changed its status." );

      NotificationConnection *toUpdate = connectedAccounts_[ mHandle.toLower() ];
      toUpdate->contactChangedUux( account );
    }
  }
}



/**
 * Create a switchboard server and report it back to the
 * NotificationConnection which requested it.
 */
void NotificationServer::createSwitchboard( int trId )
{
  NotificationConnection *nc = (NotificationConnection*)sender();
  const QString &handle = nc->handle();

  quint16 port = msnServerGroup()->allocateSwitchboardPort();
  debug( "Allocated port: " + QString::number( port ) );

  if( port <= 0 )
  {
    warning( "Couldn't allocate a valid switchboard port!" );
    return;
  }

  // MSN tokens look like:
  // 540734642.212116224.4411947
  // we try to generate a number in the same way:
  int first  = ( rand() % 899999999 ) + 100000000;
  int second = ( rand() % 899999999 ) + 100000000;
  int third  = ( rand() % 8999999   ) + 1000000;
  QString token( QString::number( first ) + "." +
                 QString::number( second ) + "." +
                 QString::number( third ) );

  SwitchboardServer *ss = new SwitchboardServer( port, msnServerGroup() );
  ss->initialize();
  ss->expectConnection( handle, token );

  nc->createdSwitchboardServer( trId,
      QString( LiveService::instance()->getSettingString( "hostname" ) + ":"
        + QString::number( port ) ), token );
}



void NotificationServer::loggedIn()
{
  NotificationConnection *nc = (NotificationConnection*)sender();
  Account *account           = LiveService::instance()->getAccount( nc->handle() );
  const QString &handle      = account->getPropertyString( "handle" );

  // If someone else was logged in with that account, log him off
  if( connectedAccounts_.contains( handle.toLower() ) )
  {
    NotificationConnection *disconnect = connectedAccounts_[ handle.toLower() ];
    disconnect->loggedInFromOtherLocation();
  }

  info( "Adding handle to connectedAccounts=" + handle.toLower() );
  connectedAccounts_[ handle.toLower() ] = nc;
}



void NotificationServer::loggedOut()
{
  NotificationConnection *nc = (NotificationConnection*)sender();

  // It was just destroyed, so probably not accessible anymore.
  // Just check the list, see if it's in it, and delete it if so.
  QHashIterator<QString,NotificationConnection*> it( connectedAccounts_ );
  while( it.hasNext() )
  {
    it.next();
    if( it.value() == nc )
    {
      connectedAccounts_.remove( it.key() );
    }
  }
}



/**
 * Return the notification connection belonging to given handle.
 */
NotificationConnection *NotificationServer::getNotificationConnection( const QString &handle ) const
{
  if( !connectedAccounts_.contains( handle.toLower() ) )
  {
    return 0;
  }

  return connectedAccounts_[ handle.toLower() ];
}
