/*
This class is obsolete and will be deleted. It is only here to allow us to copy
some of the code it contains; for command answers and log parsing, mainly.
*/

/***************************************************************************
                          msnsocketnull.h  -  description
                             -------------------
    begin                : Sat May 10 2008
    copyright            : (C) 2008 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MSNSOCKETNULL_H
#define MSNSOCKETNULL_H

#include "msnsocketbase.h"
#include "../contact/msnstatus.h"

#include <QHttpRequestHeader>
#include <QTime>
#include <QTimer>


/**
 * Define the fake server lagginess.
 *
 * Setting these minimum and maximum values changes how fast the fake server
 * will answer to KMess' requests and how often will send those of its own (like
 * pings). The speed is expressed in milliseconds.
 */
#define SOCKET_NULL_LAGGINESS_MIN           20
#define SOCKET_NULL_LAGGINESS_MAX           40

/**
 * Set the account's verified status
 *
 * Switch this define to either "1" or "0" (_including_ the quotes).
 */
#define SOCKET_NULL_IS_ACCOUNT_VERIFIED     "1"

/**
 * Set whether to fake client switching
 *
 * When using this, being disconnected (or disconnecting) will result in KMess
 * believing it was disconnected because the connected account had connected somewhere else.
 * Switch this define to either true or false.
 */
#define SOCKET_NULL_FAKE_OTH              true



/**
 * @brief Basic I/O functionality for a fake MSN server connection.
 *
 * This is a low-level class used used by MsnConnection.
 * The class is a simple and fast way to test the application while being offline; due to its nature,
 * it is useful for testing purposes only and is not available within user builds.
 * It can also be used to quickly test new features without the hassle of having to reconnect at every
 * change: the server lagginess can be set to zero for a lightning fast connection.
 * It will simulate a connection to a MSN server.
 *
 * @author Valerio Pilo <valerio@kmess.org>
 * @ingroup NetworkCore
 */
class MsnSocketNull : public MsnSocketBase
{
  Q_OBJECT


  public: // Public methods

    // The constructor
                               MsnSocketNull( ServerType serverType );
    // The destructor
                              ~MsnSocketNull();
    // Connect to the given server
    void                       connectToServer( const QString& server = QString(), const quint16 port = 0 );
    // Disconnect from the server
    void                       disconnectFromServer( bool isTransfer = false );
    // Return the local IP address
    QString                    getLocalIp() const;
    // Set whether to send pings or not
    void                       setSendPings( bool sendPings );
    // Write data to the socket without conversions
    qint64                     writeBinaryData( const QByteArray& data );
    // Write data to the socket
    qint64                     writeData( const QString& data );


  private: // Private methods

    // Schedule the next response
    void                       scheduleNext();
    // Respond to a command when acting as a Notification Server
    bool                       sendNotificationServerResponse( const QStringList& command );
    // Respond to a command when acting as a Switchboard Server
    bool                       sendSwitchboardServerResponse( const QStringList& command );


  private slots: // Private slots

    // Answer a queued command
    void                       sendResponse();
    // Send to the client the fake contact list
    void                       deliverContactList();


  private: // Private structures

    // A structure to hold the fake contact list's contacts
    struct FakeContact
    {
      // The email address of the contact
      QString                  handle;
      // The friendly name of the contact
      QString                  friendlyName;
      // The contact's GUID
      QString                  guId;
      // List of groups IDs the contact belongs to
      QStringList              groupIds;
      // The lists the contact belongs to (see Contact::MsnContactLists)
      int                      lists;
    };
    MsnSocketNull::FakeContact createFakeContact( const QString &handle, const QString &friendlyName, const QString &guId, const QStringList &groupIds, int lists ) const;
    // A structure to hold details about initially online contacts
    struct OnlineDetails
    {
      QString                  msnObject;
      ulong                    capabilities;
      QString                  status;
    };


  private: // Private attributes

    // The time since last response was processed
    QTime                      responseTime_;
    // A timer to anwser the queries after some time
    QTimer                     responseTimer_;
    // The list of queued messages
    QList<QByteArray>          messageQueue_;
    // Are we sending pings?
    bool                       sendPings_;
    // Whether the Notification Server transfer test has been done or not
    bool                       isTransferDone_;


  private: // Private static attributes

    // The list of fake contacts
    static QMap<QString,FakeContact>   contactList_;
    // The list of fake groups in the contacts list
    static QMap<QString,QString>       groupList_;
    // The list of contacts to set as initially online
    static QMap<QString,OnlineDetails> onlineList_;


};



#endif
