/***************************************************************************
              soapserver.cpp - SOAP over HTTPS Connection Manager
                             -------------------
    begin                : Sat March 28, 2009
    copyright            : (C) 2009 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "soapserver.h"
#include "../soaphandlers/soapmessage.h"
#include "../soaphandlers/soaphandler.h"
#include "../soaphandlers/rst.h"
#include "../soaphandlers/schematizedstore.h"
#include "../soaphandlers/abservice.h"
#include "../connections/soapconnection.h"
#include "../msnservergroup.h"

#include <QSslCertificate>
#include <QSslKey>
#include <QSslSocket>



// Constructor
SoapServer::SoapServer( MsnServerGroup *msnServerGroup )
: LoggingTcpServer( msnServerGroup )
, lastSslConnectionIndex_( 0 )
, msnServerGroup_( msnServerGroup )
{
  setObjectName( "SoapServer" );

  Rst *rst = new Rst( msnServerGroup );
  AbService *ab = new AbService( msnServerGroup );
  SchematizedStore *ss = new SchematizedStore( msnServerGroup );

  // update the destructor when you edit this!
  standardServerPages_[ "login.live.com/RST.srf" ] = rst;
  standardServerPages_[ "omega.contacts.msn.com/abservice/SharingService.asmx" ]
    = ab;
  standardServerPages_[ "omega.contacts.msn.com/abservice/abservice.asmx" ]
    = ab;
  standardServerPages_[ "storage.msn.com/storageservice/SchematizedStore.asmx" ]
    = ss;

  // Workaround for a bug in KMess 2.0.
  // KMess does a request against URL login.live.com/RST.srf, but when the test
  // server is enabled, the URL is rewritten and RST.srf is lost. This is fixed
  // for both 2.0.1 and 2.1. With current TestServer functionality, this fix
  // works more or less.
  // Once 2.0.1 is released, we can drop this functionality and just say
  // the kmesstestserver can't be used for kmess 2.0. (Not that it'll be needed
  // anymore, then.)
  standardServerPages_[ "login.live.com/" ] = rst;
  standardServerPages_[ "omega.contacts.msn.com/" ] = ab;
  standardServerPages_[ "storage.msn.com/" ] = ss;
}



// Initialize the server
void SoapServer::initialize()
{
  listen( QHostAddress::Any, 4430 );

  if( isListening() )
  {
    debug( "Started listening." );
  }
  else
  {
    error( "Failed to initialize!" );
  }
}



// Destructor
SoapServer::~SoapServer()
{
  qDeleteAll( sslConnections_ );

  // delete all created objects  
  delete standardServerPages_[ "login.live.com/RST.srf" ];
  delete standardServerPages_[ "omega.contacts.msn.com/abservice/abservice.asmx" ];

  standardServerPages_.clear();

  debug( "Gone down successfully." );
}



// A connection was destroyed
void SoapServer::connectionDestroyed()
{
  SoapConnection *connection = static_cast<SoapConnection*>( sender() );

  if( connection == 0 )
  {
    warning( "Invalid connection!" );
    return;
  }

  int connectionId = sslConnections_.key( connection, -1 );
  if( connectionId < 0 )
  {
    warning( "Connection with " + connection->client() + " not found!" );
    return;
  }

  sslConnections_.remove( connectionId );
}



// A connection was initiated from a client. Start the SSL encryption mode
void SoapServer::incomingConnection( int socketDescriptor )
{
  SoapConnection *connection = new SoapConnection( lastSslConnectionIndex_, this, &standardServerPages_ );

  // Connect the SSL socket's signals
  relayChildLogMessages( connection );
  connect( connection, SIGNAL(           destroyed(QObject*) ),
           this,       SLOT  ( connectionDestroyed()         ) );

  // Register the new connection
  sslConnections_[ lastSslConnectionIndex_ ] = connection;

  debug( "Created new SSL connection with id: " +
         QString::number(lastSslConnectionIndex_) );

  connection->initialize( socketDescriptor );

  ++lastSslConnectionIndex_;
}
