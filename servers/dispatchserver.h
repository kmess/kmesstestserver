/***************************************************************************
        dispatchserver.h - Faux Dispatch Server
                             -------------------
    begin                : Mon March 30, 2009
    copyright            : (C) 2009 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef DISPATCHSERVER_H
#define DISPATCHSERVER_H

#include "msnpserver.h"
#include "../connections/dispatchconnection.h"



/**
 * @brief Faux Dispatch Server.
 *
 * This class manages a MSN Dispatch server, ie a "messenger.hotmail.com"
 * clone. It redirects the clients to a Notification Server.
 *
 * @author Valerio Pilo, Diederik van der Boor
 */
class DispatchServer : public MsnpServer
{
  public:
    // Constructor
                              DispatchServer( quint16 listeningPort, MsnServerGroup *msnServerGroup );

  protected:

    // A command was received from a connection
    virtual MsnpConnection   *createConnection( int connectionId );

};



#endif
