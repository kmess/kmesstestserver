/***************************************************************************
                   adminserver.h - TestServer administration server
                             -------------------
    begin                : Wed Aug 19, 2009
    copyright            : (C) 2009 by Sjors Gielen
    email                : sjors@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "adminserver.h"
#include "../connections/adminconnection.h"
#include "../liveservice/liveservice.h"



// Constructor
AdminServer::AdminServer( quint16 listeningPort, MsnServerGroup *msnServerGroup )
: MsnpServer( listeningPort, msnServerGroup )
{
  setObjectName( "AdminServer" );
  setLoggingSilent( true );
}



// Destructor
AdminServer::~AdminServer()
{
}



// A command was received from a connection
AdminConnection* AdminServer::createConnection( int connectionId )
{
  AdminConnection *ac = new AdminConnection( connectionId, msnServerGroup() );

  connect( ac,   SIGNAL( shutDown() ),
           this, SIGNAL( shutDown() ) );
  connect( ac,   SIGNAL( tokensRequested( AdminConnection* ) ),
           this, SIGNAL( tokensRequested( AdminConnection* ) ) );
  connect( ac,   SIGNAL( contactsRequested( AdminConnection* ) ),
           this, SIGNAL( contactsRequested( AdminConnection* ) ) );
  connect( ac,   SIGNAL( tokenInformationRequested( AdminConnection*, int ) ),
           this, SIGNAL( tokenInformationRequested( AdminConnection*, int ) ) );
  connect( ac,   SIGNAL( contactInformationRequested( AdminConnection*,
                                                      const QString& ) ),
           this, SIGNAL( contactInformationRequested( AdminConnection*,
                                                      const QString& ) ) );

  connect( this, SIGNAL( sendLogMessage_( QDateTime,QString,QString,QString)),
           ac,   SLOT  (  sendLogMessage( QDateTime,QString,QString,QString)));
  return ac;
}

/// Send a log message
void AdminServer::sendLogMessage( const QDateTime &time,
                                  const QString &sender,
                                  const QString &logType,
                                  const QString &message )
{
  emit sendLogMessage_( time, sender, logType, message );
}
