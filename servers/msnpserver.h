/***************************************************************************
          tcpmanager.cpp - TCP Connection Manager
                             -------------------
    begin                : Sun March 29, 2009
    copyright            : (C) 2009 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MSNPSERVER_H
#define MSNPSERVER_H

#include <QMap>
#include <QStringList>
#include "../logging/loggingtcpserver.h"


// Forward declarations
class MsnpConnection;
class MsnServerGroup;



/**
 * @brief TCP Server.
 *
 * This class manages a simple TCP server which can be used to send and receive
 * MSN data over TCP connections. It is not meant for direct use but is to be
 * subclassed.
 *
 * @author Valerio Pilo, Diederik van der Boor
 */
class MsnpServer : public LoggingTcpServer
{
  Q_OBJECT


  public:

    // Constructor
                              MsnpServer( quint16 listeningPort, MsnServerGroup *parent = 0 );
    // Destructor
    virtual                  ~MsnpServer();
    // Initialize the server
    virtual void              initialize();
    // Return the msn server group this server belongs to.
    MsnServerGroup          * msnServerGroup() const;
    // Return the port this object is listening at
    int                       listeningPort() const;

  protected:

    // The "factory method" to create the proper TCP connection.
    // Can't use a template class because Q_OBJECT does not support it.
    virtual MsnpConnection   *createConnection( int connectionId ) = 0;


  private slots:

    // A connection was initiated from a client.
    void                      incomingConnection( int socketDescriptor );
    // A connection was destroyed
    void                      connectionDestroyed();
    // Pure virtual signals for derived classes
    virtual void              loggedIn() {}
    virtual void              loggedOut() {}
    virtual void              changedStatus( const QString&, const QString&, const QString& ) {}
    virtual void              changedFriendlyName( const QString& ) {}
    virtual void              changedUux( const QString& ) {}
    virtual void              createSwitchboard( int trId ) {}



  private: // Private properties

    // The number of the last initiated connection
    int                       lastTcpConnectionIndex_;
    // TCP port where the manager should listen on
    quint16                   listeningPort_;
    // The msn server group
    MsnServerGroup           *msnServerGroup_;
    // The list of active TCP connections
    QMap<int,MsnpConnection*> tcpConnections_;
};



#endif
