/***************************************************************************
          msnpserver.cpp - TCP Connection Manager
                             -------------------
    begin                : Sun March 29, 2009
    copyright            : (C) 2009 by Valerio Pilo
    email                : valerio@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "msnpserver.h"
#include "../msnservergroup.h"               // so the static_cast from QObject* works.
#include "../connections/msnpconnection.h"   // so the static_cast from QObject* works.



// Constructor
MsnpServer::MsnpServer( quint16 listeningPort, MsnServerGroup *serverGroup )
: LoggingTcpServer( serverGroup )
, lastTcpConnectionIndex_( 0 )
, listeningPort_( listeningPort )
, msnServerGroup_( serverGroup )
{
  setObjectName( "MsnpServer" + QString::number( listeningPort ) );
}



// Destructor
MsnpServer::~MsnpServer()
{
  // Don't qDeleteAll - every connection has the MsnServerGroup as its parent,
  // so QObject will take care of deleting them.
  //qDeleteAll( tcpConnections_ );
  tcpConnections_.clear();

  debug( "Gone down successfully." );
}



// Initialize the server
void MsnpServer::initialize()
{
  listen( QHostAddress::LocalHost, listeningPort_ );

  if( isListening() )
  {
    info( "Started listening." );
  }
  else
  {
    error( "Failed to initialize!" );
  }
}



// A connection was destroyed
void MsnpServer::connectionDestroyed()
{
  MsnpConnection *connection = static_cast<MsnpConnection*>( sender() );

  if( connection == 0 )
  {
    warning( "Invalid connection!" );
    return;
  }

  int connectionId = tcpConnections_.key( connection, -1 );
  if( connectionId < 0 )
  {
    warning( "Connection to " + connection->client() + " not found!" );
    return;
  }

  tcpConnections_.remove( connectionId );

//   kDebug() << "Destroyed TCP connection with id:" << connectionId;
}



// A connection was initiated from a client.
void MsnpServer::incomingConnection( int socketDescriptor )
{
  MsnpConnection *connection = createConnection( lastTcpConnectionIndex_ );

  relayChildLogMessages( connection );

  connect( connection, SIGNAL(           destroyed(QObject*) ),
           this,       SLOT  ( connectionDestroyed()         ) );

  // Register the new connection
  tcpConnections_[ lastTcpConnectionIndex_ ] = connection;

  connection->initialize( socketDescriptor );

  ++lastTcpConnectionIndex_;
}



int MsnpServer::listeningPort() const
{
  return listeningPort_;
}



// Return the msn server group this server belongs to.
MsnServerGroup * MsnpServer::msnServerGroup() const
{
  return msnServerGroup_;
}
