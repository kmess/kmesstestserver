/***************************************************************************
          msnservergroup.cpp - KMess Testing MSN Server
                             -------------------
    begin                : Weg July 8, 2009
    copyright            : (C) 2009 by Diederik van der Boor
    email                : vdboor@kmess.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <QDateTime>

#include "msnservergroup.h"

#include "liveservice/account.h"
#include "liveservice/liveservice.h"
#include "servers/dispatchserver.h"
#include "servers/notificationserver.h"
#include "servers/switchboardserver.h"
#include "servers/soapserver.h"
#include "servers/adminserver.h"



// Constructor
MsnServerGroup::MsnServerGroup( TokenManager *tokenManager, QObject *parent )
: LoggingObject( parent )
, dispatchServer_( 0 )
, notificationServer_( 0 )
, soapServer_( 0 )
, adminServer_( 0 )
, tokenManager_( tokenManager )
{
  setObjectName("MsnServerGroup");

  // Start the Dispatch server
  dispatchServer_ = new DispatchServer( 1863, this );
  relayChildLogMessages( dispatchServer_ );

  // Start the Notification server
  notificationServer_ = new NotificationServer( 1864, this );
  relayChildLogMessages( notificationServer_ );

  // Start the SOAP server
  soapServer_ = new SoapServer( this );
  relayChildLogMessages( soapServer_ );

  // Start the admin server
  adminServer_ = new AdminServer( 1865, this );
  relayChildLogMessages( adminServer_ );
}



// Destructor
MsnServerGroup::~MsnServerGroup()
{
  debug( "Destroying..." );

  // HACK: Delete all switchboard servers
  // This is because they call deallocSwitchboardPort(), which
  // crashes if this object is already destroyed.
  // TODO: make the MsnServerGroup create *one* switchboard server which
  // can handle multiple chats at the same time.
  QList<SwitchboardServer*> switchboards = findChildren<SwitchboardServer*>();
  for( int i = 0; i < switchboards.size(); ++i )
  {
    SwitchboardServer *ss = switchboards[ i ];
    ss->setParent( 0 );
    delete ss;
  }
  switchboards.clear();

  delete soapServer_;
//   delete notificationServer_;

  debug( "Destroyed." );
}



// Allocate a switchboard port
quint16 MsnServerGroup::allocateSwitchboardPort()
{
  int sbstartport = LiveService::instance()->getSettingInt( "sbstartport" );
  int sbendport   = LiveService::instance()->getSettingInt( "sbendport"   );

  if( sbstartport <= 0 )
  {
    sbstartport = 1900;
    LiveService::instance()->setSetting( "sbstartport", sbstartport );
  }
  if( sbendport <= 0 )
  {
    sbendport = 2000;
    LiveService::instance()->setSetting( "sbendport", sbendport );
  }

  if( sbstartport > sbendport )
  {
    // Naive fix, and we don't check for a lot of problems, but oh well.
    warning( "Sbstartport=" + QString::number(sbstartport)
           + " is higher than sbendport=" + QString::number(sbendport)
           + ". Fixing sbendport to be " + QString::number(sbstartport+10) );
    sbendport = sbstartport + 10;
  }

  for( quint16 port = sbstartport; port <= sbendport; ++port )
  {
    if( !allocatedSwitchboardPorts_.contains( port ) )
    {
      allocatedSwitchboardPorts_.append( port );
      return port;
    }
  }

  warning( "Wasn't able to allocate a switchboard port!" );
  return 0;
}



void MsnServerGroup::deallocateSwitchboardPort( quint16 port )
{
  allocatedSwitchboardPorts_.removeAll( port );
}



// Initialize the connections
void MsnServerGroup::initialize()
{
  debug( "Starting servers..." );
  dispatchServer_->initialize();
  notificationServer_->initialize();
  soapServer_->initialize();
  adminServer_->initialize();
  info( "Done starting servers." );
}



/**
 * Return the token manager in this MsnServerGroup.
 */
TokenManager *MsnServerGroup::tokenManager() const
{
  return tokenManager_;
}



/**
 * @brief Return the admin server in this MsnServerGroup.
 */
AdminServer *MsnServerGroup::adminServer() const
{
  return adminServer_;
}



/**
 * Return the notification server in this MsnServerGroup.
 */
NotificationServer *MsnServerGroup::notificationServer() const
{
  return notificationServer_;
}
